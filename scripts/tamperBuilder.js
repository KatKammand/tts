import fs from "fs";

const buildOutput = fs.readFileSync("./public/morser.min.js", "utf-8");

const tamperStruct = fs.readFileSync("./tamperStruct.txt", "utf-8");
const tamperFinal = tamperStruct.replace("//CODE_HERE", buildOutput);
fs.writeFileSync("./public/tamper.min.js", tamperFinal);
