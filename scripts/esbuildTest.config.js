import { context } from "esbuild";
import time from 'esbuild-plugin-time';

import { CSSMinifyPlugin } from "./buildPlugins.js";

const PORT = 3010;
const HOST = 'localhost';

const ctx = await context({
    entryPoints: ["index.ts"],
    bundle: true,
    minify: false,
    sourcemap: false,
    format: "iife",
    platform: "browser",
    outfile: "test/tts.test.js",
    loader: {
        ".css": "text", // This tells esbuild to treat .css files as text files
        ".html": "text",
        ".png": "dataurl",
    },
    plugins: [
        CSSMinifyPlugin,
        time()
    ],
    define: {
        "process.env.VERSION": JSON.stringify(process.env.npm_package_version),
        "process.env.KOKORO_TTS_LINK": JSON.stringify("")
    },
});

await ctx.watch();
await ctx.serve({ servedir: "test", host: HOST, port: PORT });
console.log('Build server started on http://%s:%s', HOST, PORT);
