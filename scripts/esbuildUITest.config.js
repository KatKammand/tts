import esbuild from "esbuild";

import { CSSMinifyPlugin } from "./buildPlugins.js";

// could still fix the import.meta.dirname, could make it only run the specifc file, could check what esm format produces into it

const ctx = await esbuild
    .context({
        //entryPoints: ["uiTest.ts"],
        entryPoints: { "ttsUi.test": "index.ts", /*"kokoro-tts/worker": "src/kokoro-tts/worker.ts"*/ },
        bundle: true,
        minify: false,
        sourcemap: false,
        format: "iife",
        platform: "browser",
        outdir: "served/build",
        //outfile: "served/build/ttsUi.test.js",
        loader: {
            ".css": "text", // This tells esbuild to treat .css files as text files
            ".html": "text",
            ".png": "dataurl",
        },
        plugins: [CSSMinifyPlugin /*ImportMetaFixPlugin("http://localhost:8000/build/kokoro-tts")*/],
       define: {
            "process.env.VERSION": JSON.stringify(process.env.npm_package_version),
            "process.env.KOKORO_TTS_LINK": JSON.stringify("http://localhost:8000/build"),
        },
    })
    .catch(() => process.exit(1));

//await ctx.watch();

const ctx2 = await esbuild
    .context({
        //entryPoints: ["uiTest.ts"],
        entryPoints: ["src/kokoro-tts/worker.ts"],
        bundle: true,
        minify: false,
        sourcemap: false,
        format: "esm",
        platform: "browser",
        outfile: "served/build/kokoro-tts/workerEsm.js",
        loader: {
            ".css": "text", // This tells esbuild to treat .css files as text files
            ".html": "text",
            ".png": "dataurl",
        },
        plugins: [CSSMinifyPlugin],
    })
    .catch(() => process.exit(1));

await ctx2.watch();

const { host, port } = await ctx.serve({
    servedir: "served",
});

console.log(host, port);
