import esbuild from "esbuild";

import { CSSMinifyPlugin } from "./buildPlugins.js";




esbuild
    .build({
        entryPoints: ["index.ts"],
        bundle: true,
        minify: true,
        sourcemap: true,
        format: "iife",
        platform: "browser",
        outfile: "public/tts.min.js",
        loader: {
            ".css": "text", // This tells esbuild to treat .css files as text files
            ".png": "dataurl",
        },
        plugins: [CSSMinifyPlugin],
        define: {
            "process.env.VERSION": JSON.stringify(process.env.npm_package_version),
            "process.env.KOKORO_TTS_LINK": JSON.stringify(""),
        },
    })
    .catch(() => process.exit(1));