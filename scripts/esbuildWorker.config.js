import esbuild from "esbuild";

import { CSSMinifyPlugin } from "./buildPlugins.js";

esbuild.build({
    entryPoints: ["src/kokoro-tts/worker.ts"],
    bundle: true,
    minify: true,
    sourcemap: false,
    format: "esm",
    platform: "browser",
    outfile: "public/kokoro-tts/workerEsm.js",
    loader: {
        ".css": "text",
        ".html": "text",
        ".png": "dataurl",
    },
    plugins: [CSSMinifyPlugin],
}).catch(() => process.exit(1));;
