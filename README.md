# TTS-MOD - Text-to-Speech mod and Morse to Vibrations

## Description

Extension to BondageClub that wants to breach the digital world's limitations, and bring some more physical elements to it.
See "USAGE" section for more detail on modules
Current elements:

1. TTS - Text-To-Speech (default ON)
    - tts: "voiced message" in chat, or toggle it to tts on all texts, so the tts mod will "talk" chat messages, emotes, whispers
    - See TTS Voice and Privacy section lower in the readme for comments on browser's and the voices.
    - uses browser inbuilt speech functions (see Webspeech API)
    - TTS Voices Server offers locally created AI voices for tts, still in beta but usable.
        - Install & usage instructions especially (and other explanations) need improvement still
        - (TTS Voices server)[https://gitlab.com/KatKammand/tts-server/-/tree/main]
            - Atm if you want to use it and cant get it working with the instructions, come to `TTS Mod room` in BC to ask for help
2. MORSE (default OFF)
    - Receive Morse messages from others (off by default)
    - Optional Buttplug.io element to it, to allow morsing through vibrations
3. ToySync (default OFF) - Syncs an item slot on your character with Buttplug.io device
    - Allows others/yourself to control it through club toy vibration levels.
    - NEED TO HAVE FBC Buttplug.io off for this to work, can only use one at a time.
4. MISC (smaller features)
    - Futuristic Vibrator triggers to work with beeps
    - Universal Remote Addon - extension. Both user and target need UniRemote mod (find in FUSAM) and TTS & to Opt in.
        - Allows all configurable devices to be remotely controlled

Configured through Settings UI

All elements can be toggled off/on at will.

Buttplug.io & Intiface Central (Dont need FBC, but their setup instructions are good - Here)[https://sidiousious.gitlab.io/bce/toys.html]

BUG? BUG!
I can be found in "TTS Mod Room" quite often. Or post in the BC Scripting community Discord, i do lurk there.
Or create an Issue in GitLab repo (sadly requires Gitlab account) or merge request.

## Installation

(For ToySync or Morser modules buttplug.io connected toy to work) Intiface Central running on port 12345 (can only have this or FBC connected at the same time, this provides same as FBC - with better refresh rate + more), with only the device you want to use with this connected to it. (works with multiple connected but will use 1st one only at the moment).
Intiface Central should show "TTSSync connected" once you have its server running and the mod running in the club (and are logged in to a user).

Available on FUSAM (the collective bc-addon-loader mod). if you are not using it yet, check it out (FUSAM here)[https://sidiousious.gitlab.io/bc-addon-loader/]

-   FUSAM also provides instructions to mobile bookmark use.

TamperMonkey: `https://katkammand.gitlab.io/tts/ttsLoader.user.js`

Bookmark: `javascript:(function(){if(window.TTS_CONFIG===undefined){let n=document.createElement("script");n.type="module";n.setAttribute("src","https://katkammand.gitlab.io/tts/tts.min.js?_="+Date.now());document.head.appendChild(n);};})();`

PROBLEMS WITH GETTING VOICE TO WORK?

-   make sure you have sound working on the browser/tab
    -   make sure tab is not muted & your device volume is up
    -   try making sound in the club eg spank/test notification, if those cause sound, then issue is somewhere else
    -   make sure TTS module is active in the settings

## Usage

TTS Settings now open from BC Preferences menu --> TTS Settings button
See `/tts-ui` to open the settings menu
See `/help` --> for commands (most already in settings menu - except for morse related),
all start with `/tts`, `/morse` or `/toysync`
(you can click on the bolded `/tts-<command>` to autocomplete it, no need to type them)
(or `/tts- + TAB` for nice list)
`/tts-mod-status` lists active module statuses.

### TTS Text-To-Speech

1. Text-To-Speech - will trigger browser Text-To-Speech for any messages, whispers, emotes, that start with `TTS: [tts reads this]`

    - `/tts-all` Auto read for all Chat messages, emotes & whispers (doesnt voice actions)
    - `/tts-own` Toggle reading your own messages
    - `/tts-sender` Togggle reading messages sender player name with the message (sry non-english people, i'll add an option to not not say "says/whispers" later)
        - !!Special character including nicknames cant be voiced by TTS, Name will be used instead!!
    - Give Specified Players individual voices in settings. (eg yourself or your lovers)
    - Set a default voice for all TTS messages in the settings menu (everyone with no voice set, will use default)
    - `/tts-skip` to skip currently voiced text
    - `/tts-clear` to clear all queued tts messages (eg someone sent a bunch of long messages to spam you)
    - For others to hear the TTS, they need to have the mod running too (the voices they use might be different).
    - See Voices and Privacy for more info on voices (how to get more voices etc.)

### Morse

2. Morse - allow sending morse messages through a Buttplug.io/Intiface Central connected device

    - Has some training tasks and visibility functionalities, so the user can be trained.
    - Works with Chat messages, whispers, beeps.
    - Do not have to have the mod to send Morse commands, only to receive.
    - Morse module settings in settings UI now
    - `/morse-toggle` to activate it (you can now receive messages)
    - `/morse-help` to see commands (you can send messages and commands to yourself at the moment)
    - `/morse-vibrations-toggle` to activate Intiface Central connection and the buttplug.io vibrations related to `/morse` functions
        - Works at the same time as `/toysync-` modules vibrations, if on same toy, will pause toysync vibrations, morse, then continue.
    - In chat `Morse [playerid] msg: [message that will be transformed to morse]` to send a morsed message
        - can send to yourself
    - Has training functions for morse available, see `/morse-help` for a list of related commands.
        - If you are sent multiple type tasks, the oldest is in the prompt 1st, the next ones will pop up after you have submitted the response, until you have done all queued morse prompt tasks.
    - Others (or yourself) can now send chat messages, whispers, beeps that trigger the morse, if the message follows required pattern.

### ToySync & Buttplug.io

3. Buttplug.io powered IRL ToySync (similar to FBC ToySync, but instant response time)

    - Dont need FCB for it, but their instructions on how to set it up are good - see [See FCB instructions related to it](https://sidiousious.gitlab.io/bce/toys.html)
    - Use settings menu to slot devices --> need to activate the module for connection to be created.
    - `/toysync-toggle` to activate
    - Gets your toysync settings from FBC if you had it setup there previously, if not:
    - Give a connected device a slot, then equip a toy on your character, and change the toys vibration level.
    - for XToys compatible devices, see clubs XToys mod (also in fusam): [here](https://github.com/ItsNorin/Bondage-Club-XToys-Integration)

### MISC

4. Misc - Random small features
    - Can use Beeps to send FuturisticVibrator voice command triggers to a target wearing it (Target has to have TTS and this enabled, sender doesnt need.)

### TTS ToySync

5. TTS ToySync
    - Allows Syncronizing text-to-speech reading and connected buttplug.io/toysync toys.
    - Only target of the actions needs to have TTS, TTS Toysync & Toysync active.
    - Idea is the to allow someone to "narrate a sequence of actions", where it would be appropriate for Toy's to react too, and let the toy reaction happen at the same time as the user hears TTS voice reading it.
        - If you "TTS Toysyncronization allowed" --> any messages TTS would normally read for you, that have valid `[action]` commands embedded in its text, are processed. No need for any message prefixes.
        - Example message: "TTSToySync: Person1 presses a button and person2 feels the toys in them start vibrating `[low:5]`, after 5 seconds they start slowly gaining speed `[up]`, and momentum `[up:2]`, but they were being toyed with as they dropped down again `[down]` and turned off `[off]`"
    - Current available commands:
        - General format: `[<action>:<wait seconds>]` eg: `[max:5.5]` (maximum intensity, wait 5.5 seconds before continuing sequence reading)
        - `[:2]` - wait 2 seconds, do nothing else. (wait time can be decimal seconds, so could be eg 0.5 if you want to wait smaller amounts)
        - `[low]` - Low vibration level, same as on Toysync devices
        - `[med]` - Medium vibration level
        - `[high]` - High level
        - `[max]` - Maximum level
        - `[up]` - Moves up by one level, unless at max.
        - `[down]` - Moves down by one level, unless already off
        - `[off]` - Stops vibrations
        - `[<number>]` - Any number between 0.0 - 1.0, allows finer control of devices than the usual levels, eg, increasing in 0.1 increments (requires you to remember what it was though).
            - eg: "Your toys start vibrating slighly `[0.1]`, gradually `[0.2]` growing `[0.3:1]` `[0.4:1]` `[0.5:1]` `[0.6:1]` `[0.7:1]` `[0.8:1]` `[0.9:1]` reaching its max `[1.0:5]` and cutting off `[off]`"
    - Tips: Best to first make the action command then the text description, eg:
        - "ttstoysync: the toys `[off]` reset, then start building `[up]` momemtum, steadily `[up]` growing, and suddenly hitting `[max]` speed, then waiting `[:2]` for a moment, and turning `[off]` off."
    - if someone else than me uses this and wants "shock" command to this, find me in the "TTS Room" or open issue
        - can be pishock integrated, can be just club shock collar trigger ^^
        - not going to bother doing it unless asked though, I suspect this parts users are going to be quite limited to selected few ^^

Intiface Central is connected to this mod and club when its Status: "TTS-Sync connected"

## TTS Voice and Privacy

### OS Support

-   Windows --> All good, see "Voices" on how to get more/English voices if missing
-   Mac OS --> should work, but Safari has some issues, suggest using some proper browser, like firefox (or Edge for great voices... or Chrome, but why would you use chrome?)
-   Linux --> Uh... maybe? It uses the systems Accessability Narrator voices, and some Linux distros should share it with browsers by default, others dont. Not my area of expertiese. You can test it by running the following commands in a browser console:
    -   `speechSynthesis.getVoices()` --> should return an List of voice options, if it doesnt, you need to research on how to enable WebSpeech API/ SpeechSynthesis for browsers on your distro
    -   `speechSynthesis.speak(new SpeechSynthesisUtterance('It works if you hear this'))` --> this would trigger a narrator voicing for the given text, using a default voice.

### Browser Support

-   Desktop Browsers should all work (assuming its up to date and still maintanied)
    Some browsers might have additional Online voices (eg edge), but all should have the ones your OS provides
-   Safari on Mac seems to have issues. Voice stops working quite soon, hard to debug, suggest you use different browser.

-   Most Mobile Browsers should work.
    -   see [The browser support](https://caniuse.com/?search=SpeechSynthesisUtterance) for your browser.
    -   Android Edge Mobile doesnt work (Android Edge Canary edition might work and with natural voices too... maybe?)
    -   Android Chrome & Firefox should work, IOS Safari too.

### Voices

-   You can download more voices for windows in windows settings --> "Time & Language" --> "Speech" --> "Managed Voices" --> "Add voices"
    -   the windows UI for this is inoptimal, it doesnt tell you when they are downloaded.
    -   basically just need to give it some time then restart pc/browser
    -   you know they are installed when they appear in the voice selections/"Installed voice packages" shows them (and says 0MB for them for some reason)
-   Also Edge browser has significantly better, like really really good voices, but only for edge (have the "Online Natural" in the name).
    -   The "Online Natural" voices are processed on Microsoft servers, so that information gets shared to Microsoft.
-   Chrome has some similar online processed voices (eg: "Google UK English", all that start with "Google")
    -   They have been disabled for TTS, because they are heavily rate limited, and cutoff very fast.
    -   Those voices are also online processed on Google servers.
-   Microsoft Voices (that do not say Online) are processed locally, and do not share information outside.
-   Dont know how to do it on Mac (or phones), your on your own. Google "Mac narrator voices" or something.

### Privacy Question

-   The legacy voices, what most browsers have, are processed on your OS, locally (System Accessability Narrator). Those are private.
-   The Online Natural voices (only available on Edge) are really beautiful, but processed Online in Azure servers, so you end up sharing your texts to Microsoft.
    If you are using chrome you are probably already sharing everything to google though.

-   Problem with those voices, only on Edge, and they are processed on Azure (microsofts) servers, so they will see what lewd texts you send. If you dont care about Microsoft seeing your stuff, then do try those voices ^^
-   https://zolomohan.github.io/text-to-speech/ --> nice page to show and test the voices available for you

## Support

-   If your settings arent saving, check that you have enough LocalStorage alloted for the page.
-   Nickname not being voiced --> make sure you have `/tts-toggle-sender` true, and nickname doesnt have special characters.

Open an issue here on the Gitlab repo. I do lurk the BC Scripting Discord, will probably setup own or something in future.

## Contributing

Open issue for bugs and suggestions, do pull requests, i'll most likely look it through. OR just fork it and do something.

Feel free to take things and add it to your own mods.

## License

MIT

## Project status

WIP

## TODO

! By mentioning some addition/todo/suggestion to me you can increase its proprity in my mind and make it happen faster ^^, if its listed here, means i am aware of it, but the more i am suggested it, the higher its priority!

-   Map room TTS to actually listen to Map audiable chat instead of absolutely all of it
-   Map rooms --> non cheat version doesnt work with distance sensitivity
-   Toysync --> bcx/wheel equipped items dont work until vib speed changes
-   Beep voice \* trigger not working unless it triggers another option too
-   Patterns
-   TtsToysync --> bugs, last command doesnt get read at times, textless sequence fails at times? hiding commands not working reliably
-   TtsToysync --> device choosing --> pattern triggering from here too?
-   TTS special char filtering --> ' + - to not be filtered (and some other purely grammar special chars?)
-   Toysync --> rotate commands --> vibrate multichannel --> name+deviceId (some have multiple devices with same name)
-   Toysync --> share slotted
-   pishocks - more uses --> vibrations?
-   morse --> fail/punish doesnt resume toysync vib level properly...
-   toysync --> disconnect device reconnect or something causes crash with LastIntensity
-   tts Default voice 'disable' --> so only players who are given a voice are voiced
-   tts config for Emote, Whisper, Chat, Beep, BCX Voice
-   toysync slot option names to more understandable
-   morse increase spacing between chat morse for readability (maybe add some contrast?)
-   test ws://localhost:+port with intiface --> mobile issues
-   morse multiple devices (simple 1 or ALL devices?)
-   morse vib queue
-   morse repeat msg btn
-   morse ability to stop & repeat the morse
-   morse visibility change not working?
-   morse increase reward/punishment wait times
-   morse training mode --> allow repeat of vibrations message
-   maybe remove punishment? more feedback on reward/punish feelings
-   morse timer ability (counts how long the morse msg will take and shows timer? can also send that info to others)
-   tts - toggle sender prefixes? eg German might not want Player 'says' ich bin
-   tts option to disable long texts
-   tts whispers to lower volume and pitch?
-   tts option toggle emotes
-   tts all caps higher volume?
-   tts share your own voice option to others?
-   tts option to hide texts if they are tts'd
-   tts option to tts actions too
-   tts add pitch, volume & rate options
-   tts special char filter to something more sensible
-   morser message queue (prompt queue done)
-   IoConnection reconnect etc if Intiface wasnt on when first attempting connection
-   REFACTOR MODULES SYSTEM (eh, could, but fixing spagetti isnt a feature, and its not too bad, yet...)
    -   give up window['TTS'] madness and keep all as separate as possible modules...
    -   split modules, some config base to it
-   xtoys integration? --> nope. use XToys mod, see fusam
-   Buttplug.io upgrade (disconnect/reconnect abilities)
-   blugio individual motors
-   move intiface connection to own config/settings
-   morse --> feedback message to check pronouns --> check dominance level

-   TTS/Toysync one-to-one

    -   ability to do different voices from same char
        -   "headphone message" voice and "instructions voice"
        -   mc voice and normal voice
        -   ability to send pitched/rated/volumed voice command?
            -   like a whisper - or a slower read message
    -   ability to control a subs settings
    -   Toysynced message? add commands to raise toy speed on certain spots
        -   eg "feels increase [+] slowly and slowly [+] then stop [-1] ... ... [wait 5s] and burst to speed [4]
    -   ability to disable all extra sounds, to force to listen to only to designated TTS stufff (no BC sounds, no unspecified player ID's)

## TO TEST

Things that were changed but not tested yet (should be empty...):

## Thank yous

FBC for being a great example for many code parts.
Responsive for RibbonMenu.
Themed for dark theme
Toy for logo!
Test subjects for being great test subjects ^^
