/* eslint-disable */

import { ModSDKGlobalAPI } from "bondage-club-mod-sdk";

declare global {
    var RibbonMenuMods: any[];
    var bcModSdk: ModSDKGlobalAPI | undefined;
    var PropertyShockPublishAction: (C?: Character, Item?: Item, Automatic?: boolean) => void;
    var ActivityDictionary: string[][];
    var ActivityCheckPrerequisite: (pre: string, acting: Character, acted: Character, group: AssetGroup) => boolean;
    var DialogDrawActivityMenu: (C: Character) => void;
    var CommandParse: (msg: string) => void;
    var Player: PlayerCharacter & { FBCOtherAddons?: { name: string }[] };
    var InventoryItemVulvaFuturisticVibratorHandleChat: (
        data: VibratingItemData,
        C: Character,
        Item: Item,
        LastTime: number
    ) => void;
    var WardrobeSize: number;
    var WardrobeOffset: number;
    var CraftingSlot: number;
    var CraftingOffset: number;
    var CraftingMode: "Slot" | "Item" | "Property" | "Lock" | "Name" | "Color";
    var CraftingConvertItemToSelected: (craft: any) => CraftingItem;
    var CraftingModeSet: (mode: "Slot" | "Item" | "Property" | "Lock" | "Name" | "Color") => void;
    var CraftingItemListBuild: () => void;
    var ChatRoomRegisterMessageHandler: (handler: ChatRoomMessageHandler) => void;
    var ChatRoomHTMLEntities: (text: string) => string;
    var SpeechGarble: (C: Character, text: string) => string;
    var CharacterLoadSimple: (accName: string) => Character;
    var CharacterDelete: (accName: string) => void;
    var CharacterNaked: (C: Character) => void;
    var ExtendedItemGetData: <ExtendedArchetype>(
        asset: Asset,
        Archetype: ExtendedArchetype,
        Type?: string
    ) => null | ExtendedDataLookupStruct;
    var ChatRoomChatInputRect: [number, number, number, number];
    var DialogMenuMode:
        | "dialog"
        | "items"
        | "colorDefault"
        | "colorExpression"
        | "colorItem"
        | "permissions"
        | "activities"
        | "locking"
        | "locked"
        | "extended"
        | "tighten"
        | "crafted"
        | "struggle";
    var ItemColorLoad: (
        c: Character,
        item: Item,
        x: number,
        y: number,
        width: number,
        height: number,
        includeResetButton?: boolean
    ) => void;
    var ItemColorDraw: (
        c: Character,
        group: string,
        x: number,
        y: number,
        width: number,
        height: number,
        includeResetButton?: boolean
    ) => void;
    var ItemColorOnExit: (cb: (c: Character) => void) => void;
    var ItemColorClick: (
        c: Character,
        group: string,
        x: number,
        y: number,
        width: number,
        height: number,
        includeResetButton?: boolean
    ) => void;
    var DrawCharacter: (
        C: Character,
        x: number,
        y: number,
        zoom: number,
        heightResizeAllowed?: boolean,
        canvas?: CanvasRenderingContext2D
    ) => void;
    var CharacterReleaseTotal: (C: Character) => void;
    var ServerAccountUpdate: any;
    var ChatRoomCurrentTime: () => string;
    var ElementIsScrolledToEnd: (element: string) => boolean;
    var ElementScrollToEnd: (element: string) => void;
    var RelogChatLog: HTMLDivElement | null;
    var ServerBeep: any;
    var ServerSend: (event: string, data: unknown) => void;
    var GameVersion: string;
    var GLVersion: string;
    var LoginErrorMessage: string;
    var CharacterAppearanceReturnRoom: string;
    var StruggleProgress: number;
    var StruggleProgressCurrentMinigame: any;
    var StruggleMinigames: Record<any, StruggleMinigame>;
    var StruggleLockPickDraw: (C: Character) => void;
    var StruggleProgressDexTarget: number;
    var StruggleProgressDexCurrent: number;
    var StruggleProgressFlexCircles: unknown[];
    var StruggleStrengthProcess: (Reverse: boolean) => void;
    var StruggleFlexibilityProcess: (Reverse: boolean) => void;
    var StruggleDexterityProcess: () => void;
    /** @deprecated */
    var StruggleStrength: (Reverse: boolean) => void;
    /** @deprecated */
    var StruggleFlexibility: (Reverse: boolean, Hover?: boolean) => void;
    /** @deprecated */
    var StruggleDexterity: (Reverse: boolean) => void;
    var StruggleProgressAuto: number;
    var StruggleProgressChallenge: number;
    var DialogAllowBlush: boolean;
    var DialogAllowEyebrows: boolean;
    var DialogAllowFluids: boolean;
    var InformationSheetSelection: Character | null;
    var InformationSheetLoadCharacter: (C: Character) => void;
    var CharacterLoadOnline: (data: any, sourceMemberNumber: number) => Character;
    var CraftingUpdatePreview: () => void;
    var CraftingConvertSelectedToItem: () => any;
    var CraftingSelectedItem: CraftingItem;
    var ServerPlayerIsInChatRoom: () => boolean;
    var InventoryItemMiscLoversTimerPadlockDraw: () => void;
    var InventoryItemMiscLoversTimerPadlockClick: () => void;
    var InventoryItemMiscLoversTimerPadlockExit: () => void;
    var InventoryItemMiscLoversTimerPadlockLoad: () => void;
    var InventoryItemMiscMistressTimerPadlockDraw: () => void;
    var InventoryItemMiscMistressTimerPadlockClick: () => void;
    var InventoryItemMiscMistressTimerPadlockExit: () => void;
    var InventoryItemMiscMistressTimerPadlockLoad: () => void;
    var InventoryItemMiscOwnerTimerPadlockDraw: () => void;
    var InventoryItemMiscOwnerTimerPadlockClick: () => void;
    var InventoryItemMiscOwnerTimerPadlockExit: () => void;
    var InventoryItemMiscOwnerTimerPadlockLoad: () => void;
    var InventoryItemMiscTimerPasswordPadlockDraw: () => void;
    var InventoryItemMiscTimerPasswordPadlockClick: () => void;
    var InventoryItemMiscTimerPasswordPadlockExit: () => void;
    var InventoryItemMiscTimerPasswordPadlockLoad: () => void;
    var ServerInit: () => void;
    var DialogFocusSourceItem: Item | null;
    var DialogFocusItem: Item | null;
    var CharacterNickname: (C: any) => string;
    var OnlineProfileExit: (save: boolean) => void;
    var ElementCreateTextArea: (id: string) => HTMLTextAreaElement;
    var ElementCreateInput: (id: string, type: string, value: string, maxlength: string) => HTMLInputElement;
    var ElementPosition: (id: string, x: number, y: number, w: number, h?: number) => void;
    var ElementPositionFix: (id: string, fontSize: number, x: number, y: number, w: number, h?: number) => void;
    var ElementRemove: (id: string) => void;
    var ElementValue: (id: string, newValue?: string) => string;
    var CurrentScreen: string;
    var ChatRoomCharacterItemUpdate: (C: Character, group: string) => void;
    var ChatRoomCharacterUpdate: (C: Character) => void;
    var ChatRoomCreateElement: () => void;
    var ChatRoomChatHidden: boolean;
    var ChatRoomBackground: string;
    var CharacterGetCurrent: () => Character;
    var CharacterLoadCanvas: (C: Character) => void;
    var ServerCharacterNicknameRegex: RegExp;
    var CharacterDelete: (AccountName: string) => void;
    var CharacterAppearanceStringify: (C: Character) => string;
    var CharacterAppearanceBackup: string;
    var ChatRoomCharacter: Character[];
    var ChatRoomCharacterDrawlist: Character[];
    var Character: Character[];
    var ChatRoomTargetMemberNumber: number;
    var PreferenceSubscreenList: string[];
    var PreferenceSubscreen: string;
    var PreferenceMessage: string;
    var NotificationGetTotalCount: (type: 0 | 1 | 2 | 3) => number;
    var InventoryGroupIsBlocked: (C: Character, group?: string) => boolean;
    var MainCanvas: HTMLCanvasElement;
    var TranslationLanguage: string;
    var DrawText: (text: string, x: number, y: number, color: string, backColor?: string) => void;
    var DrawTextFit: (text: string, x: number, y: number, w: number, color: string, backColor?: string) => void;
    var ChatRoomChatLog: ChatRoomChatLogEntry[];
    var DrawButton: (
        x: number,
        y: number,
        w: number,
        h: number,
        label: string,
        color: string,
        image?: string,
        hoveringText?: string,
        disabled?: boolean
    ) => void;
    var DrawCheckbox: (
        x: number,
        y: number,
        w: number,
        h: number,
        text: string,
        isChecked: boolean,
        disabled?: boolean,
        textColor?: string,
        checkImage?: string
    ) => void;
    var DrawImageResize: (image: string, x: number, y: number, w: number, h: number) => boolean;
    var MouseIn: (x: number, y: number, w: number, h: number) => boolean;
    var TextLoad: (id?: string) => void;
    var TextGet: (id: string) => string;
    var StruggleDrawLockpickProgress: (C: Character) => void;
    var StruggleLockPickOrder: null | number[];
    var SkillGetWithRatio: (C: Character, skillType: string) => number;
    var LoginRun: () => void;
    var LoginClick: () => void;
    var CurrentScreenFunctions: ScreenFunctions;
    var ServerIsConnected: boolean;
    var LoginSubmitted: boolean;
    var LoginSetSubmitted: () => void;
    var ServerConnect: () => void;
    var ServerDisconnect: (data: unknown, close?: boolean) => void;
    var PoseFemale3DCG: any[];
    var ServerSocket: any;
    var Commands: ICommand[];
    var CharacterSetActivePose: (C: Character, newPose: string | string[], force: boolean) => void;
    var CharacterSetFacialExpression: (
        C: Character,
        assetGroup: string,
        expression: string,
        timer?: number,
        color?: string | string[]
    ) => void;
    var CommonColorIsValid: (color: string | string[]) => boolean;
    var ActivityChatRoomArousalSync: (C: Character) => void;
    var ServerAppearanceBundle: (appearance: Item[]) => ItemBundle[];
    var ServerAppearanceLoadFromBundle: (
        C: Character,
        assetFamily: string,
        bundle: ItemBundle[],
        sourceMemberNumber?: number,
        appearanceFull?: boolean
    ) => boolean;
    var CharacterRefresh: (C: Character, push?: boolean, refreshDialog?: boolean) => void;
    var AppearanceExit: () => void;
    var AppearanceLoad: () => void;
    var AppearanceRun: () => void;
    var AppearanceClick: () => void;
    var CharacterAppearanceMode: string;
    var CharacterAppearanceSelection: Character;
    var DialogDrawItemMenu: (C: Character) => void;
    var InventoryGet: (C: Character, groupName: string) => Item | null;
    var DialogDraw: () => void;
    var DialogClick: () => void;
    var CurrentCharacter: Character;
    var GLDrawCanvas: HTMLCanvasElement & {
        GL: { textureCache: Map<unknown, unknown> };
    };
    var GLDrawResetCanvas: () => void;
    var WardrobeFixLength: () => void;
    var ChatRoomDrawCharacterOverlay: (C: Character, charX: number, charY: number, zoom: number, pos: number) => void;
    var ChatRoomHideIconState: number;
    var CharacterAppearanceWardrobeLoad: (C: Character) => void;
    var ChatRoomData: {
        [x: string]: any;
        Background: string;
        Name: string;
    };
    var WardrobeBackground: string;
    var WardrobeLoad: () => void;
    var WardrobeRun: () => void;
    var WardrobeClick: () => void;
    var WardrobeExit: () => void;
    var WardrobeCharacter: Character[];
    var WardrobeFastLoad: (C: Character, position: number, update?: boolean) => void;
    var WardrobeFastSave: (C: Character, position: number, update?: boolean) => void;
    var SpeechGarbleByGagLevel: (level: number, dialogText: string, ignoreOOC?: boolean) => string;
    var SpeechGetTotalGagLevel: (C: Character) => number;
    var ChatRoomResize: (load: boolean) => void;
    var ChatRoomRun: () => void;
    var ChatRoomClick: () => void;
    var ServerBundledItemToAppearanceItem: (assetFamily: string, item: ItemBundle) => Item | null;
    var DrawBackNextButton: (
        x: number,
        y: number,
        w: number,
        h: number,
        label: string,
        color: string,
        image?: string,
        labelPrevious?: () => string,
        labelNext?: () => string,
        disabled?: boolean,
        arrowWidth?: number,
        tooltipPosition?: any
    ) => void;
    var MouseX: number;
    var MouseY: number;
    var ActivitySetArousal: (C: Character, progress: number) => void;
    var ActivitySetArousalTimer: (
        C: Character,
        activity: Record<string, unknown>,
        zone: string,
        progress: number
    ) => void;
    var ActivityTimerProgress: (C: Character, progress: number) => void;
    var TimerProcess: (timestamp: number) => void;
    var ChatRoomListManipulation: (list: number[] | null, adding: boolean, memberNumber: string | number) => void;
    var ServerOpenFriendList: () => void;
    var ServerAccountBeep: (data: Record<string, unknown>) => void;
    var ServerClickBeep: () => void;
    var BCX_Loaded: boolean;
    var BCX_SOURCE: string;
    var StartBcUtil: () => void;
    var PreferenceSubscreenBCESettingsLoad: () => void;
    var PreferenceSubscreenBCESettingsExit: () => void;
    var PreferenceSubscreenBCESettingsRun: () => void;
    var PreferenceSubscreenBCESettingsClick: () => void;
    var OnlineGameAllowChange: () => boolean;
    var ChatRoomKeyDown: (event: KeyboardEvent) => void;
    var FriendListBeepLog: any[];
    var FriendListModeIndex: number;
    var FriendListShowBeep: (id: number) => void;
    var DialogCanUnlock: (C: Character, item: Item) => boolean;
    var CommandExecute: (msg: string) => void;
    var ManagementMistress: any;
    var CommonSetScreen: (category: string, screen: string) => void;
    var ChatRoomLeashPlayer: number;
    var ChatRoomClearAllElements: () => void;
    var bceGotoRoom: (room: string) => void;
    var DialogLeave: () => void;
    var ChatRoomStart: (
        space: string,
        game: string,
        leaveRoom: string | null,
        leaveSpace: string | null,
        background: string,
        bgTagList: string[]
    ) => void;
    var BackgroundsTagList: string[];
    var ChatRoomJoinLeash: string;
    var bceStartClubSlave: () => Promise<void>;
    var bceSendToClubSlavery: () => void;
    var bceCanSendToClubSlavery: () => boolean;
    var ChatRoombceSendToClubSlavery: () => void;
    var ChatRoombceCanSendToClubSlavery: () => boolean;
    var CharacterSetCurrent: (C: Character) => void;
    var CommonCSVCache: { [key: string]: string[][] };
    var CharacterBuildDialog: (C: Character, csv: string[][]) => void;
    var ChatRoomAppendChat: (div: HTMLElement) => void;
    var ChatRoomMessage: (data: ServerChatRoomMessage) => void;
    var ChatRoomSyncPose: (data: { MemberNumber: number; Character?: Character; Pose: string | string[] }) => void;
    var ChatRoomSendChat: (skipHistory?: boolean) => void;
    var Asset: Asset[];
    var AssetGroup: AssetGroup[];
    var CharacterDecompressWardrobe: (wardrobe: ItemBundle[][] | string) => ItemBundle[][];
    var CharacterCompressWardrobe: (wardrobe: ItemBundle[][]) => string;
    var CharacterAppearanceWardrobeOffset: number;
    var GameRun: (time: DOMHighResTimeStamp) => void;
    var NotificationRaise: (eventType: "Beep", data: Record<string, unknown>) => void;
    var NotificationReset: (eventType: "Beep") => void;
    var AudioPlayInstantSound: (src: string, volume?: number) => void;
}
