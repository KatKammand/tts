// ==UserScript==
// @name TTS
// @namespace https://www.bondageprojects.com/
// @version 0.3.0
// @description Club TTS and Morse mod
// @author
// @match https://bondageprojects.elementfx.com/*
// @match https://www.bondageprojects.elementfx.com/*
// @match https://bondage-europe.com/*
// @match https://www.bondage-europe.com/*
// @run-at document-end
// @grant none
// ==/UserScript==

(function () {
    "use strict";
    var script = document.createElement("script");
    script.type = "module";
    script.setAttribute("crossorigin", "anonymous");
    script.src = "https://katkammand.gitlab.io/tts/tts.min.js";

    document.head.appendChild(script);
})();
