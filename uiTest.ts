import { mo_activateConnection, isIoConnectionNeeded } from "./src/common/ioConnection";
import { openSettings } from "./src/common/settings";
import { toysyncInitializer } from "./src/features/toysync/toySync";
import { loadAllSettings } from "./src/common/config";
import { addTtsListeners } from "./src/features/tts/ttsQueue";
import { getTtsActive, getTtsConfigValue } from "./src/features/tts/config";
import { initKokoroTts } from "./src/features/tts/kokoro-tts";
import { loadVoices } from "./src/features/tts/ttsVoices";

new EventSource("/esbuild").addEventListener("change", () => location.reload());

window["Player"] = {
    MemberNumber: 1,
    AccountName: "testUser",
    Appearance: [],
} as PlayerCharacter;

window["PreferenceSubscreenExtensionsClear"] = () => {
    console.log("PreferenceSubscreenExtensionsClear called");
};

const init = async () => {
    const voices = speechSynthesis.getVoices();
    if (!voices || voices.length === 0) {
        setTimeout(init, 200);
        return;
    }

    loadAllSettings();
    await addTtsListeners();
    toysyncInitializer();

    if (isIoConnectionNeeded()) {
        await mo_activateConnection();
    }

    if (getTtsActive() && getTtsConfigValue("ttsKokoroVoicesActive")) {
        console.debug("KOKORO TTS ACTIVE");
        await initKokoroTts();
        await loadVoices();
    }

    openSettings();
};
const btn = document.createElement("button");
btn.textContent = "Open Settings";
btn.onclick = openSettings;
document.body.append(btn);

init();
