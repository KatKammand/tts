import { openDB } from "idb"; // ✅ Import as an ES module

// IndexedDB Setup
const DB_NAME = "TTS_Kokoro_workerCacheDB";
const STORE_NAME = "requests";

async function openDatabase() {
    return openDB(DB_NAME, 1, {
        upgrade(db) {
            db.createObjectStore(STORE_NAME);
        },
    });
}

async function storeResponse(url: string, blob: Blob, contentType: string) {
    const db = await openDatabase();
    const tx = db.transaction(STORE_NAME, "readwrite");
    await tx.store.put({ blob, contentType }, url);
    await tx.done;
    self.postMessage({ type: "response", result: `Stored ${url} in IndexedDB` });
}

async function getCachedResponse(url: string) {
    const db = await openDatabase();
    return db.get(STORE_NAME, url);
}

// ✅ Monkey-patch fetch to cache all requests
export function activateFetchInterceptor() {
    const originalFetch = self.fetch;
    self.fetch = async function (input: RequestInfo | URL, options?: RequestInit) {
        let url: string;

        if (typeof input === "string") {
            url = input; // ✅ If input is a string, use it directly
        } else if (input instanceof Request) {
            url = input.url; // ✅ If input is a Request object, get its URL
        } else if (input instanceof URL) {
            url = input.toString(); // ✅ If input is a URL object, convert it to string
        } else {
            throw new Error("Unsupported fetch input type");
        }

        console.log("Intercepting fetch:", url);
        self.postMessage({ type: "response", result: `Intercepting fetch for ${url}` });

        // 🔍 Check if already cached
        const cachedData = await getCachedResponse(url);
        if (cachedData) {
            console.log(`Serving ${url} from IndexedDB cache`);
            self.postMessage({ type: "response", result: `Serving ${url} from IndexedDB cache` });
            return new Response(cachedData.blob, {
                status: 200,
                statusText: "OK",
                headers: { "Content-Type": cachedData.contentType },
            });
        }

        // 🌍 Fetch from network
        console.log(`Fetching ${url} from network...`);
        const response = await originalFetch(input, options);
        const blob = await response.blob();

        // 💾 Store in IndexedDB
        const contentType = response.headers.get("Content-Type") || "application/octet-stream";
        await storeResponse(url, blob, contentType);

        return new Response(blob, {
            status: 200,
            statusText: "OK",
            headers: { "Content-Type": contentType },
        });
    };
}

