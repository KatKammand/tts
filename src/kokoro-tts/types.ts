export type WorkerAudioResponse = {
    type: "complete";
    requestId: string;
    text: string;
    audio: string;
};

export type WorkerAudioRequest = {
    type: "generate";
    requestId: string;
    text: string;
    voiceId: string;
}
