import { KokoroTTS } from "kokoro-js";
import { WorkerAudioRequest, WorkerAudioResponse } from "./types";
import { activateFetchInterceptor } from "./indbCache";

type AudioResults = Omit<WorkerAudioResponse, "type">;

activateFetchInterceptor()

let TTS_SYSTEM: KokoroTTS;

let IS_LOADING = false;
let IS_READY = false;
const model_id = "onnx-community/Kokoro-82M-v1.0-ONNX";

async function detectWebGPU() {
    try {
        // @ts-ignore
        const adapter = await navigator?.gpu?.requestAdapter();
        return !!adapter;
    } catch (e) {
        return false;
    }
}

class CreationQueue {
    private queue: (() => Promise<void>)[] = [];
    private isRunning = false;

    async generateAudioWithQueue(text: string, voiceId: string, requestId: string): Promise<AudioResults> {
        return new Promise<AudioResults>((resolve, reject) => {
            const task = async () => {
                try {
                    //@ts-ignore
                    const audio = await TTS_SYSTEM.generate(text, { voice: voiceId });
                    resolve({ text, requestId, audio: URL.createObjectURL(audio.toBlob()) });
                } catch (error) {
                    reject(error);
                } finally {
                    this.isRunning = false;
                    this.processNext();
                }
            };

            this.queue.push(task);
            this.processNext();
        });
    }

    private processNext() {
        if (this.isRunning || this.queue.length === 0) return;
        this.isRunning = true;
        const nextTask = this.queue.shift();
        if (nextTask) nextTask();
    }
}
const CREATION_QUEUE = new CreationQueue();

async function createWorker() {
    self.postMessage({ type: "response", result: "Starting kokoro tts" });
    if (IS_LOADING) {
        console.log("KOKORO IS LOADING");
        return;
    } else if (IS_READY) {
        console.log("KOKORO ALREADY READY");
        return;
    } else {
        IS_LOADING = true;
    }

    const device = (await detectWebGPU()) ? "webgpu" : "wasm";
    self.postMessage({ type: "device", device });
    console.log("Kokoro tts will use device: ", device);
    self.postMessage({ type: "response", result: "will use " + device });

    try {
        console.log("kokoro.web.js loaded");
        self.postMessage({ type: "response", result: "kokoro web.js loaded" });

        TTS_SYSTEM = await KokoroTTS.from_pretrained(model_id, {
            dtype: device === "wasm" ? "q8" : "fp32",
            device,
        });
        self.postMessage({ type: "response", result: "TTS_SYSTEM loaded" });
        if (!TTS_SYSTEM) {
            self.postMessage({ type: "response", result: "Failed to load TTS system" });
            console.error("Failed to load TTS system");
            return false;
        }

        self.addEventListener("message", async (e) => {
            const { text, voiceId, requestId } = e.data as WorkerAudioRequest;

            const result = await CREATION_QUEUE.generateAudioWithQueue(text, voiceId, requestId);
            if (!result?.audio) {
                self.postMessage({ type: "error", data: result });
            } else {
                self.postMessage({ type: "complete", requestId, text, audio: result.audio } as WorkerAudioResponse);
            }

        });

        self.postMessage({ type: "ready", voices: TTS_SYSTEM.voices });

        IS_READY = true;
        IS_LOADING = false;

        return true;
    } catch (e) {
        console.error(e);
    }
    IS_LOADING = false;
    return false;
}

// Listen for messages from the main thread


createWorker();

