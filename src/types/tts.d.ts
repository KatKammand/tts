export type ValueOf<T> = T[keyof T];

export type ChatMsgHandlerProps = {
    msgData: ServerChatRoomMessage;
    senderChar: Character;
    msgCurrent: string;
    /** metadata will be { senderName: 'Someone' | nickname | membername } in the ones Chat/Emote/Whisper */
    metadata: IChatRoomMessageMetadata;
};

export type Enumerate<N extends number, Acc extends number[] = []> = Acc["length"] extends N
    ? Acc[number]
    : Enumerate<N, [...Acc, Acc["length"]]>;

export type IntRange<F extends number, T extends number> = Exclude<Enumerate<T>, Enumerate<F>>;
