import { SlotOptions, ToySyncDevice, getDeviceByName } from "../../common/ioDevices";
import {
    CONF_PROPS,
    TOYSYNC_SLOTTED_DEVICES,
    getConfigValue,
    getToySyncConfigValue,
    setToySyncConfigValue,
} from "../../common/config";
import { strCompare } from "../../common/utils";

const getSlottedDevices = () => getToySyncConfigValue("slottedDevices");
const setSlottedDevices = (newSlottedDevices: TOYSYNC_SLOTTED_DEVICES, save = true) =>
    setToySyncConfigValue("slottedDevices", newSlottedDevices, save);

export const getToySyncActive = () => getConfigValue(CONF_PROPS.TOYSYNC_ISACTIVE);

export const getSlotsDevices = (slotname: string) => getSlottedDevices()?.[slotname] || [];

export const getSlottedDevicesList = (): Array<[string, string[]]> => Object.entries(getSlottedDevices());

export const getDevicesSlot = (deviceName: string): SlotOptions => {
    const slottedDevicesList = getSlottedDevicesList();
    const foundSlot = slottedDevicesList.find((s) => {
        const [, devices] = s;
        return devices.some((d) => strCompare(d, deviceName));
    });
    return foundSlot?.[0] as SlotOptions;
};

export const addDeviceToSlot = (slot: SlotOptions, device: ToySyncDevice, save = true) => {
    const slotLower = slot.toLowerCase() as SlotOptions;
    const slottedDevices = getSlottedDevices();

    if (device.SlotName) {
        removeDeviceFromSlot(device.SlotName, device, false);
    }

    const deviceName = device?.bPlugDevice?.Name;
    if (!deviceName) return;

    device.SlotName = slotLower;
    const currentSlotsDevices = slottedDevices[slotLower] || [];

    slottedDevices[slotLower] = [...currentSlotsDevices, deviceName];
    setSlottedDevices(slottedDevices, save);
};

export const removeDeviceFromSlot = (slot: SlotOptions, device: ToySyncDevice, save = true) => {
    const slotLower = slot.toLowerCase() as SlotOptions;
    const slottedDevices = getSlottedDevices();
    const currentSlotsDevices = slottedDevices[slotLower];
    if (!currentSlotsDevices) return;

    const deviceName = device?.bPlugDevice?.Name;
    if (!deviceName) return;

    device.SlotName = null;

    slottedDevices[slotLower] = currentSlotsDevices.filter((d) => !strCompare(d, deviceName));
    setSlottedDevices(slottedDevices, save);
};

export const fbcImport = (fbcDevices: { Name: string; SlotName: SlotOptions }[]) => {
    fbcDevices.forEach((d) => {
        //no need to save, this is initialized from fbc and only done so if nothing's saved on our end.
        //any changes after this will cause a save. and even this is never saved, meh not much lost
        const device = getDeviceByName(d.Name);
        if (device) {
            addDeviceToSlot(d.SlotName, device, false);
        }
    });
};
