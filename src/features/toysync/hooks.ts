import { CONF_PROPS, getConfigValue, mo_toggleToySyncActive } from "../../common/config";
import { HOOK_PRIORITIES } from "../../common/constants";
import { HookFunc, ItemUpdated, hookWrap } from "../../common/hooks";
import { mo_activateConnection } from "../../common/ioConnection";
import { HOOK_GROUPS, MODULES, hookFunction, removeHooksByToggle } from "../../common/sdk";
import { getPlayerId } from "../../common/utils";
import { checkAndVibrateSlot } from "./toySync";

//Others triggered item updates
const RoomItemSync: HookFunc = (args: [{ Source: number; Item: ItemUpdated; }]) => {
    if (args.length !== 1 || !getConfigValue(CONF_PROPS.TOYSYNC_ISACTIVE)) return;

    const [data] = args;
    if (!data || !data?.Item) return;
    const { Target, Group, Property } = data.Item;
    const newIntensity = Number(Property?.Intensity);
    if (Target !== getPlayerId() || !Group) return;

    checkAndVibrateSlot(Group, newIntensity);

    return args;
};

//Self triggered item updates
const SelfItemSync: HookFunc = (args: [string, ItemUpdated]) => {
    const [serverSent, itemUpdated] = args;
    if (serverSent !== "ChatRoomCharacterItemUpdate" || !itemUpdated || !getConfigValue(CONF_PROPS.TOYSYNC_ISACTIVE))
        return;

    const { Target, Group, Property } = itemUpdated;
    const newIntensity = Number(Property?.Intensity);
    if (Target !== getPlayerId() || !Group) return;

    checkAndVibrateSlot(Group, newIntensity);

    return args;
};

function othersToy(args: [{ Source: number; Item: ItemUpdated; }], next) {
    hookWrap(RoomItemSync, args, next);
}
function selfToy(args: [string, ItemUpdated | ServerAccountBeepRequest], next) {
    hookWrap(SelfItemSync, args, next);
}

export const registerToysyncHooks = () => {
    if (!getConfigValue(CONF_PROPS.TOYSYNC_ISACTIVE)) return;
    //Toy updates by others
    hookFunction(HOOK_GROUPS.TOYSYNC_ISACTIVE, MODULES.TOYSYNC, "ChatRoomSyncItem", HOOK_PRIORITIES.Observe, othersToy);

    //ATM only needed for self toggled
    hookFunction(HOOK_GROUPS.TOYSYNC_ISACTIVE, MODULES.TOYSYNC, "ServerSend", HOOK_PRIORITIES.Observe, selfToy);
};

export const toggleToySyncActive = async (): Promise<boolean> => {
    const isActive = mo_toggleToySyncActive();
    if (isActive) {
        registerToysyncHooks();
        await mo_activateConnection();
    } else {
        removeHooksByToggle(HOOK_GROUPS.TOYSYNC_ISACTIVE);
    }
    return isActive;
};
