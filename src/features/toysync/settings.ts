import { connectionToggler, mo_ioReconnect } from "../../common/ioConnection";
import { SlotOptions, getDeviceByName, getDeviceNamesList } from "../../common/ioDevices";
import {
    SelectorChangeHandler,
    createBtnContainer,
    createButton,
    createCheckbox,
    createDivContainer,
    createInput,
    createOptionsList,
    createTextSpan,
} from "../../common/components";
import { getToySyncActive } from "./config";
import { desyncDevice, getSlotOptions, updateToySlot } from "./toySync";
import { CONF_PROPS, defaultIntifaceUrl, getConfigValue, setConfigValue } from "../../common/config";
import { toggleToySyncActive } from "./hooks";

export const TOYSYNC_TAB_REFRESH_EVENT = "toysync-settings-refresh";

const refreshEvent = new Event(TOYSYNC_TAB_REFRESH_EVENT, { bubbles: true });

// RIGHT INITIALIZATION WITH THE OTHER THING NOT WORKING ATM...

const createToySyncList = (toyName: string) => {
    const div = createDivContainer();
    div.textContent = "Select slot for: " + toyName.toUpperCase();

    const slots = [...getSlotOptions()].map((s) => ({ id: s, value: s }));
    slots.unshift({ id: "NONE" as SlotOptions, value: "NONE" as SlotOptions });
    const changeHandler: SelectorChangeHandler = (slot) => {
        const newSlot = slot.value as SlotOptions | "NONE";
        if (newSlot === "NONE") {
            desyncDevice(toyName);
        } else {
            updateToySlot(newSlot, toyName);
        }
    };
    const deviceSlot = getDeviceByName(toyName)?.SlotName || "NONE";
    div.appendChild(createOptionsList(slots, deviceSlot, changeHandler));
    return div;
};

const createToysList = () => {
    return createDivContainer(getDeviceNamesList().map((n) => createToySyncList(n)));
};

const intifaceUrlInput = () => {
    const input = createInput(getConfigValue(CONF_PROPS.INTIFACE_URL));

    const updateIntifaceUrl = (newUrl: string) => {
        if (getConfigValue(CONF_PROPS.INTIFACE_URL) === newUrl) return;

        setConfigValue(CONF_PROPS.INTIFACE_URL, newUrl);
        mo_ioReconnect();
        input.dispatchEvent(refreshEvent);
    };

    const onSave = (e: Event) => {
        e.preventDefault();
        updateIntifaceUrl(input.value);
    };
    const onReset = (e: Event) => {
        e.preventDefault();
        updateIntifaceUrl(defaultIntifaceUrl);
    };

    const saveBtn = createButton("Save Url", onSave);
    const resetBtn = createButton("Reset Url", onReset);

    const btnContainer = createBtnContainer();
    btnContainer.append(saveBtn, resetBtn);
    return createDivContainer(
        [createTextSpan(`Intiface Url - desktop default is '${defaultIntifaceUrl}'. Current:`), input, btnContainer],
        "tts-boxed-container"
    );
};

/** Container for all toysync settings */
export const toysyncSettings = () => {
    const div = createDivContainer();
    const toggleToysync = async () => {
        await connectionToggler(toggleToySyncActive);
        div.dispatchEvent(refreshEvent);
    };

    const moduleDescriptors = [
        createTextSpan("Use Buttplug.io & Intiface Central to connect your characters toys, to your IRL toys."),
        createTextSpan(
            "See TTS readme for more on this. You need Intiface Central on your desktop: https://intiface.com/central/"
        ),
        createTextSpan(
            "When you add or remove device to Intiface, refresh the settings UI by clicking the ToySync Tab."
        ),
    ];

    const toggleBoxes = createDivContainer(
        [createCheckbox(getToySyncActive(), toggleToysync, "Toysync Active - switch toysync module on/off")],
        "tts-toggle-container"
    );

    div.append(...[...moduleDescriptors, toggleBoxes, intifaceUrlInput(), createToysList()]);

    return div;
};
