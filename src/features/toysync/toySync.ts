import { CONF_PROPS, getConfigValue } from "../../common/config";
import { SyncIntensities } from "../../common/constants";
import {
    SlotOptions,
    ToySyncDevice,
    getDeviceByName,
    subscribeToAddDevice,
    subscribeToRemoveDevice,
    vibrateDevice,
} from "../../common/ioDevices";
import { intensityToVibrateLv, strCompare } from "../../common/utils";
import {
    addDeviceToSlot,
    fbcImport,
    getDevicesSlot,
    getSlotsDevices,
    getSlottedDevicesList,
    removeDeviceFromSlot,
} from "./config";

const toySyncVibrate = async (device: ToySyncDevice, intenisty: SyncIntensities) => {
    const vibratelvl = intensityToVibrateLv(intenisty) || 0;
    await vibrateDevice(device, vibratelvl);
};

const createSlotOptions = (): SlotOptions[] => {
    //@ts-ignore
    const assetGroups = (window?.AssetGroup as AssetGroup[]) || [];
    const possibleSlots = assetGroups.filter((a) => {
        if (!a.IsItem()) return false;
        return !!a.Asset.find((i) => i.AllowEffect?.includes("Vibrating"));
    });
    let res = possibleSlots.map((a) => a.Name?.toLowerCase()) as SlotOptions[];
    if (res.length === 0) {
        res = [
            "itemfeet",
            "itemlegs",
            "itemvulva",
            "itemvulvapiercings",
            "itembutt",
            "itempelvis",
            "itemnipples",
            "itemnipplespiercings",
            "itembreast",
            "itemdevices",
        ];
    }
    return res;
};

const SLOT_OPTIONS = createSlotOptions();

export const getSlotOptions = () => [...SLOT_OPTIONS];

const checkIsSlotValid = (slot: SlotOptions) => {
    return getSlotOptions().find((s) => strCompare(s, slot));
};

const getSlotNameDevice = (slotName: SlotOptions): ToySyncDevice[] => {
    const slotLower = slotName.toLowerCase() as SlotOptions;
    return getSlotsDevices(slotLower).map((d) => getDeviceByName(d)) || [];
};

export const checkIntensity = (newIntensity: number): boolean => newIntensity <= 3 && newIntensity >= -1;

const getFbcSettingKey = () => `bce.settings.${Player?.AccountName}`;

const initDeviceIntenisty = (slotName: SlotOptions): SyncIntensities => {
    let grouName = slotName as AssetGroupName;
    const item = Player?.Appearance.find((i) => strCompare(i.Asset?.Group?.Name, grouName));
    return item?.Property?.Intensity ?? (-1 as SyncIntensities);
};

const initSlottedDevice = (slotName: SlotOptions, device: ToySyncDevice) => {
    device.SlotName = slotName;
    const initIntenisty = initDeviceIntenisty(device.SlotName);
    if (initIntenisty !== -1) {
        toySyncVibrate(device, initIntenisty);
    }
};

export const handleNewDeviceAdded = (device: ToySyncDevice) => {
    const foundSlot: SlotOptions = getDevicesSlot(device?.bPlugDevice?.Name);
    if (!foundSlot) return;

    initSlottedDevice(foundSlot, device);
};

const handleDeviceRemoved = (device: ToySyncDevice) => {
    const foundSlot: SlotOptions = getDevicesSlot(device?.bPlugDevice?.Name);
    if (!foundSlot) return;

    removeDeviceFromSlot(foundSlot, device);
};

export const checkAndVibrateSlot = (group: string, newIntensity: number) => {
    if (!getConfigValue(CONF_PROPS.TOYSYNC_ISACTIVE)) return;
    if (!checkIntensity(newIntensity)) return;

    const slotName = group as SlotOptions;
    if (!checkIsSlotValid(slotName)) return;

    const syncDevices = getSlotNameDevice(slotName);
    if (syncDevices?.length > 0) {
        syncDevices.forEach((d) => {
            if (d?.LastIntensity !== newIntensity) {
                toySyncVibrate(d, newIntensity as SyncIntensities);
            }
        });
    }
};

export const desyncDevice = (deviceName: string) => {
    const device = getDeviceByName(deviceName);
    if (!device?.SlotName) return;

    removeDeviceFromSlot(device.SlotName, device);
};

export const updateToySlot = (newSlot: SlotOptions, toyName: string) => {
    const device = getDeviceByName(toyName);
    if (!device || !checkIsSlotValid(newSlot)) return;

    const slotLower = newSlot.toLowerCase() as SlotOptions;

    addDeviceToSlot(slotLower, device);

    const initIntenisty = initDeviceIntenisty(slotLower);
    if (initIntenisty !== -1) {
        toySyncVibrate(device, initIntenisty);
    }

    return;
};

const getFbcSettings = (): { Name: string; SlotName: SlotOptions }[] => {
    try {
        let settings = JSON.parse(localStorage.getItem(getFbcSettingKey()));
        try {
            //this part works only if FBC active too.
            //@ts-ignore
            const onlineSettings = JSON.parse(LZString.decompressFromBase64(Player?.OnlineSettings?.BCE) || null);
            if (
                onlineSettings?.buttplugDevices &&
                (onlineSettings?.version >= settings?.version ||
                    (typeof settings?.version === "undefined" && typeof onlineSettings?.version !== "undefined"))
            ) {
                settings = onlineSettings;
            }
        } catch (e) {}
        const res = JSON.parse(settings?.buttplugDevices);
        return Array.isArray(res) ? res : [];
    } catch (e) {
        return [];
    }
};

//USE THIS ONLY IN BEGINNING TO READ FBC SLOTS --> @TODO remove completely?
export const toysyncInitializer = () => {
    if (!getConfigValue(CONF_PROPS.TOYSYNC_ISACTIVE)) {
        return;
    }

    subscribeToAddDevice(handleNewDeviceAdded);
    subscribeToRemoveDevice(handleDeviceRemoved);

    const slottedDevices = getSlottedDevicesList();
    if (Object.values(slottedDevices).length > 0) return;

    //none stored, so see if fbc has some stored that match currently connected devices
    const fbcStoredDevicesSlots = getFbcSettings();
    if (fbcStoredDevicesSlots.length > 0) fbcImport(fbcStoredDevicesSlots);
};
