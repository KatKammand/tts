import { alphabetCaps, alphabetMorseString, mo_charToMorse, mo_morseToChar } from "./constants";
import { BPlugDevice, getIsIoConnected, mo_activateConnection, mo_vibrateDevice } from "../../common/ioConnection";
import { mo_promptUser } from "./promptUI";
import { getName, getRandomNumber, mo_PsWait, mo_sendAction } from "../../common/utils";
import { getDeviceByName, morseDevice } from "../../common/ioDevices";
import {
    toggleMorseVibrations,
    getWaitTime,
    getMorseConfigValue,
    getMorseVibrationsActive,
    getMorseDeviceName,
} from "./config";

let mo_isMorsing = false;

type MorseChars = "/" | " " | "-" | ".";

const getMorseDevice = () => {
    return getDeviceByName(getMorseDeviceName());
};

const mo_charVibrate = async (wait: number, device: BPlugDevice) => {
    const intenisty = getMorseConfigValue("intenisty") as number;
    await mo_vibrateDevice(intenisty, device);
    await mo_PsWait(wait);
    await mo_vibrateDevice(0, device);
    await mo_PsWait(getWaitTime("morse"));
};

//atm just removes undefined symbols --> should be fine right?
const mo_wordConverter = (tString: string, isMorse = false): string => {
    const refObj = isMorse ? mo_morseToChar : mo_charToMorse;
    const joiner = isMorse ? "" : " ";
    const cArr = isMorse ? tString.split(" ") : [...tString];

    return cArr
        .map((c) => refObj[c.toUpperCase()])
        .filter((c) => !!c)
        .join(joiner);
};

export const mo_textConverter = (text = "", isMorse = false): string => {
    const splitter = isMorse ? "/" : " ";
    const joiner = isMorse ? " " : "/";
    let newText = text;
    if (typeof text !== "string") {
        newText = "" + text;
    }

    return newText
        .split(splitter)
        .map((s) => mo_wordConverter(s, isMorse))
        .join(joiner);
};

const mo_morseToVibrations = async (text: string, isMorse = false) => {
    if (!getMorseVibrationsActive() || mo_isMorsing) return;

    const morse = isMorse ? text : mo_textConverter(text, isMorse);
    const chars = [...morse] as MorseChars[];
    if (chars.length === 0) return;
    const device = getMorseDevice();
    if (!device) return;

    //@TODO --> atm just ignores conflicts, later make morse queue?
    mo_isMorsing = true;

    morseDevice(
        async (d) => {
            for (const c of chars) {
                switch (c) {
                    //char morsed
                    case " ":
                        await mo_PsWait(getWaitTime(" "));
                        break;
                    //word morsed
                    case "/":
                        await mo_PsWait(getWaitTime("/"));
                        break;
                    default:
                        await mo_charVibrate(getWaitTime(c), d);
                }
            }
        },
        device.bPlugDevice?.Name
    );

    mo_isMorsing = false;
};

//-.- -. . . .-.. --> kneel (increas readability?)
//@TODO - training task just vibrations atm,
//if visibility is 2 --> require the morse instead in submit?
//Reward command? & Punish command

// ________ TASKS __________

export const mo_train_type = async () => {
    const toMorse = await mo_promptUser("Type text to morse");
    await mo_morseToVibrations(toMorse);
};

export const mo_morseAndResponse = async (text: string) => {
    const tUp = text.toUpperCase();
    await mo_morseToVibrations(tUp);
    const response = await mo_promptUser("Type morsed text. \n" + alphabetMorseString);
    const isRight = response?.toUpperCase() === tUp;
    const deviceName = getMorseDevice()?.bPlugDevice?.Name;
    if (isRight) {
        window.alert("CORRECT! GOOD GIRL");
        await morseDevice(moVibrateReward, deviceName);
    } else {
        window.alert(
            "Incorrect. More training needed, you can do it! \n Answer was: " + tUp + " : " + mo_textConverter(tUp)
        );
        await morseDevice(moVibratePunish, deviceName);
    }
    return isRight;
};

export const mo_train_testLetter = async (count = 1, randomCount = false) => {
    let letters = "";
    const maxCount = randomCount ? count : getRandomNumber(count);
    for (let i = 0; i < maxCount; i++) {
        letters += alphabetCaps[Math.floor(Math.random() * alphabetCaps.length)];
    }
    return await mo_morseAndResponse(letters);
};

export const mo_train_testLetterTask = async (times: number, charCount = 1, randomCount = false) => {
    let right = 0;
    for (let count = 0; count < times; count++) {
        if (await mo_train_testLetter(charCount, randomCount)) {
            right += 1;
        }
    }
    const text = `${getName()} got ${right}/${times} correct in letter task.`;
    mo_sendAction(text);
};

export const mo_msgTask_morse = async (chatText: string) => {
    if (typeof chatText !== "string") return;

    const isRight = await mo_morseAndResponse(chatText);
    const text = `${getName()} ${isRight ? "succeeds" : "fails"} the morse test!`;
    mo_sendAction(text);
};

export const msg_morse = async (chatText: string) => {
    if (typeof chatText !== "string") return;
    await mo_morseToVibrations(chatText.toUpperCase());
};

export const mo_toggleMorseVibrate = async (): Promise<boolean> => {
    const isActive = toggleMorseVibrations();
    if (isActive) {
        if (!getIsIoConnected()) {
            await mo_activateConnection();
        }
    }
    return isActive;
};

export const moVibrateReward = async (bDevice: BPlugDevice) => {
    if (!bDevice) return;

    await mo_vibrateDevice(0.1, bDevice);
    await mo_PsWait(200);
    await mo_vibrateDevice(0.3, bDevice);
    await mo_PsWait(400);
    await mo_vibrateDevice(0.5, bDevice);
    await mo_PsWait(600);
    await mo_vibrateDevice(0.7, bDevice);
    await mo_PsWait(800);
    await mo_vibrateDevice(0.9, bDevice);
    await mo_PsWait(600);
    await mo_vibrateDevice(0, bDevice);
};

export const moVibratePunish = async (bDevice: BPlugDevice) => {
    if (!bDevice) return;

    await mo_vibrateDevice(0.5, bDevice);
    await mo_PsWait(300);
    await mo_vibrateDevice(0, bDevice);
};

export const moVibrateDevice = async (action: (device: BPlugDevice) => Promise<void>, device: BPlugDevice) => {
    if (!device) return;
    mo_isMorsing = true;
    await action(device);
    mo_isMorsing = false;
};
