import {
    SelectorChangeHandler,
    createCheckbox,
    createDivContainer,
    createOptionsList,
    createTextSpan,
} from "../../common/components";
import { toggleMorse } from "../../common/config";
import { connectionToggler } from "../../common/ioConnection";
import { getDeviceNamesList } from "../../common/ioDevices";
import {
    getMorseActive,
    getMorseDeviceName,
    getMorseVibrationsActive,
    getShareDevices,
    setMorseDeviceName,
    toggleShareDevices,
} from "./config";
import { mo_chatTriggers, registerMorseHooks, removeMorseHooks } from "./hooks";
import { mo_toggleMorseVibrate } from "./morse";

export const MORSE_TAB_REFRESH_EVENT = "morse-settings-refresh";

const morseDeviceSelector = () => {
    const deviceNames = ["NONE", ...getDeviceNamesList()];
    const onChange: SelectorChangeHandler = (option) => {
        const deviceName = option.value;
        if (deviceName === "NONE") {
            setMorseDeviceName("");
        } else {
            setMorseDeviceName(deviceName);
        }
    };
    return createDivContainer([
        createTextSpan("Select Device to Morse to if Morse vibrations is active"),
        createOptionsList(
            deviceNames.map((n) => ({ value: n, id: n })),
            getMorseDeviceName(),
            onChange
        ),
    ]);
};

/** Container for all morse settings */
export const morseSettings = () => {
    const div = createDivContainer();

    const refreshTab = () => div.dispatchEvent(new Event(MORSE_TAB_REFRESH_EVENT, { bubbles: true }));

    const morseBasicToggler = () => {
        const isActive = toggleMorse();
        //if base module is off, turn the vibrations module off too
        if (!isActive && getMorseVibrationsActive()) connectionToggler(mo_toggleMorseVibrate);

        if (isActive) {
            registerMorseHooks();
        } else {
            removeMorseHooks();
        }

        refreshTab();
    };
    const toggleMorseVibrate = async () => {
        await connectionToggler(mo_toggleMorseVibrate);
        refreshTab();
    };

    const moduleDescriptors = [
        createTextSpan("Use Buttplug.io & Intiface Central to connect your characters toys, to your IRL toys."),
    ];

    const toggleBoxes = createDivContainer(
        [
            createCheckbox(getMorseActive(), morseBasicToggler, "Morse Active - switch morse module on/off"),
            createTextSpan(
                "Use Buttplug.io & Intiface Central to connect a IRL toys and allow others to send morse messages as vibrations."
            ),
            createCheckbox(
                getMorseVibrationsActive(),
                toggleMorseVibrate,
                "Morse Vibrations Active - On: Intiface connection created and Morse messages are vibrated to selected device."
            ),
            createCheckbox(
                getShareDevices(),
                toggleShareDevices,
                "Allow Morse device sharing - other users can ask for your device name used for morse vibrations"
            ),
        ],
        "tts-toggle-container"
    );

    const helpTexts = createDivContainer();
    helpTexts.textContent = "MORSE CHAT COMMANDS THAT CAN BE RECEIVED WHEN THIS MODULE IS ACTIVE:";
    helpTexts.append(...Object.values(mo_chatTriggers).map((t) => createTextSpan(t.help)));

    div.append(...[...moduleDescriptors, toggleBoxes, morseDeviceSelector(), helpTexts]);

    return div;
};
