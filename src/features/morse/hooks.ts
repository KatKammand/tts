import { getConfigValue, CONF_PROPS } from "../../common/config";
import { FBC_BEEP_PREFIX, HOOK_PRIORITIES } from "../../common/constants";
import { HookFunc, ItemUpdated, hookWrap } from "../../common/hooks";
import { getDeviceByName } from "../../common/ioDevices";
import { hookFunction, HOOK_GROUPS, MODULES, removeHooksByToggle } from "../../common/sdk";
import { getName, getPlayerId, mo_sendAction } from "../../common/utils";
import { checkAndVibrateSlot } from "../toysync/toySync";
import { getMorseConfigValue, setMorseConfigValue, getShareDevices, getMorseDeviceName } from "./config";
import { mo_textConverter, mo_msgTask_morse, msg_morse, mo_train_testLetterTask } from "./morse";

type ChatTrigger = {
    trig: () => string;
    trigDirect: () => string;
    help: string;
    action: (data: ServerChatRoomMessage, trig: string) => ServerChatRoomMessage;
};

export const mo_msgHider = (data: ServerChatRoomMessage, returnMsg = "", msg = ""): ServerChatRoomMessage => {
    if (msg) {
        switch (getMorseConfigValue("msgHideState")) {
            case 0:
                break;
            case 1:
                const morse = mo_textConverter(msg);
                returnMsg += " " + morse;
                break;
            default:
                returnMsg += " " + msg;
                break;
        }
    }

    const newData = { ...data };

    if (returnMsg === "") {
        newData.Type = "Hidden";
    }
    newData.Content = returnMsg;
    return newData;
};

const getContentParts = (data: any, trig: string) => {
    return data.Content.toLowerCase().split(trig.toLowerCase());
};

export const mo_chatTriggers: Record<string, ChatTrigger> = {
    morseStart: {
        trig: () => `Morse ${getPlayerId()} type:`,
        trigDirect: () => "Morse type:",
        help: "Chat: 'Morse [PlayerId] type: [msg]' / Whisper&Beep: 'Morse type: [msg]",
        action(data, trig) {
            const parts = getContentParts(data, trig);
            const msg = parts[1]?.trim();
            if (!msg) return data;

            mo_msgTask_morse(msg);
            return mo_msgHider(data, parts[0], msg);
        },
    },
    morseMsg: {
        trig: () => `Morse ${getPlayerId()} msg:`,
        trigDirect: () => "Morse msg:",
        help: "Chat: 'Morse [PlayerId] msg: [msg]' / Whisper&Beep: 'Morse msg: [msg]'",
        action(data, trig) {
            const parts = getContentParts(data, trig);
            const msg = parts[1]?.trim();
            if (!msg) return data;

            msg_morse(msg);
            return mo_msgHider(data, parts[0], msg);
        },
    },
    switchVisMode: {
        trig: () => `Morse ${getPlayerId()} visibility:`,
        trigDirect: () => "Morse visibility:",
        help: "Morse [PlayerId] visibility: [0-2] - sets if the player will see 0: nothing, 1: morse msg, 2: entire message in its text form",
        action(data, trig) {
            const parts = getContentParts(data, trig);
            const msg = parts[1]?.trim();
            const numb = Number(msg);
            if (!numb || numb < 0 || numb > 2 || getMorseConfigValue("msgHideState") === numb) return data;

            setMorseConfigValue("msgHideState", numb);
            return mo_msgHider(data, parts[0], msg);
        },
    },
    shareDevices: {
        trig: () => `Morse ${getPlayerId()} devices`,
        trigDirect: () => "Morse devices",
        help: "Morse [PlayerId] devices - sends to public chat the device name used to Morse - if player allows sharing.",
        action(data) {
            if (!getShareDevices()) {
                mo_sendAction(`${getName()} device sharing is disabled.`);
                return data;
            }
            const name = getMorseDeviceName();
            const isDeviceFound = getDeviceByName(name);
            let sendText = `${getName()} is connected to ${name}`;
            if (!isDeviceFound) {
                sendText = `${getName()} has no connected device set for morsing`;
            }
            mo_sendAction(sendText);
            return mo_msgHider(data);
        },
    },
    letterTraining: {
        trig: () => `Morse ${getPlayerId()} train letters:`,
        trigDirect: () => "Morse train letters:",
        help: "Morse [PlayerId] train letters: [number of times];[number of letters - optional];[random number of letters (y) - optional] | eg Morse 12345 train letters: 10;3;y --> 10 trainings, with 1-3 letters each time",
        action(data, trig) {
            const parts = getContentParts(data, trig);
            const [numb, max, randomize] = parts[1]?.trim()?.split(";") || [];
            const count = Number(numb);
            if (!count) return data;

            mo_train_testLetterTask(count, Number(max) || undefined, randomize === "true");
            return mo_msgHider(data);
        },
    },
};

const ChatRoomMsgMorse: HookFunc = (args: [ServerChatRoomMessage]) => {
    if (args.length < 1 || !getConfigValue(CONF_PROPS.MORSE_ISACTIVE)) return;

    const [data] = args;
    if (!data?.Type || !data?.Sender || !data?.Content || typeof data.Content !== "string") return;
    if (!["Chat", "Whisper"].includes(data.Type)) return;
    const contentLower = data.Content.toLowerCase();
    if (!contentLower.includes("morse")) return;

    const isWhisper = data.Type === "Whisper";
    const triggers = Object.values(mo_chatTriggers);

    let isDirect = false;
    const foundTrigger = triggers.find((t) => {
        if (contentLower.includes(t.trig().toLowerCase())) return true;

        if (isWhisper && contentLower.includes(t.trigDirect().toLowerCase())) {
            isDirect = true;
            return true;
        }
        return false;
    });
    if (!foundTrigger) return;

    const trigString = isDirect ? foundTrigger.trigDirect() : foundTrigger.trig();
    const newData = foundTrigger.action(data, trigString);
    return [newData, ...args.slice(1)];
};

const BeepMorse: HookFunc = (args: any[]) => {
    if (args.length < 1 || !getConfigValue(CONF_PROPS.MORSE_ISACTIVE)) return;

    const [data] = args;
    if (!data || data.BeepType !== null || !data.Message || !data.Message?.includes("Morse")) return;

    let isDirect = false;
    const triggers = Object.values(mo_chatTriggers);
    const foundTrigger = triggers.find((t) => {
        if (data.Message.includes(t.trig())) return true;

        if (data.Message.includes(t.trigDirect())) {
            isDirect = true;
            return true;
        }
        return false;
    });
    if (!foundTrigger) return;

    const trigString = isDirect ? foundTrigger.trigDirect() : foundTrigger.trig();

    if (!foundTrigger) return;
    //fbc instant messenger adds {data} to end of Message --> remove this from morsed text
    const fbcBeepClean = data.Message.split(FBC_BEEP_PREFIX) as string[];
    const cleanMsg = fbcBeepClean[0];
    const fbcInstantMsgPart = fbcBeepClean[1] ? " \n" + fbcBeepClean.slice(1).join("\n") : "";

    const chatMsgData = { Type: "", Content: cleanMsg, Dictionary: [] } as unknown as ServerChatRoomMessage;
    const newDataC = foundTrigger.action(chatMsgData, trigString);
    const newData = {
        ...data,
        Message: newDataC.Content + fbcInstantMsgPart,
        Type: newDataC.Type,
    };
    return [newData, ...args.slice(1)];
};

function morseChat(args, next) {
    hookWrap(ChatRoomMsgMorse, args, next);
}
function morseBeep(args, next) {
    hookWrap(BeepMorse, args, next);
}

export const registerMorseHooks = () => {
    if (!getConfigValue(CONF_PROPS.MORSE_ISACTIVE)) return;
    hookFunction(HOOK_GROUPS.MORSE_ISACTIVE, MODULES.MORSE, "ChatRoomMessage", HOOK_PRIORITIES.Observe, morseChat);
    hookFunction(HOOK_GROUPS.MORSE_ISACTIVE, MODULES.MORSE, "ServerAccountBeep", HOOK_PRIORITIES.Observe, morseBeep);
};

export const removeMorseHooks = () => removeHooksByToggle(HOOK_GROUPS.MORSE_ISACTIVE);
