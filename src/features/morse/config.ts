import { CONF_PROPS, getConfigValue, setConfigValue } from "../../common/config";
import { ValueOf } from "../../types/tts";

//split it to user config & load it to localstorage
type WAIT_MS = {
    "/": number;
    morse: number;
    " ": number;
    "-": number;
    ".": number;
    good: number;
    bad: number;
};

export type MORSE_CONFIG = {
    morseVibrationsActive: boolean;
    shardeDevices: boolean;
    waits: WAIT_MS;
    speedLvl: number;
    intenisty: number;
    morseDevice: string;
    msgHideState: 0 | 1 | 2;
};

//atm this isnt fully change compitable --> if you change its structure, stored will always override default.
//if you add or remove properties to this --> rework how it is loaded from storage
const mo_WAITS_MS: WAIT_MS = {
    "/": 2300,
    morse: 500,
    " ": 1000,
    "-": 900,
    ".": 300,
    good: 1000,
    bad: 200,
};

//default config values
export const morseConfig: MORSE_CONFIG = {
    morseVibrationsActive: false,
    shardeDevices: false,
    waits: mo_WAITS_MS,
    speedLvl: 1,
    intenisty: 0.75,
    morseDevice: "",
    msgHideState: 1,
};

export const getMorseConfigValue = (key: keyof MORSE_CONFIG): ValueOf<MORSE_CONFIG> =>
    getConfigValue(CONF_PROPS.MORSE_CONFIG)[key];

export const setMorseConfigValue = (key: keyof MORSE_CONFIG, value: ValueOf<MORSE_CONFIG>, save = true) => {
    const oldMorseConfig = getConfigValue(CONF_PROPS.MORSE_CONFIG);
    oldMorseConfig[key] = value;
    setConfigValue(CONF_PROPS.MORSE_CONFIG, { ...oldMorseConfig }, save);
};

const toggleConfig = (key: keyof MORSE_CONFIG): boolean => {
    const newV = !getMorseConfigValue(key);
    setMorseConfigValue(key, newV);
    return newV;
};

export const getWaitTime = (key: keyof WAIT_MS): number =>
    (getMorseConfigValue("waits")[key] as number) * (getMorseConfigValue("speedLvl") as number);

export const setHideState = (numb: number) => setMorseConfigValue("msgHideState", numb);

export const toggleMorseVibrations = () => toggleConfig("morseVibrationsActive");

export const toggleShareDevices = () => toggleConfig("shardeDevices");
export const getShareDevices = (): boolean => getMorseConfigValue("shardeDevices") as boolean;

export const getMorseVibrationsActive = (): boolean => getMorseConfigValue("morseVibrationsActive") as boolean;

export const setMorseDeviceName = (name: string) => setMorseConfigValue("morseDevice", name);

export const getMorseDeviceName = (): string => getMorseConfigValue("morseDevice") as string;

export const getMorseActive = (): boolean => getConfigValue(CONF_PROPS.MORSE_ISACTIVE);
