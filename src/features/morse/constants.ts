import charsJson from "./resources/morse.json";

export const mo_charToMorse = charsJson;
export const mo_morseToChar = Object.keys(mo_charToMorse).reduce((a, k) => {
    a[mo_charToMorse[k]] = k;
    return a;
}, {});

export const alphabetCaps = "abcdefghijklmnopqrstuvwxyz".toUpperCase();
const createAlphabetMorseString = () => {
    const charMorseObj = [...alphabetCaps].reduce((obj, char) => {
        obj[char] = mo_charToMorse[char];
        return obj;
    }, {});
    const valuePairs = Object.keys(charMorseObj)
        .map((k) => `${k}: ${charMorseObj[k]}`)
        .join("  |  ");
    return valuePairs;
};

export const alphabetMorseString = createAlphabetMorseString();
