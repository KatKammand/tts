//________ Non blocking prompt ______

//should solve the Queue issue on morse hook level or something, i mean could do a backup queue here but lets see.

//primitive queue. do a better one later.
let queueIndex = 0;

const promptQueue: HTMLDivElement[] = [];

const createId = (id: string, index: number) => {
    return `${id}-${index}`;
};

export const mo_promptUser = (modalText: string): Promise<string> => {
    queueIndex++;
    const index = queueIndex;
    const isPreviousOpen = promptQueue.length !== 0;

    return new Promise((resolve) => {
        const modal = document.createElement("div");
        modal.id = createId("mo_myModal", index);
        if (isPreviousOpen) {
            modal.style.display = "none";
        }
        modal.innerHTML = `
<div class='mo-modal-content' >
<span class='mo-closer' id=${createId("mo_closeModal", index)}>&times;</span>
<div>${modalText}</div>
<label for=${createId("mo_userInput", index)}>Enter something:</label>
<input type="text" id=${createId("mo_userInput", index)}>
<button id=${createId("mo_submitInput", index)}>Submit</button>
</div>
`;
        const getElement = (id: string) => document.getElementById(createId(id, index));
        promptQueue.push(modal);
        document.body.appendChild(modal);

        //MBCHC
        const preventFocusLoss = (event: KeyboardEvent) => {
            event.stopPropagation(); // Prevent the event from propagating
        };

        const cleanup = () => {
            modal.removeEventListener("keydown", preventFocusLoss);
            promptQueue.shift();
            if (promptQueue.length !== 0) {
                const nextPropt = promptQueue[0];
                nextPropt.style.display = "block";
            }
            modal.remove();
        };

        const submit = () => {
            const res = (getElement("mo_userInput") as HTMLInputElement).value as string;
            cleanup();
            resolve(res);
        };
        // Add a keydown event listener to the document
        modal.addEventListener("keydown", preventFocusLoss);

        getElement("mo_userInput").addEventListener("keypress", (event) => {
            if (event.key === "Enter") submit();
        });

        getElement("mo_closeModal").addEventListener("click", () => {
            cleanup();
            resolve("");
        });

        getElement("mo_submitInput").addEventListener("click", submit);

        getElement("mo_userInput").focus();
    });
};
