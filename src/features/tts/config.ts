import { CONF_PROPS, getConfigValue, setConfigValue } from "../../common/config";
import { ValueOf } from "../../types/tts";
import { DEFAULT_TTS_SERVER_URL } from "./constants";

export type TTS_CONFIG = {
    voicePitch: number;
    voiceRate: number;
    voiceVolume: number;
    ttsWhisperVolume: number; //unused atm
    ttsAll: boolean;
    ttsOwn: boolean;
    ttsSender: boolean; //name sender in tts speech
    ttsBcxVoice: boolean;
    bcxVolume: number;
    bcxRate: number;
    ttsPlayerVoices: Record<number, string>; //PlayerId, voiceName
    ttsDefaultVoice: string;
    ttsToysyncActive: boolean;
    ttsToysyndDeviceName: string;
    /** Kokoro TTS - local generated voices */
    ttsKokoroVoicesActive: boolean;
    /** Use tts-server for tts voices */
    ttsServerActive: boolean;
    ttsServerUrl: string;
};

export const defaultTTSConfig: TTS_CONFIG = {
    voicePitch: 1.0, //0.0 - 2.0
    voiceRate: 1.0, //0.1 - 10.0
    voiceVolume: 1.0, //0.1 - 1
    ttsWhisperVolume: 1, //unused
    ttsAll: true,
    ttsOwn: true,
    ttsSender: true,
    ttsBcxVoice: true,
    bcxVolume: 0.6,
    bcxRate: 0.5,
    ttsPlayerVoices: {},
    ttsDefaultVoice: "",
    ttsToysyncActive: false,
    ttsToysyndDeviceName: "",
    ttsKokoroVoicesActive: false,
    ttsServerActive: false,
    ttsServerUrl: DEFAULT_TTS_SERVER_URL,
};

const getPlayerVoiceObj = () => getTtsConfigValue("ttsPlayerVoices") || {};

/** Validate the newVoice before this phase */
export const addTtsPlayerVoice = (id: number, newVoice: string) => {
    const voiceObj = getPlayerVoiceObj();
    voiceObj[id] = newVoice?.toLocaleLowerCase();
    setTtsConfigValue("ttsPlayerVoices", voiceObj);
};

export const resetTtsPlayerVoice = (id: number) => {
    const voiceObj = getPlayerVoiceObj();
    delete voiceObj[id];
    setTtsConfigValue("ttsPlayerVoices", voiceObj);
};

export const getTtsConfigValue = (key: keyof TTS_CONFIG): ValueOf<TTS_CONFIG> =>
    getConfigValue(CONF_PROPS.TTS_CONFIG)[key];

export const setTtsConfigValue = (key: keyof TTS_CONFIG, value: ValueOf<TTS_CONFIG>) => {
    const oldTtsConfig = getConfigValue(CONF_PROPS.TTS_CONFIG) as TTS_CONFIG;
    (oldTtsConfig[key] as ValueOf<TTS_CONFIG>) = value;
    setConfigValue(CONF_PROPS.TTS_CONFIG, { ...oldTtsConfig });
};

export const getPlayerVoiceName = (id: number): string | undefined => getPlayerVoiceObj()[id];

export const setDefaultVoiceName = (newVoice: string) =>
    setTtsConfigValue("ttsDefaultVoice", newVoice?.toLocaleLowerCase());

export const getDefaultVoiceName = (): string => getTtsConfigValue("ttsDefaultVoice") as string;

export const getIsVoiceSender = () => getTtsConfigValue("ttsSender");

export const getIsOwn = () => getTtsConfigValue("ttsOwn");

export const getIsAll = () => getTtsConfigValue("ttsAll");

export const getAllPlayerVoices = (): { playerId: number; voiceName: string; }[] =>
    Object.entries(getPlayerVoiceObj()).map((v) => ({
        playerId: Number(v[0]),
        voiceName: v[1],
    }));

export const getTtsActive = () => getConfigValue(CONF_PROPS.TTS_ISACTIVE);

export const toggleTtsValue = (
    value: keyof TTS_CONFIG
) => {
    const keyValue = value;
    const newValue = !getTtsConfigValue(keyValue);
    setTtsConfigValue(keyValue, newValue);
    return newValue;
};
