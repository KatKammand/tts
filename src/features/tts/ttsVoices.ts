import { fetchTtsApiSpeech, fetchVoices } from "./api";
import { getDefaultVoiceName, getPlayerVoiceName, getTtsConfigValue, setDefaultVoiceName } from "./config";
import { fetchKokoroVoiceSpeech, getKokoroVoiceList, initKokoroTts, isKokoroReady } from "./kokoro-tts";

export type TtsServerVoice = {
    id: string;
    name: string;
    category: string;
    languageCode: string;
};

export type TtsKokoroVoice = {
    nameId: string,
    name: string;
    language: string;
    gender: string;
    traits: string;
    targetQuality: string;
    overallGrade: string;
};

export type SpeechCallback = () => Promise<void>;

export enum VoiceSourceEnum {
    LOCAL = 1,
    SERVER = 2,
    KOKORO = 3,
}

export type SpeechVoice = {
    /** if Local --> SpeechSynthesisVoice, else TtsServerVoice */
    voiceSource: VoiceSourceEnum;
    /** make it unique but reader friendly so it can be used as id but also list part - categorty:name & local:name */
    nameId: string;
    lang: string;
    voice: TtsServerVoice | SpeechSynthesisVoice | TtsKokoroVoice;
};

// this should just be made into 1 type all conform to on an earlier phase, with all 3 systems using some interface and all, but... alas, such is the way of "its a hobby project and i want to get it to work quick!"
const createSpeechVoice = (
    voiceSource: VoiceSourceEnum,
    voice: TtsServerVoice | SpeechSynthesisVoice | TtsKokoroVoice,
): SpeechVoice => {
    switch (voiceSource) {
        case VoiceSourceEnum.LOCAL:
            const synthesisVoice = voice as SpeechSynthesisVoice;
            return {
                voiceSource: VoiceSourceEnum.LOCAL,
                nameId: synthesisVoice.name.toLocaleLowerCase(),
                lang: synthesisVoice.lang,
                voice: synthesisVoice,
            };
        case VoiceSourceEnum.SERVER:
            const ttsVoice = voice as TtsServerVoice;
            return {
                voiceSource: VoiceSourceEnum.SERVER,
                nameId: ttsVoice.name.toLocaleLowerCase(),
                lang: ttsVoice.languageCode,
                voice: ttsVoice,
            };
        case VoiceSourceEnum.KOKORO:
            const kokoroVoice = voice as TtsKokoroVoice;
            let lang = kokoroVoice.language;
            if (kokoroVoice.gender) {
                lang += `-${kokoroVoice.gender}`;
            }
            if (kokoroVoice.traits) {
                lang += `-${kokoroVoice.traits}`;
            }
    
            return {
                voiceSource: VoiceSourceEnum.KOKORO,
                nameId: 'kokoro-' + voice.name.toLocaleLowerCase(),
                lang: lang,
                voice: kokoroVoice,
            };
    }
};

const VoicesMap: Map<string, SpeechVoice> = new Map();


/** Just need at start since both loading happens async, and dont get any info when local voices are available from browser... */
const voiceLoadsDone = {
    local: false,
    server: false,
};


const getStoredDefaultVoice = () => getVoiceByName(getDefaultVoiceName());

export const getVoiceByName = (name: string): SpeechVoice | undefined => VoicesMap.get(name);
export const getPlayerVoice = (id: number) => getVoiceByName(getPlayerVoiceName(id));
export const getDefaultVoice = (): SpeechVoice => {
    const voice = getVoiceByName(getDefaultVoiceName());
    if(voice) {
        return voice;
    } else {
        return getVoiceByName(getAnyBackupVoice());
    }
}

const getAnyBackupVoice = (): string => {
    for (const lang of ["en-US", "en-UK", "en-GB", "en-"]) {
        const engVoice = window.speechSynthesis.getVoices()?.find((v) => v.lang?.includes(lang));
        if (engVoice?.name) {
            return engVoice.name.toLocaleLowerCase();
        }
        break;
    }

    if(VoicesMap.size > 0) {
        return VoicesMap.keys().next().value;
    } else {
        return "";
    }
}

const setInitDefaultVoice = () => {
    //only do this if there is no
    if (getStoredDefaultVoice()) return;
    if (getTtsConfigValue("ttsServerActive") && !voiceLoadsDone.server) return;
    if (!voiceLoadsDone.local) return;

    console.log("TTS MOD RESETTING DEFAULT VOICE");
    setDefaultVoiceName(getAnyBackupVoice());
};

const loadServerVoices = async () => {
    const voiceList = await fetchVoices();
    voiceList.forEach((v) => {
        const speechVoice = createSpeechVoice(VoiceSourceEnum.SERVER, v);
        VoicesMap.set(speechVoice.nameId, speechVoice);
    });

    voiceLoadsDone.server = true;
};

const loadKokoroVoices = async () => {
    const kokoroVoices = getKokoroVoiceList();
    kokoroVoices.forEach((v) => {
        const speechVoice = createSpeechVoice(VoiceSourceEnum.KOKORO, v);
        VoicesMap.set(speechVoice.nameId, speechVoice);
    });
};

export const removeKokoroVoices = () => {
    Array.from(VoicesMap.entries()).forEach(([key, value]) => {
        if (value.voiceSource === VoiceSourceEnum.KOKORO) {
            VoicesMap.delete(key);
        }
    });
}

export const removeServerVoices = async () => {
    Array.from(VoicesMap.entries()).forEach(([key, value]) => {
        if (value.voiceSource === VoiceSourceEnum.SERVER) {
            VoicesMap.delete(key);
        }
    });
};

//just do like max 20 secs of waiting?
let retryAttemptCount = 0;
let localRetryTimeoutId = null;
const loadLocalVoices = () => {
    if (localRetryTimeoutId) return;

    const voiceList = window.speechSynthesis.getVoices();
    if (voiceList.length === 0) {
        localRetryTimeoutId = setTimeout(() => {
            localRetryTimeoutId = null;

            //stop retrying if after 10 attempts nothing was found... assume none are available.
            if (retryAttemptCount > 10) {
                console.error(
                    "TTS - NO LOCAL VOICES FOUND. Make sure your OS has tts voices, or retry by pressing refresh in tts settings."
                );
                retryAttemptCount = 0;
                voiceLoadsDone.local = true;
                setInitDefaultVoice();
                return;
            }
            loadLocalVoices();
        }, 2000);
        return;
    }

    //voices found, add them to list and see if defaultVoice needs to be set
    voiceList.forEach((v) => {
        const nameLower = v.name?.toLowerCase() || "";
        /** Google voices get rate limited so filter them out */
        if (!nameLower.includes("google")) {
            const speechVoice = createSpeechVoice(VoiceSourceEnum.LOCAL, v);
            VoicesMap.set(speechVoice.nameId, speechVoice);
        }
    });

    voiceLoadsDone.local = true;
};


const DEFAULT_KOKORO_TTS_VOICE_NAME = "kokoro-heart"
export const loadVoices = async (voiceSource?: VoiceSourceEnum) => {
    loadLocalVoices();

    if (getTtsConfigValue("ttsServerActive")) {
        await loadServerVoices();
    }

    if (getTtsConfigValue("ttsKokoroVoicesActive")) {
        if(!isKokoroReady()) {
            await initKokoroTts();
        }

        await loadKokoroVoices();

        //just activated kokoro - set default voice to a kokoro voice
        if(voiceSource === VoiceSourceEnum.KOKORO && getDefaultVoice()?.voiceSource !== VoiceSourceEnum.KOKORO) {
            let defKokoro = getVoiceByName(DEFAULT_KOKORO_TTS_VOICE_NAME)?.nameId;
            if(!defKokoro) {
                Array.from(VoicesMap.entries()).find(([key, value]) => {
                    if (value.voiceSource === VoiceSourceEnum.KOKORO) {
                        defKokoro = key;
                    }
                    return true;
                });
            }

            //find first kokoro voice and set it as default
            setDefaultVoiceName(defKokoro);
        }
    }

    setInitDefaultVoice();
};

/** These need to run on creation, and return a function that can be run to play the sounds, and awaited for the sound's ending. */
export const createServerVoiceCallback = (text: string, voice: TtsServerVoice | TtsKokoroVoice, isBcx: boolean, isKokoro: boolean): SpeechCallback => {
    const fetchPromise = isKokoro ? fetchKokoroVoiceSpeech(text, voice as TtsKokoroVoice) : fetchTtsApiSpeech(text, voice as TtsServerVoice);

    return () =>
        new Promise(async (resolve) => {
            try {
                const stringUrl = await fetchPromise;
                if (!stringUrl) return resolve();
                const audio = new Audio(stringUrl);

                if (isBcx) {
                    audio.playbackRate = getTtsConfigValue("bcxRate") as number;
                    audio.volume = getTtsConfigValue("bcxVolume") as number;
                } else {
                    audio.playbackRate = getTtsConfigValue("voiceRate") as number;
                    audio.volume = getTtsConfigValue("voiceVolume") as number;
                }
                audio.onended = (e) => resolve();
                audio.onerror = (e) => {
                    console.error("TTS Server voice invalid audio. TEXT: ", text, ". VOICE: " + voice.name);
                    resolve();
                };
                audio.play();
            } catch (e) {
                console.error("TTS Server voice play failed", e);
                resolve();
            }
        });
};

export const createLocalVoiceCallback = (text: string, voice: SpeechSynthesisVoice, isBcx: boolean): SpeechCallback => {
    return () =>
        new Promise((resolve) => {
            try {
                const speechUtterance = new SpeechSynthesisUtterance();
                speechUtterance.voice = voice;
                speechUtterance.text = text;
                speechUtterance.pitch = getTtsConfigValue("voicePitch") as number;

                if (isBcx) {
                    speechUtterance.volume = getTtsConfigValue("bcxVolume") as number;
                    speechUtterance.rate = getTtsConfigValue("bcxRate") as number;
                } else {
                    speechUtterance.volume = getTtsConfigValue("voiceVolume") as number;
                    speechUtterance.rate = getTtsConfigValue("voiceRate") as number;
                }
                speechUtterance.onend = () => resolve();
                window.speechSynthesis.speak(speechUtterance);
            } catch (e) {
                console.error("TTS local voice play failed.", e);
                resolve();
            }
        });
};

export const getVoicesList = () => {
    if (VoicesMap.size === 0) loadVoices();
    return Array.from(VoicesMap.values());
};
