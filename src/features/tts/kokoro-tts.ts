import { TTS_GIT_LINK_KOKORO } from "../../common/constants";
import { ttsChatMsg } from "../../common/utils";
import { WorkerAudioRequest, WorkerAudioResponse } from "../../kokoro-tts/types";
import { TtsKokoroVoice } from "./ttsVoices";

const WORKER_URL = `${TTS_GIT_LINK_KOKORO}/workerEsm.js`;

//const WORKER_URL = `${TTS_GIT_LINK_KOKORO}/worker.js`;

// naming convention of this file fights against the basic naming conventions, but...
async function loadWebWorker(workerUrl: string): Promise<Worker> {
    const response = await fetch(workerUrl);
    if (!response.ok) throw new Error(`Failed to load worker script: ${workerUrl}`);

    const scriptText = await response.text();
    const blob = new Blob([scriptText], { type: "application/javascript" });
    const blobURL = URL.createObjectURL(blob);

    return new Worker(blobURL, { type: "module" }); // No "module" type needed
}

let IS_READY: boolean = false;
let IS_LOADING: boolean = false;
let LOADED_KOKORO_VOICES: TtsKokoroVoice[] = [];

let LOADED_WORKER: Worker = null;

/** Generations requests sent to worker thread, with their return promise stored. */
const PENDING_GENERATIONS = new Map<string, (value: string | PromiseLike<string>) => void>();

const setToReady = (status: boolean, voices: Record<string, TtsKokoroVoice>) => {
    IS_READY = status;
    IS_LOADING = false;
    const voiceList = Object.entries(voices).map(([key, value]) => ({ ...value, nameId: key }));
    console.log(`LOADED ${voiceList.length} of Kokoro voice options.`);
    LOADED_KOKORO_VOICES = voiceList;
};

async function createWebWorker(): Promise<boolean> {
    return new Promise(async (resolve) => {
        if (IS_LOADING) {
            console.log("KOKORO IS LOADING");
            return;
        } else {
            IS_LOADING = true;
        }

        if (LOADED_WORKER) {
            console.log("Kokoro Web worker already exists. Terminating old.");
            LOADED_WORKER.terminate();
            LOADED_WORKER = null;
        }

        // Create the worker if it does not yet exist.
        LOADED_WORKER = await loadWebWorker(WORKER_URL);

        // Create a callback function for messages from the worker thread.
        const onMessageReceived = (e) => {
            try {
                switch (e.data.type) {
                    case "device":
                        const device = e.data.device;
                        if ("webgpu".toLocaleLowerCase() === device?.toLocaleLowerCase()) {
                            ttsChatMsg(`You are activating Kokoro.tts with device ${device}`, 60);
                        } else {
                            ttsChatMsg(
                                `You are activating Kokoro.tts with device ${device} - Can be 10x slower than WEBGPU - look up how to activate WEBGPU for your browser!`,
                                60
                            );
                        }

                        console.log(`Loading Kokoro.tts model for (device="${device}")`);
                        break;

                    case "ready":
                        setToReady(true, e.data.voices);
                        resolve(true);
                        break;

                    case "error":
                        console.error(e.data.data);
                        break;

                    case "response":
                        console.debug(e.data.result);
                        break;

                    case "complete":
                        const { audio, text, requestId } = e.data as WorkerAudioResponse;
                        console.debug("Received audio: ", text);
                        receiveCompletedAudio(text, audio, requestId);
                        break;
                }
            } catch (e) {
                console.error("Error handling message:", e);
            }
        };

        const onErrorReceived = (e) => {
            console.error("Worker error:", e);
            resolve(false);
        };

        LOADED_WORKER.addEventListener("message", onMessageReceived);
        LOADED_WORKER.addEventListener("error", onErrorReceived);
    });
}

export async function initKokoroTts() {
    console.log("start kokoro init");
    const isInitSuccess = await createWebWorker();
    if (!isInitSuccess) {
        console.error("Failed to initialize Kokoro.tts");
    } else {
        console.log("Kokoro initialized");
    }
}

export function terminateKokoroTts() {
    LOADED_WORKER.terminate();
    LOADED_WORKER = null;
    LOADED_KOKORO_VOICES = [];
    IS_LOADING = false;
    IS_READY = false;
}

export function getKokoroVoiceList() {
    return LOADED_KOKORO_VOICES;
}

function receiveCompletedAudio(text: string, audioUrl: string, requestId: string) {
    const resolver = PENDING_GENERATIONS.get(requestId);
    if (resolver) {
        resolver(audioUrl); // ! --> checks if the function is null, if not, calls it. TIL.
        PENDING_GENERATIONS.delete(requestId); // Clean up
    } else {
        console.warn("No kokoro-tts resolver found for request id:", requestId);
    }
    console.debug("completed audio received:", text, audioUrl);
}

export function fetchKokoroVoiceSpeech(text: string, voice: TtsKokoroVoice): Promise<string> {
    return new Promise((resolve) => {
        const requestId = crypto.randomUUID(); // Generate a unique ID for tracking
        PENDING_GENERATIONS.set(requestId, resolve);

        console.debug("start kokoro audio generation for request id: " + requestId);

        LOADED_WORKER.postMessage({
            type: "generate",
            requestId: requestId,
            text: text.trim(),
            voiceId: voice.nameId,
        } as WorkerAudioRequest);
    });
}

export const isKokoroReady = () => IS_READY;

export default {
    initKokoroTts,
    getKokoroVoiceList,
    isKokoroReady,
    fetchKokoroVoiceSpeech,
    terminateKokoroTts
};
