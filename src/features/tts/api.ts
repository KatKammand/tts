import { hasLetterOrNumber } from "../../common/utils";
import { getTtsConfigValue } from "./config";
import { TtsServerVoice } from "./ttsVoices";

type VoitTtsPostBody = {
    credentials: {
        apiKey: string;
    };
    payload: {
        text: string;
        voice: {
            id: string;
            name: string;
            category: string;
            languageCode: string;
        };
    };
};

const getTtsServerUrl = () => getTtsConfigValue("ttsServerUrl");

export const fetchVoices = async (): Promise<TtsServerVoice[]> => {
    if (!getTtsConfigValue("ttsServerActive")) return [];

    try {
        const response = await fetch(`${getTtsServerUrl()}/list-voices`);

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        return (await response.json()) as TtsServerVoice[];
    } catch (error) {
        // Handle errors here
        console.error("Error fetching data:", error);
        return [];
    }
};

/** retuns callback promise function that can be called to play the voice, and can be awaited for play end */
export const fetchTtsApiSpeech = async (text: string, voice: TtsServerVoice): Promise<string> => {
    //AI voices dont process pure special character strings well. actually should probably do some more special char filtering for these in future.
    if (!getTtsConfigValue("ttsServerActive") || !hasLetterOrNumber(text)) return;

    const body: VoitTtsPostBody = {
        credentials: {
            apiKey: "",
        },
        payload: {
            text,
            voice,
        },
    };

    try {
        const response = await fetch(`${getTtsServerUrl()}/synthesize-speech`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body),
        });

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const wavFile = await response.blob();
        if (!wavFile) {
            console.info("TTS no wav file returned for text: ", text, "and voice: " + voice.name);
            return;
        }

        return URL.createObjectURL(wavFile);
    } catch (error) {
        // Handle errors here
        console.error("Error fetching data:", error);
    }
};
