import { CONF_PROPS, getConfigValue } from "../../common/config";
import { ChatMsgHandlerProps } from "../../types/tts";
import {
    getPlayerId,
    getValidCharNameForTTS,
    hasSpecialCharacters,
    removeSpecialCharacters,
    replaceLinks,
    strCompare,
} from "../../common/utils";
import { getIsVoiceSender, getTtsConfigValue } from "./config";
import { ttsToyMsgToQueue } from "./ttsToysync";
import {
    getPlayerVoice,
    loadVoices,
    createLocalVoiceCallback,
    createServerVoiceCallback,
    TtsServerVoice,
    SpeechCallback,
    getDefaultVoice,
    SpeechVoice,
    getVoiceByName,
    VoiceSourceEnum,
} from "./ttsVoices";

export type SpeechTypes = "Whisper" | "Chat" | "Emote" | "BCXVoice";

const TypeFiller: Record<SpeechTypes, string> = {
    Whisper: "whispers ",
    Chat: "says ",
    Emote: "",
    BCXVoice: "",
};

export type ActionProps = {
    wait?: number;
    intensityNumber?: number;
};

export type SpeechInstanceAction = (props?: ActionProps) => Promise<void>;

export type SpeechInstance = {
    speakerId: number;
    speakerName: string;
    text: string;
    type: SpeechTypes;
    startAction?: SpeechInstanceAction; //allows eg ToySync changes on each
    isToysync?: boolean;
};

type SpeechQueueItme = {
    speakCallback: SpeechCallback;
    startAction?: SpeechInstanceAction;
    isToysync?: boolean;
};

let isSpeaking = false;
const SPEECH_QUEUE: SpeechQueueItme[] = [];

/** If true --> all TtsToysync devices should be prevented from being toggled by other things. And at end unreserve them. */
let isTtsToysyncSpeak = false;

const transformText = (speechInstance: SpeechInstance) => {
    const { text, type, speakerName } = speechInstance;
    let finalText = "";
    if (getIsVoiceSender() && type !== "BCXVoice") {
        if (type === "Emote" && text.startsWith("*")) {
            finalText = ""; //added
        } else {
            finalText = `${speakerName} ${TypeFiller[type]}`;
        }
    }
    return finalText + removeSpecialCharacters(replaceLinks(text));
};

const createSpeechCallback = (text: string, voice: SpeechVoice, isBcx = false): SpeechCallback => {
    if (!text || !voice) return;

    let callback: SpeechCallback = null;

    if (voice.voiceSource === VoiceSourceEnum.LOCAL) {
        callback = createLocalVoiceCallback(text, voice.voice as SpeechSynthesisVoice, isBcx);
    } else if (voice.voiceSource === VoiceSourceEnum.KOKORO) {
        callback = createServerVoiceCallback(text, voice.voice as TtsServerVoice, isBcx, true);
    } else {
        if (!getTtsConfigValue("ttsServerActive")) {
            const defaultVoice = getDefaultVoice();
            if (defaultVoice?.voiceSource === VoiceSourceEnum.LOCAL) {
                callback = createLocalVoiceCallback(text, voice.voice as SpeechSynthesisVoice, isBcx);
            } else {
                console.error(
                    "TTS selected voice is TTS Server voice, but it is not active, so no tts could be done. Switch to local voice or activate server"
                );
                return;
            }
        }
        callback = createServerVoiceCallback(text, voice.voice as TtsServerVoice, isBcx, false);
    }
    return callback;
};

/** only do this for local speech instances */
const createSpeechQueueItem = (speechInstance: SpeechInstance): SpeechQueueItme => {
    const voice = getPlayerVoice(speechInstance.speakerId) || getDefaultVoice();
    const text = transformText(speechInstance);

    if (!voice) {
        console.error("TTS Failed to find any voices, so no tts can be done.");
        return;
    }
    const isBcx = speechInstance.type === "BCXVoice";
    const callback: SpeechCallback = createSpeechCallback(text, voice, isBcx);

    return {
        speakCallback: callback,
        startAction: speechInstance.startAction,
        isToysync: speechInstance.isToysync,
    };
};

/**
 * Recursive function that could in extreme cases throw error, atm requires som 10k function calls for this to happen.
 * Most likely never will happen, as the queue is more than liekly ot be empty at some point and reset the call stack.
 *
 * @TODO also this could be done in a way that it sets currently active speaking, and a functiont to cancel that?
 */
const speakNext = async () => {
    if (SPEECH_QUEUE.length === 0) return;

    if (!getConfigValue(CONF_PROPS.TTS_ISACTIVE)) {
        ttsClearQueue();
        return;
    }

    const nextSpeech = SPEECH_QUEUE.shift();
    if (getTtsConfigValue("ttsToysyncActive") && typeof nextSpeech?.startAction === "function")
        await nextSpeech.startAction();
    if (!nextSpeech.speakCallback) return speakNext();

    isSpeaking = true;
    await nextSpeech.speakCallback();
    isSpeaking = false;
    speakNext();
};

export const addTestTtsToQueue = async (text: string, voiceId: string) => {
    const voice = getVoiceByName(voiceId);
    const callback = createSpeechCallback(text, voice);
    if (!callback) return;

    SPEECH_QUEUE.push({ speakCallback: callback });

    if (!isSpeaking) speakNext();
};

export const addToTtsQueue = (speech: SpeechInstance) => {
    const queueItem = createSpeechQueueItem(speech);
    if (!queueItem) return;

    SPEECH_QUEUE.push(queueItem);

    if (!isSpeaking) speakNext();
};

export const ttsSkipToNext = () => {
    if (!isSpeaking) return;

    window.speechSynthesis.cancel();

    //for some reason immediate speecSynthesis.speak() breaks the system, short timeout fixes.
    //to avoid new speech from starting
    isSpeaking = true;

    setTimeout(() => {
        isSpeaking = false;
        speakNext();
    }, 250);
};

//unused
export const ttsClearQueue = () => {
    if (!isSpeaking) return;

    window.speechSynthesis.cancel();
    while (SPEECH_QUEUE.pop()) {}
    isSpeaking = false;
};

const getSenderName = (c: Character, senderName: string, isCheat: boolean) => {
    const isDeprivedName = strCompare(senderName, "Someone");
    if (isDeprivedName && isCheat) {
        return getValidCharNameForTTS(c);
    }

    return hasSpecialCharacters(senderName) ? c.Name : senderName;
};

const TRIGGER_WORD = "tts:";

export const ttsProcessMsg = (msgPrps: ChatMsgHandlerProps, isCheat: boolean): string | undefined => {
    if (!getConfigValue(CONF_PROPS.TTS_ISACTIVE)) return;

    const { msgData, senderChar, msgCurrent, metadata } = msgPrps;
    if (!msgData?.Type || !msgData?.Sender || typeof msgData?.Content !== "string") return;
    if (!["Chat", "Whisper", "Emote"].includes(msgData.Type)) return; //Beeps also come through this... hmm... for future.

    const isTtsOwn = getTtsConfigValue("ttsOwn");
    if (!isTtsOwn && msgData.Sender === getPlayerId()) return;

    let msgToProcessLower = (isCheat ? msgData.Content : msgCurrent).toLowerCase();

    const isTtsAll = getTtsConfigValue("ttsAll"); //tts doll uses this at least, keep it for tts doll ^^

    if (!msgToProcessLower || (!isTtsAll && !msgToProcessLower.includes("tts:"))) return;

    //emote handling for *Name does* and **Something happens*
    let removedName = "";
    if (msgData.Type === "Emote" && metadata?.senderName) {
        const isDoubleEmote = msgData.Content.startsWith("*");
        if (isDoubleEmote) {
            msgToProcessLower = `*${msgToProcessLower}`;
        } else {
            removedName = metadata.senderName;
            if (msgToProcessLower.startsWith(metadata.senderName?.toLowerCase())) {
                msgToProcessLower = msgToProcessLower.substring(metadata.senderName.length);
            }
        }
    }

    //if isTtsAll, want the parts before TTS: too, but can skip the TTS: messages from the voice msgs
    const senderName = getSenderName(senderChar, metadata?.senderName, isCheat);
    const newMsg = msgToProcessLower
        .split(TRIGGER_WORD)
        .slice(isTtsAll ? 0 : 1)
        .map((a) => a.trim())
        .join(" ")
        .trim();

    if (!newMsg) return;

    const speechInstance: SpeechInstance = {
        speakerId: msgData.Sender,
        speakerName: senderName,
        text: newMsg,
        type: msgData.Type as SpeechTypes,
    };

    if (getTtsConfigValue("ttsToysyncActive")) {
        const newText = ttsToyMsgToQueue(speechInstance);
        if (newText && msgData.Sender !== getPlayerId()) {
            return `${removedName} ${newText}`.trim();
        }
    } else {
        addToTtsQueue(speechInstance);
    }
};

export const addTtsListeners = async () => {
    await loadVoices();
    window.speechSynthesis.onvoiceschanged = async () => {
        await loadVoices();
    };
};
