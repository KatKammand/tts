import {
    addTtsPlayerVoice,
    getAllPlayerVoices,
    getDefaultVoiceName,
    getTtsActive,
    resetTtsPlayerVoice,
    setDefaultVoiceName,
    setTtsConfigValue,
    toggleTtsValue,
} from "./config";
import {
    SelectorChangeHandler,
    SelectorOption,
    createBtnContainer,
    createButton,
    createCheckbox,
    createDivContainer,
    createInput,
    createLinkElement,
    createList,
    createListItem,
    createOptionsList,
    createSliderElement,
    createTextSpan,
} from "../../common/components";
import { getTtsConfigValue } from "./config";
import { getPlayerId } from "../../common/utils";
import { TTS_README_LINK_TTSTOYSYNC, TTS_README_LINK_VOICE } from "../../common/constants";
import { toggleTtsActive, toggleTtxBcxListener } from "./hooks";
import { VoiceSourceEnum, getPlayerVoice, getVoicesList, loadVoices, removeKokoroVoices, removeServerVoices } from "./ttsVoices";
import { DEFAULT_TTS_SERVER_URL, TTS_SERVER_REPO } from "./constants";
import { addTestTtsToQueue } from "./ttsQueue";
import refreshPng from "../../resources/refresh.png";
import { initKokoroTts, terminateKokoroTts } from "./kokoro-tts";

export const TTS_TAB_REFRESH_EVENT = "tts-refresh-event";

const refreshEvent = new Event(TTS_TAB_REFRESH_EVENT, { bubbles: true });

const createVoiceList = (initVoice?: string, chngHandler?: SelectorChangeHandler): HTMLElement => {
    const voiceOptions = getVoicesList();

    const divContainer = createDivContainer();
    const selectorList = createOptionsList(
        voiceOptions.map((v) => ({
            id: v.nameId,
            value: `${v.nameId} | ${v.lang}`,
        })),
        initVoice,
        chngHandler
    );

    const refreshList = async (e) => {
        await loadVoices();
        divContainer.dispatchEvent(refreshEvent);
    };

    const refreshBtn = createButton("", refreshList, "tts-refresh-icon");
    refreshBtn.style.backgroundImage = `url('${refreshPng}')`;

    divContainer.append(selectorList, refreshBtn);
    divContainer.style.flexDirection = "row";

    return divContainer;
};

const createDefaultVoiceSelector = () => {
    const div = createDivContainer();
    div.textContent = "Select Default voice";

    div.append(createVoiceList(getDefaultVoiceName(), (name) => setDefaultVoiceName(name.value)));

    return div;
};

const createPlayerVoiceList = (deleteAction: (item: SelectorOption) => void) => {
    const allPlayerVoices = getAllPlayerVoices();
    return createList(
        allPlayerVoices.map((v) => ({
            id: v.playerId + "",
            value: `MemberNumber: ${v.playerId} | Voice: ${v.voiceName}`,
        })),
        deleteAction
    );
};

const createPlayerVoiceAdd = (addToListFunc: (id: number, name: string) => void) => {
    const idInput = createInput(getPlayerId() + "");
    idInput.id = "tts-add-player-voice-input";
    const divInput = createDivContainer();
    divInput.className = "tts-input-container";
    divInput.textContent = "PlayerId: ";
    divInput.appendChild(idInput);

    const voiceList = createVoiceList();

    const selectHandler = () => {
        const playerId = Number(idInput.value);
        const selector = voiceList.children[0] as HTMLSelectElement;
        const voice = selector.value;
        if (Number.isNaN(playerId) || playerId < 0 || !voice || !!getPlayerVoice(playerId)) return;

        addTtsPlayerVoice(playerId, voice);
        addToListFunc(playerId, voice);
    };

    const btn = createButton("ADD ID TO VOICELIST", selectHandler);
    idInput.addEventListener("keypress", (event: KeyboardEvent) => {
        if (event.key === "Enter") selectHandler();
    });

    return createDivContainer([divInput, voiceList, btn], "tts-voices-container");
};

const createPlayersVoiceSelector = () => {
    const deleteAction = (item: SelectorOption) => resetTtsPlayerVoice(Number(item.id));
    const list = createPlayerVoiceList(deleteAction);
    const addToList = (id: number, name: string) => {
        list.appendChild(createListItem({ id: id + "", value: `PlayerId: ${id} | VoiceName: ${name}` }, deleteAction));
    };

    const div = createDivContainer();
    div.textContent = "Give Players individual voices:";
    div.append(...[createPlayerVoiceAdd(addToList), createTextSpan("Active Player Voices"), list]);
    return div;
};

const sliderOptions = () => {
    return createDivContainer([
        createSliderElement("Pitch", 0, 2, getTtsConfigValue("voicePitch") as number, (value) =>
            setTtsConfigValue("voicePitch", value)
        ),
        createSliderElement("Speed", 0, 10, getTtsConfigValue("voiceRate") as number, (value) =>
            setTtsConfigValue("voiceRate", value)
        ),
        createSliderElement("Volume", 0, 1, getTtsConfigValue("voiceVolume") as number, (value) =>
            setTtsConfigValue("voiceVolume", value)
        ),
    ]);
};

const bcxVoiceSliderOptions = () => {
    return createDivContainer([
        createTextSpan('BCX Voice settings - Default lower to make it a "voice in your head". Uses your own voice.'),
        createSliderElement("Speed", 0, 10, getTtsConfigValue("bcxRate") as number, (value) =>
            setTtsConfigValue("bcxRate", value)
        ),
        createSliderElement("Volume", 0, 1, getTtsConfigValue("bcxVolume") as number, (value) =>
            setTtsConfigValue("bcxVolume", value)
        ),
    ]);
};

const voiceTester = () => {
    const voiceList = createVoiceList(getDefaultVoiceName());
    const input = createInput("Test phrase for the voice");

    const onClick = () => {
        const text = input.value;
        const selector = voiceList.children[0] as HTMLSelectElement;
        const voiceNameId = selector.value;

        if (text && voiceNameId) addTestTtsToQueue(text, voiceNameId);
    };

    const testVoiceBtn = createButton("Test the voice", onClick);
    return createDivContainer([createTextSpan("Test different voices here"), voiceList, input, testVoiceBtn]);
};

const ttsServerUrlInput = () => {
    const input = createInput(getTtsConfigValue("ttsServerUrl") as string);

    const updateUrl = (newUrl: string) => {
        if (getTtsConfigValue("ttsServerUrl") === newUrl) return;

        setTtsConfigValue("ttsServerUrl", newUrl);
        input.dispatchEvent(refreshEvent);
    };

    const onSave = (e: Event) => {
        e.preventDefault();
        updateUrl(input.value);
    };
    const onReset = (e: Event) => {
        e.preventDefault();
        updateUrl(DEFAULT_TTS_SERVER_URL);
    };

    const saveBtn = createButton("Save Url", onSave);
    const resetBtn = createButton("Reset Url", onReset);

    const btnContainer = createBtnContainer();
    btnContainer.append(saveBtn, resetBtn);
    return createDivContainer(
        [
            createTextSpan(`TTS Server Url - default is '${DEFAULT_TTS_SERVER_URL}'. Current:`),
            input,
            btnContainer,
            voiceTester(),
        ],
        "tts-boxed-container"
    );
};

/** Container for all tts settings */
export const ttsSettings = () => {
    const toggleBoxes = createDivContainer();
    toggleBoxes.className = "tts-toggle-container";

    const toggleTtsVoiceServer = async () => {
        const isActive = toggleTtsValue("ttsServerActive");
        if (isActive) {
            await loadVoices();
        } else {
            removeServerVoices();
        }
        toggleBoxes.dispatchEvent(refreshEvent);
    };

    const toggleKokoroTts = async () => {
        const isActive = toggleTtsValue("ttsKokoroVoicesActive");
        if (isActive) {
            await initKokoroTts();
            await loadVoices(VoiceSourceEnum.KOKORO);
        } else {
            terminateKokoroTts();
            removeKokoroVoices();
        }
        toggleBoxes.dispatchEvent(refreshEvent);
    };

    toggleBoxes.append(
        ...[
            createCheckbox(getTtsActive(), toggleTtsActive, "TTS Active - switch text-to-speech module on or off"),
            createCheckbox(
                getTtsConfigValue("ttsKokoroVoicesActive") === true,
                toggleKokoroTts,
                "Use Kokoro-TTS High Quality AI voices - Uses your GPU/CPU, downloads a 80MB-300MB model (only once). Privacy friendly. (only desktop, chromium/firefox nightly work)."
            ),
            createCheckbox(
                getTtsConfigValue("ttsAll") === true,
                () => toggleTtsValue("ttsAll"),
                "TTS All - ON: All messages are voiced. OFF: only texts coming after 'TTS:' <this is read>"
            ),
            createCheckbox(
                getTtsConfigValue("ttsOwn") === true,
                () => toggleTtsValue("ttsOwn"),
                "TTS Own - ON: your and other peoples messages are TTS voiced, OFF: only other peoples messages are voiced"
            ),
            createCheckbox(
                getTtsConfigValue("ttsSender") === true,
                () => toggleTtsValue("ttsSender"),
                "TTS Sender Active - ON: sender player name is voiced. OFF: only the message is voiced."
            ),
            createCheckbox(
                getTtsConfigValue("ttsBcxVoice") === true,
                toggleTtxBcxListener,
                "TTS BCX 'Listen to my voice' - ON: Bcx listen to my voice are voiced. OFF: Bcx listen to my voice are not voiced."
            ),
            createCheckbox(
                getTtsConfigValue("ttsToysyncActive") === true,
                () => toggleTtsValue("ttsToysyncActive"),
                "TTS Toysyncronization allowed - Requires ToySync to be active and some buttplug.io devices to be connected to have any effect."
            ),
            createTextSpan(
                "TTS Toysync messages can be sent to you, by sending normal message/whisper. Allows embedding Buttplug.io Toy commands in text, and synchronizing them with TTS voice reading."
            ),
            createTextSpan(
                'Message format: "A normal message where i have inserted toy changes [max], your device just jumped to max, now lets set low and wait 5 sec [low:5]"'
            ),
            createDivContainer([createLinkElement(TTS_README_LINK_TTSTOYSYNC, "Link To TTS ToySync use instructions")]),
            createCheckbox(
                getTtsConfigValue("ttsServerActive") === true,
                toggleTtsVoiceServer,
                "Use TTS Server - Local AI voices server, see below link for more information and instructions"
            ),
            createDivContainer([createLinkElement(TTS_SERVER_REPO, "Link To TTS Server repository & readme")]),
        ]
    );

    return createDivContainer([
        toggleBoxes,
        ttsServerUrlInput(),
        createLinkElement(TTS_README_LINK_VOICE, "Want more voices? Read this Readme part"),
        createDefaultVoiceSelector(),
        sliderOptions(),
        createPlayersVoiceSelector(),
        bcxVoiceSliderOptions(),
    ]);
};
