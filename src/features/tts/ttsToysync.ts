import { ToySyncDevice, getAllDevices, getDeviceByName, vibrateDevice } from "../../common/ioDevices";
import { getIntensityValueList, intensityToVibrateLv, mo_PsWait } from "../../common/utils";
import { SpeechInstance, SpeechInstanceAction, addToTtsQueue } from "./ttsQueue";

/**
 * Has similarities To ToySync, but only real thing is competition for same devices, same with morse
 * This is supposed to Syncronize Toy intensity changes with tts voice reading.
 */

//ttstoysync: example text would be like you feel the toys turn on [low], and you walk around a bit,
//suddently the vibrations increase [up] and increase [up:10.0]... [:10] and then they stop [off]

type TOY_ACTIONS = "up" | "down" | "off" | "low" | "med" | "high" | "max" | "number";

//if none selected --> use all? and if one selected use it? all that is needed is name
//if it is changed --> need to update change its "isMorsing" to false --> should add isTtsing otherwise this might think its doing this while morsing
const getTtsToySyncDevices = (): ToySyncDevice[] => {
    let selectedDeviceName = "";
    const namedDevice = getDeviceByName(selectedDeviceName);
    if (namedDevice) {
        return [namedDevice];
    } else {
        return getAllDevices();
    }
};

const checkValidIntesity = (numb: number) => numb !== null && numb !== undefined && numb >= 0 && numb <= 1;

const defaultAction = async (newIntensity: number, waitMs: number) => {
    if (checkValidIntesity(newIntensity)) {
        getTtsToySyncDevices().forEach((d) => {
            if (!d || d.isMorsing || d.LastIntensity === newIntensity) return;
            vibrateDevice(d, newIntensity);
        });
    }
    if (waitMs) await mo_PsWait(waitMs);
};

const TTS_TOY_ACTION_LIST: Record<TOY_ACTIONS, SpeechInstanceAction> = {
    /** Increase intensity by 1 step (low --> med | max --> max) */
    up: async (props) => {
        getTtsToySyncDevices().forEach((d) => {
            if (!d || d.isMorsing || d.LastIntensity === 1) return;
            let newIntensity = d.LastIntensity;

            const intenistyValues = getIntensityValueList();
            intenistyValues.sort().forEach((v, index) => {
                if (v === d.LastIntensity) newIntensity = intenistyValues[index + 1];
            });
            if (newIntensity === undefined) return;

            vibrateDevice(d, newIntensity);
        });
        if (props.wait) await mo_PsWait(props.wait);
    },
    /** Decrease intensity */
    down: async (props) => {
        getTtsToySyncDevices().forEach((d) => {
            if (!d || d.isMorsing || d.LastIntensity === 0) return;
            let newIntensity = d.LastIntensity;

            const intenistyValues = getIntensityValueList();
            intenistyValues.sort().forEach((v, index) => {
                if (v === d.LastIntensity) newIntensity = intenistyValues[index - 1];
            });
            if (newIntensity === undefined) return;

            vibrateDevice(d, newIntensity);
        });
        if (props.wait) await mo_PsWait(props.wait);
    },
    /** Stop vibrations --> -1 */
    off: async (props) => defaultAction(intensityToVibrateLv(-1), props.wait),
    /** Set LOW --> 0 */
    low: async (props) => defaultAction(intensityToVibrateLv(0), props.wait),
    /** Set MEDIUM --> 1*/
    med: async (props) => defaultAction(intensityToVibrateLv(1), props.wait),
    /** Set HIGH --> 2 */
    high: async (props) => defaultAction(intensityToVibrateLv(2), props.wait),
    /** Set MAXIMUM --> 3 */
    max: async (props) => defaultAction(intensityToVibrateLv(3), props.wait),
    /** Set CUSTOM NUMBER... uh needs rework for the system though */
    number: async (props) => defaultAction(props.intensityNumber, props.wait),
};

//NEED TO RESERVE THE DEVICE JUST LIKE MORSER DOES FOR THIS... so set IsMorsing = true i guess

const parseMsg = (input: string): { text: string; action: string }[] => {
    const regex = /\[([^\]]+)\]/g;
    let match: RegExpExecArray;
    let lastIndex = 0;
    const result = [];

    while ((match = regex.exec(input)) !== null) {
        const text = input.substring(lastIndex, match.index);

        result.push({ text: text.trim(), action: match[1] });
        lastIndex = regex.lastIndex;
    }

    if (lastIndex < input.length) {
        const text = input.substring(lastIndex);
        result.push({ text: text.trim(), action: null });
    }
    const sortedResults = [];
    // Move actions one position forward
    result.forEach((r, i) => {
        if (i === 0 && r.text) {
            sortedResults.push({ text: r.text, action: null });
        }
        const newText = result[i + 1]?.text || "";
        if (!newText && !r.action) return;
        sortedResults.push({ text: newText, action: r.action });
    });

    return sortedResults;
};

/**
 * SO plan is to do something like:
 * "ttstoysync: example text would be like you feel the toys turn on [low], and you walk around a bit, suddently the vibrations increase [up] and increase [up]... [:10] and then they stop [off]"
 * Need to check for [low:0.5] --> low intensity, 0.5 sec wait
 */

//eh figure this out later? --> add used: { isReserved: boolean, usingModule: 'morse' | 'tts' }
export const reserveTtsToysyncvDevices = () => {
    const devices = getTtsToySyncDevices();
    devices.forEach((d) => (d.isTtsToysyncing = true));
};

/** returns new string  */
export const ttsToyMsgToQueue = (ttsToyMsg: SpeechInstance) => {
    if (!ttsToyMsg.text) return;

    let hadValidTtsToysyncCommands = false;
    const textsArr = parseMsg(ttsToyMsg.text);
    const textActionMapped: SpeechInstance[] = textsArr.map((t) => {
        let actionFunc: SpeechInstanceAction;

        const actionStr = t.action || "";

        const [actionType, waitSec] = actionStr.split(":");

        //waitSec is undefined, or in seconds (can have decimals)
        const waitTime = Number(waitSec) > 0 ? Number(waitSec) * 1000 : undefined;

        //its a numberic value --> translate to directly vibration level
        const actionValue = Number(actionType);
        if (actionType !== "" && !Number.isNaN(actionValue)) {
            actionFunc = () => TTS_TOY_ACTION_LIST["number"]({ intensityNumber: actionValue, wait: waitTime });
        } else {
            const matchedAction = TTS_TOY_ACTION_LIST[actionType] as SpeechInstanceAction;
            if (matchedAction) {
                actionFunc = () => matchedAction({ wait: waitTime });
            } else if (waitTime) {
                actionFunc = () => defaultAction(null, waitTime);
            }
        }

        let returnMsg = t.text;
        if (actionFunc) {
            hadValidTtsToysyncCommands = true;
        } else if (!!actionStr) {
            returnMsg = `[${actionStr}] ${t.text}`;
        }

        return {
            ...ttsToyMsg,
            text: returnMsg,
            startAction: actionFunc,
            isToysync: true,
        };
    });

    if (!hadValidTtsToysyncCommands) {
        addToTtsQueue(ttsToyMsg);
        return;
    }

    textActionMapped.forEach((spInst) => addToTtsQueue(spInst));
    //cleans [commands] from the text that is shown to others
    return textActionMapped.map((t) => t.text.trim()).join(" ");
};
