import { CONF_PROPS, getConfigValue, toggleConfig } from "../../common/config";
import { BC_CLUB_CHAT_ID, HOOK_PRIORITIES } from "../../common/constants";
import { HOOK_GROUPS, MODULES, hookFunction, removeHooksByToggle } from "../../common/sdk";
import { getPlayerId } from "../../common/utils";
import { getTtsActive, getTtsConfigValue, toggleTtsValue } from "./config";
import { addToTtsQueue, ttsProcessMsg } from "./ttsQueue";

const LISTEN_TO_MY_VOICE_PREFIX = "[Voice] ";

const TTS_READER: ChatRoomMessageHandler = {
    Description: "TTS Reader",
    Priority: 1000, //Vanilla SpeechGarble etc is 100 --> 300 sens dep stuff, just do this last, and voice what is left, if left
    Callback: (data: ServerChatRoomMessage, sender: Character, msg: string, metadata: IChatRoomMessageMetadata) => {
        const newMsg = ttsProcessMsg({ msgData: data, senderChar: sender, msgCurrent: msg, metadata }, false);
        if (newMsg) {
            return { msg: newMsg };
        }
        return { msg: msg };
    },
}

/**
 * BCX "Listen to my voice" uses "ChatRoomSendLocal" --> bcx function, which doesnt seem to be hookable.
 * manual "hooking" results in function hash complaint from bcx
 * So for now... chat interval loops it is...
 */

let TTS_CHAT_OBSERVER: MutationObserver = null;

const addChatLogObserver = () => {
    const targetNode = document.getElementById(BC_CLUB_CHAT_ID);
    const config = { childList: true };
    const callback = (mutationList) => {
        if (!getConfigValue(CONF_PROPS.TTS_ISACTIVE)) return;
        //BCX should do appendChild, so 1 addition at time.
        if (mutationList.length !== 1 || !getTtsConfigValue("ttsBcxVoice")) return;
        const mutation = mutationList[0];

        //same here again, BCX, 1 added node
        if (mutation?.addedNodes?.length !== 1) return;
        const newText = mutation.addedNodes[0].innerText as string;

        //bcx Listen to my voice --> '[Voice] <message>'
        if (!newText.startsWith(LISTEN_TO_MY_VOICE_PREFIX)) return;

        const listenMsg = newText.substring(LISTEN_TO_MY_VOICE_PREFIX.length);
        if (!listenMsg) return;

        addToTtsQueue({
            speakerId: getPlayerId(),
            speakerName: "Voice",
            text: listenMsg,
            type: "BCXVoice",
        });
    };
    try {
        if (TTS_CHAT_OBSERVER) {
            TTS_CHAT_OBSERVER?.disconnect();
            TTS_CHAT_OBSERVER = null;
        }
    } catch (e) { }
    const observer = TTS_CHAT_OBSERVER || new MutationObserver(callback);
    observer.observe(targetNode, config);
    TTS_CHAT_OBSERVER = observer;
};

//should really do the FBC styled waitFor() func
let OBSERVE_INIT_TIMEOUT_ID = null;
const chatObserverInit = () => {
    if (OBSERVE_INIT_TIMEOUT_ID) clearTimeout(OBSERVE_INIT_TIMEOUT_ID);
    if (!getTtsConfigValue("ttsBcxVoice")) return;
    if (!document.getElementById(BC_CLUB_CHAT_ID)) {
        OBSERVE_INIT_TIMEOUT_ID = setTimeout(chatObserverInit, 2000);
        return;
    }

    addChatLogObserver();
};

function chatCreateHook(args, next) {
    //this hook runs before chatlog is created but most reliable one i can find...
    if (!document.getElementById(BC_CLUB_CHAT_ID)) chatObserverInit();
    return next(args);
}

const addChatRoomCreateHook = () => {
    if (document.getElementById(BC_CLUB_CHAT_ID)) addChatLogObserver();
    hookFunction(HOOK_GROUPS.ttsBcxVoice, MODULES.TTS, "ChatRoomLoad", HOOK_PRIORITIES.Observe, chatCreateHook);
};

const removeBcxVoiceHook = () => {
    removeHooksByToggle(HOOK_GROUPS.ttsBcxVoice);
    if (OBSERVE_INIT_TIMEOUT_ID) clearTimeout(OBSERVE_INIT_TIMEOUT_ID);
    try {
        if (TTS_CHAT_OBSERVER) {
            TTS_CHAT_OBSERVER.disconnect();
            TTS_CHAT_OBSERVER = null;
        }
    } catch (e) { }
};

export const registerTtsHooks = () => {
    if (!getTtsActive()) return;

    ChatRoomRegisterMessageHandler(TTS_READER);

    if (getTtsConfigValue("ttsBcxVoice")) {
        addChatRoomCreateHook();
    }
};


const removeTtsHooks = () => {
    ChatRoomMessageHandlers = ChatRoomMessageHandlers.filter((handler) => handler.Description !== TTS_READER.Description);
    removeBcxVoiceHook();
};

export const toggleTtxBcxListener = () => {
    const newValue = toggleTtsValue('ttsBcxVoice')
    if (newValue) {
        addChatRoomCreateHook();
    } else {
        removeBcxVoiceHook();
    }
};

export const toggleTtsActive = () => {
    const newValue = toggleConfig(CONF_PROPS.TTS_ISACTIVE);
    if (newValue) {
        registerTtsHooks();
    } else {
        removeTtsHooks();
    }
    return newValue;
};
