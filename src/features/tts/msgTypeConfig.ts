const MsgTypeDefaults = {};

//class for msg type configs
class MsgTypeConfig {
    type: "Default" | "Chat" | "Emote" | "Whisper" | "Beep" | "Action";
    isCustomised: boolean; //dont use default?
    isTtsActive: boolean; //TTS for this type
    voicePitch: number;
    voiceRate: number;
    ttsAll: boolean;
    ttsOwn: boolean;
    ttsSender: boolean; //name sender in tts speech
    ttsGagCheat: boolean; //true --> read the original, false --> read the gag speech

    //have voices? no? more to do definitely
    ttsPlayerVoices: Record<number, string>; //PlayerId, voiceName
    ttsDefaultVoice: string;

    constructor(storedObj?: MsgTypeConfig) {
        //if no storedObj --> use defaults.
    }

    getVoice() {}
    setDefaultVoice() {}
    getPlayerVoice() {}
    setPlayerVoice() {}
}
