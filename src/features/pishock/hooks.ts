import { ModSDKModAPI } from "bondage-club-mod-sdk";
import { getPishockValue } from "./config";
import { getPlayerId } from "../../common/utils";
import { sendShock } from "./api";
import { HOOK_GROUPS, MODULES, hookFunction, removeHooksByToggle } from "../../common/sdk";
import { HOOK_PRIORITIES } from "../../common/constants";

/** This will still miss the ones that dont send chat mesasges --> eg FuturisticPanties tamper protection */

function pishockHook(args: [ServerChatRoomMessage], next) {
    (() => {
        if (!getPishockValue("isActive") || args.length < 1) return;
        const [msgData] = args;
        if (!msgData?.Type || !msgData?.Content || !Array.isArray(msgData?.Dictionary)) return;
        const Dictionary = msgData.Dictionary as ChatMessageDictionary;

        //Content TriggerShock[0-2] || "Content": "FuturisticChastityBeltShockRequiredSpeech"
        if (msgData.Type === "Action" && msgData.Content?.includes("Shock")) {
            const targetChar = Dictionary.find(
                //@ts-ignore
                (d) => d.Tag === "DestinationCharacterName" || d.Tag === "DestinationCharacter"
                //@ts-ignore
            )?.MemberNumber;
            if (targetChar !== getPlayerId()) return;

            Dictionary.find((d) => {
                //@ts-ignore
                const shockIntensity = !Number.isNaN(Number(d.ShockIntensity));
                const isValid = !Number.isNaN(Number(shockIntensity));
                if (isValid) sendShock(Number(shockIntensity));
                return isValid;
            });
            //eg Content: ChatOther-ItemLegs-ShockItem"
        } else if (msgData.Type === "Activity" && msgData.Content?.includes("Shock")) {
            //@ts-ignore
            if (!Dictionary.find((d) => d.TargetCharacter === getPlayerId())) return;
            //@ts-ignore
            if (Dictionary.find((d) => d.ActivityName === "ShockItem")) {
                sendShock(0); //These dont have ShockIntensity --> use default value of 0 for lowest
            }
        }
    })();
    return next(args);
}

export const removePishockHooks = () => removeHooksByToggle(HOOK_GROUPS.pishockActive);

export const registerPishockHooks = () => {
    if (!getPishockValue("isActive")) return;
    hookFunction(HOOK_GROUPS.pishockActive, MODULES.PISHOCK, "ChatRoomMessage", HOOK_PRIORITIES.Observe, pishockHook);
};
