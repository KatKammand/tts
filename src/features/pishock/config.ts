//NEED TO KEEP THESE AWAY FROM THE WINDOW --> DONT WANT SOME SCRIPT SETTING INTENSITY TO MAX

import { CONF_PROPS, getConfigValue, setConfigValue } from "../../common/config";
import { ValueOf } from "../../types/tts";

export type IntenistyTypes = "lowIntensity" | "medIntensity" | "highIntensity";

export enum PISHOCK_OPS {
    SHOCK = <PISHOCK_OPS>0,
    VIBRATE = <PISHOCK_OPS>1,
    BEEP = <PISHOCK_OPS>2,
}

export type PishockAuth = {
    Username: string;
    Code: string; //eg 14BAEEDJ171
    Apikey: string; //"1ec2a45c-c2ec-4a11-7313-322943e1d3be",
};

//warn about the keys and such being stored in localstorage, so malicious scripts can capture them.
export type PISHOCK_CONFIG = PishockAuth & {
    isActive: boolean;
    lowIntensity: number;
    medIntensity: number;
    highIntensity: number;
    /** Allows setting what the shock triggers really do --> if someone wants to just "pretend shocks" for a while, replacing with beeps or vibrations instead */
    shockOperation: PISHOCK_OPS;
};

export const defaultPishockConfig: PISHOCK_CONFIG = {
    isActive: false,
    Username: "",
    Code: "",
    Apikey: "",
    lowIntensity: 10,
    medIntensity: 20,
    highIntensity: 30,
    shockOperation: PISHOCK_OPS.SHOCK,
};

export const setPishockValue = (key: keyof PISHOCK_CONFIG, value: ValueOf<PISHOCK_CONFIG>) => {
    const oldPshockConfig = getConfigValue(CONF_PROPS.PISHOCK_CONFIG) as PISHOCK_CONFIG;
    (oldPshockConfig[key] as ValueOf<PISHOCK_CONFIG>) = value;
    setConfigValue(CONF_PROPS.PISHOCK_CONFIG, { ...oldPshockConfig });
};

export const getPishockValue = (key: keyof PISHOCK_CONFIG): ValueOf<PISHOCK_CONFIG> =>
    getConfigValue(CONF_PROPS.PISHOCK_CONFIG)[key];

export const getPishockAuth = (): PishockAuth => ({
    Username: getPishockValue("Username") as string,
    Code: getPishockValue("Code") as string,
    Apikey: getPishockValue("Apikey") as string,
});

export const togglePishockValue = (value: "isActive") => {
    const keyValue = value as keyof PISHOCK_CONFIG;
    const newValue = !getPishockValue(keyValue);
    setPishockValue(keyValue, newValue);
    return newValue;
};

export const updateIntensity = (intenistyType: IntenistyTypes, newIntensity: number) => {
    if (newIntensity < 1 || newIntensity > 100) return;
    setPishockValue(intenistyType as keyof PISHOCK_CONFIG, newIntensity);
};
