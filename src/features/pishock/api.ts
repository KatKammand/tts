import { IntRange } from "../../types/tts";
import { IntenistyTypes, PISHOCK_OPS, PishockAuth, getPishockAuth, getPishockValue } from "./config";

//https://pishock.com/#/ --> sadly no direct link to docs, need to open in the tab "Pishock API Docs"

type IntensityRange = IntRange<1, 100>;
type DurationRange = IntRange<1, 15>;

type PishockPost = PishockAuth & {
    Name: "BONDAGE_CLUB_TTS_MOD";
    Intensity: IntensityRange; //"0"-"100" //not for beeps
    Duration: DurationRange; //seconds 1-15
    Op: 0 | 1 | 2; //0 --> shock? 1 --> vibrate? 3 --> beep?
};

const PISHOCK_ACTIONS_URL = "https://do.pishock.com/api/apioperate/";
const PISHOCK_INFO_URL = "https://do.pishock.com/api/GetShockerInfo";
const TTS_PISHOCK_SENDER_NAME = "BONDAGE_CLUB_TTS_MOD";

let SHOCK_MIN_INTERVAL_MS = 2500;

/** Date.valueOf() of last sent request */
let lastRequestTimeMs: number = 0;

const getIntensityValue = (clubShockIntensity: number) => {
    let intensityKey: IntenistyTypes = "lowIntensity";
    if (clubShockIntensity > 0 && clubShockIntensity < 3) {
        intensityKey = "medIntensity";
    } else if (clubShockIntensity === 3) {
        intensityKey = "highIntensity";
    }

    return getPishockValue(intensityKey) as IntensityRange;
};

async function postToApi<T>(url: string, obj: object): Promise<T> {
    try {
        const response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(obj),
        });

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        return (await response.json()) as T;
    } catch (error) {
        throw new Error(`Fetch error: ${error}`);
    }
}

const sendPishockAction = async (op: PISHOCK_OPS, intenisty: IntensityRange, duration: DurationRange) => {
    if (!getPishockValue("isActive")) {
        console.error("TTS - PISHOCK NOT ACTIVE");
        return;
    }
    const auth = getPishockAuth();
    if (!auth.Apikey || !auth.Code || !auth.Username) {
        console.error("TTS - PISHOCK - MISSING PART OF AUTHORIZATION VALUES");
        return;
    }

    try {
        const responseData = await postToApi<PishockPost>(PISHOCK_ACTIONS_URL, {
            ...auth,
            Name: TTS_PISHOCK_SENDER_NAME,
            Intensity: `${intenisty}`,
            Duration: `${duration}`,
            Op: `${op}`,
        });
        console.debug(`TTS - Pishock action done:`, responseData);
    } catch (error) {
        console.error(error);
    }
};

/** ATM only one used */
export const sendShock = async (clubShockIntensity: number) => {
    const timeNow = Date.now();
    if (timeNow - lastRequestTimeMs < SHOCK_MIN_INTERVAL_MS) return;
    console.debug("TTS - Pishock command at intensity:", clubShockIntensity);

    lastRequestTimeMs = timeNow;
    const shockOps = getPishockValue("shockOperation") as PISHOCK_OPS;
    await sendPishockAction(shockOps, getIntensityValue(clubShockIntensity), 1);
};

export const sendVibrate = async (vibrateIntensity: IntensityRange) => {
    const timeNow = Date.now();
    if (timeNow - lastRequestTimeMs < SHOCK_MIN_INTERVAL_MS) return;

    lastRequestTimeMs = timeNow;
    await sendPishockAction(PISHOCK_OPS.VIBRATE, vibrateIntensity, 5);
};

/** Use this as connection tester */
export const getPishockCollarData = async () => {
    const auth = getPishockAuth();
    if (!auth.Apikey || !auth.Code || !auth.Username) {
        console.error("TTS - PISHOCK - MISSING PART OF AUTHORIZATION VALUES");
        return "TTS - PISHOCK - MISSING PART OF AUTHORIZATION VALUES";
    }
    try {
        const responseData = await postToApi<PISHOCK_OPS>(PISHOCK_INFO_URL, auth);
        console.debug(`TTS - Pishock info fetched:`, responseData);
        return "TTS - Pishock connection working";
    } catch (error) {
        console.error(error);
        return "TTS - Pishock connection error" + error;
    }
};
