import {
    createButton,
    createCheckbox,
    createDivContainer,
    createInput,
    createLinkElement,
    createSliderElement,
    createTextSpan,
} from "../../common/components";
import { getPishockCollarData, sendShock, sendVibrate } from "./api";
import {
    getPishockValue,
    togglePishockValue,
    setPishockValue,
    updateIntensity,
    PishockAuth,
    PISHOCK_OPS,
} from "./config";
import { registerPishockHooks, removePishockHooks } from "./hooks";

export const PISHOCK_TAB_REFRESH_EVENT = "pishock-settings-refresh";

const testShockLow = () => sendShock(0);
const testVibrate = () => sendVibrate(20);
const testPishockData = () => getPishockCollarData();

const sliderOptions = () => {
    return createDivContainer([
        createTextSpan("Intensity levels go from 1-100, dont set them too high for yourself please ^^."),
        createTextSpan("Intensity 50 should feel the same as intensity 50 from Pishocks web page"),
        createSliderElement(
            "Low Shock Intensity",
            1,
            100,
            getPishockValue("lowIntensity") as number,
            (value) => updateIntensity("lowIntensity", value),
            1
        ),
        createSliderElement(
            "Medium Shock Intensity",
            1,
            100,
            getPishockValue("medIntensity") as number,
            (value) => updateIntensity("medIntensity", value),
            1
        ),
        createSliderElement(
            "High Shock Intensity",
            1,
            100,
            getPishockValue("highIntensity") as number,
            (value) => updateIntensity("highIntensity", value),
            1
        ),
    ]);
};

const shockOperation = () => {
    const getName = () => `Shock Operation: ${PISHOCK_OPS[getPishockValue("shockOperation") as PISHOCK_OPS]}`;

    return createDivContainer([
        createTextSpan(
            "What happens on shock trigger - useful when you want 'fake shocks', eg only beep or vibrate when normally shocked."
        ),
        createTextSpan("0 - Shock, 1 - Vibrate, 2 - Beep"),
        createSliderElement(
            getName,
            0,
            2,
            getPishockValue("shockOperation") as number,
            (value) => setPishockValue("shockOperation", value),
            1
        ),
    ]);
};

const pishockAuthOptionInput = (keyValue: keyof PishockAuth, helperText: string) => {
    const input = createInput(getPishockValue(keyValue) as string);

    const onSave = (e: Event) => {
        e.preventDefault();
        setPishockValue(keyValue, input.value);
    };

    const saveBtn = createButton(`Save ${keyValue}`, onSave);

    return createDivContainer([createTextSpan(helperText), input, saveBtn], "tts-boxed-container");
};
("Your Pishock username - see profile at pishock page: ");
const authOptions = () => {
    return createDivContainer([
        createTextSpan("In this part you need to add 3 values used to connect to your Pishock device/page."),
        createLinkElement("https://pishock.com/", "Pishock link"),
        createTextSpan(
            "On that page, go to right top corner 'MENU' --> 'PISHOCK API DOCS' --> 'Authentication', has instructions where to find the values"
        ),
        createTextSpan(
            "These are saved in your LocalStorage. It means other scripts and mods you run on this page can access them. Only run scripts you know and trust!"
        ),
        pishockAuthOptionInput("Username", "Your Pishock username - see profile at pishock page. Eg: 'PiPet'"),
        pishockAuthOptionInput(
            "Apikey",
            "Your Pishock Apikey - see profile at pishock page. Eg: '1ec2a40c-c2ec-4a19-7323-342943a1d3cn'"
        ),
        pishockAuthOptionInput(
            "Code",
            "Your Pishock Code - generate it for the collar at pishock page. Eg: '14BAEEDJ171'"
        ),
        createTextSpan(
            "After adding the value, press the save button or the data is not stored. Do this to all 3 before pressing the test."
        ),
        createButton("Test Connection with Vibration", testVibrate),
        createButton("Test Connection with low intensity shock", testShockLow),
    ]);
};

const pishockToggler = () => {
    const isActive = togglePishockValue("isActive");
    if (isActive) registerPishockHooks();
    else removePishockHooks();
};

/** Container for all misc settings */
export const pishockSettings = () => {
    const div = createDivContainer();

    const toggleBoxes = createDivContainer(
        [
            createCheckbox(
                getPishockValue("isActive") as boolean,
                pishockToggler,
                "Pishock Active - switch pishock module on/off"
            ),
        ],
        "tts-toggle-container"
    );

    div.append(
        ...[
            createTextSpan("PISHOCK - READ ALL INSTRUCTIONS CAREFULLY!"),
            createTextSpan("Uses the Pishock API to connect the clubs shock actions to Pishock collar."),
            createTextSpan("GOOD IDEA TO LIMIT MAX SHOCK IN PISHOCK SIDE TOO"),
            toggleBoxes,
            createTextSpan("Pishock integration still under testing, no promises that it works as intended ^^"),
            createTextSpan(
                "If active & connection is working (use test buttons in settings), almost all shock actions will also trigger a 1 second Pishock collar shock (max 1 per 2 seconds though)"
            ),
            authOptions(),
            sliderOptions(),
            shockOperation(),
        ]
    );

    return div;
};
