import { HOOK_PRIORITIES } from "../../common/constants";
import { hookWrap } from "../../common/hooks";
import { hookFunction, HOOK_GROUPS, removeHooksByToggle } from "../../common/sdk";
import { getMiscConfig, toggleMiscValue } from "./config";
import { beepVoiceToySync } from "./misc";

function voiceToy(args, next) {
    hookWrap(beepVoiceToySync, args, next);
}

export const registerBeepVoiceHook = () => {
    if (!getMiscConfig("beepFutureVibConn")) return;
    hookFunction(HOOK_GROUPS.beepFutureVibConn, null, "ServerAccountBeep", HOOK_PRIORITIES.Observe, voiceToy);
};

export const removeBeepVoiceHook = () => removeHooksByToggle(HOOK_GROUPS.beepFutureVibConn);

export const toggleBeepFutureVib = () => {
    const isActive = toggleMiscValue("beepFutureVibConn");
    if (isActive) registerBeepVoiceHook();
    else removeBeepVoiceHook();
};
