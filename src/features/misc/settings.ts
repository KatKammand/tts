import { createCheckbox, createDivContainer } from "../../common/components";
import { getMiscConfig, toggleMiscValue } from "./config";
import { toggleBeepFutureVib } from "./hooks";

export const MISC_TAB_REFRESH_EVENT = "misc-settings-refresh";

/** Container for all misc settings */
export const miscSettings = () => {
    const div = createDivContainer();

    const toggleBoxes = createDivContainer(
        [
            createCheckbox(
                getMiscConfig("beepFutureVibConn"),
                toggleBeepFutureVib,
                "Beeps can activate Futurisrtic Vibrator voice triggers. Should respect normal item Player restrictions."
            ),
            createCheckbox(
                getMiscConfig("beepFutureVibResponseBeep"),
                () => toggleMiscValue("beepFutureVibResponseBeep"),
                "Futuristic Vibrator beep trigger send response beep to informing what happened."
            ),
        ],
        "tts-toggle-container"
    );

    div.append(...[toggleBoxes]);

    return div;
};
