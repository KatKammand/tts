import { CONF_PROPS, getConfigValue, setConfigValue } from "../../common/config";
import { ValueOf } from "../../types/tts";

export type MISC_CONFIG = {
    beepFutureVibConn: boolean;
    beepFutureVibResponseBeep: boolean;
};

export const defaultMiscConfig: MISC_CONFIG = {
    beepFutureVibConn: false,
    beepFutureVibResponseBeep: true,
};

export const setMiscConfig = (key: keyof MISC_CONFIG, value: ValueOf<MISC_CONFIG>) => {
    const oldMiscConfig = getConfigValue(CONF_PROPS.MISC_CONFIG) as MISC_CONFIG;
    (oldMiscConfig[key] as ValueOf<MISC_CONFIG>) = value;
    setConfigValue(CONF_PROPS.MISC_CONFIG, { ...oldMiscConfig });
};

export const getMiscConfig = (key: keyof MISC_CONFIG): ValueOf<MISC_CONFIG> =>
    getConfigValue(CONF_PROPS.MISC_CONFIG)[key];

export const toggleMiscValue = (
    value: keyof MISC_CONFIG
) => {
    const keyValue = value;
    const newValue = !getMiscConfig(keyValue);
    setMiscConfig(keyValue, newValue);
    return newValue;
};
