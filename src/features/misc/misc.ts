import { FBC_BEEP_PREFIX } from "../../common/constants";
import { HookFunc } from "../../common/hooks";
import { getName, tts_sendBeep } from "../../common/utils";
import { getMiscConfig } from "./config";

/** FUTURISTIC VIBRATOR BEEPS */

const voiceTriggerFeedback = (feedBackMsg: string, senderId: number) => {
    if (!getMiscConfig("beepFutureVibResponseBeep") || !feedBackMsg) return;

    //make it into an "action" to avoid feedback triggered loop
    const feedbackInit = `${getName()}'s Futuristic Vibrator`;
    const finalMsg = `(${feedbackInit} ${feedBackMsg})`;
    tts_sendBeep(senderId, finalMsg);
};

//cant be bothered to create typedefs for the bc vanilla functions here
//modified InventoryItemVulvaFuturisticVibratorHandleChat
const beepVoiceTriggerProcess = (params: {
    data: VibratingItemData;
    C: Character;
    Item: Item;
    beepMsg: string;
    senderId: number;
}) => {
    const { data, C, Item, beepMsg, senderId } = params;
    //data, C, Item, LastTime
    if (!Item || !data || !Item || !beepMsg || !senderId) return;

    //These come from a beep, so must be a friend --> also cant be the sender themselves.
    const playerIdFromFriendlist = Player.FriendList.find((f) => f === senderId);
    if (!playerIdFromFriendlist) return;
    if (
        Item.Property.AccessMode === ItemVulvaFuturisticVibratorAccessMode.PROHIBIT_SELF &&
        playerIdFromFriendlist === C.MemberNumber
    )
        return;
    if (
        Item.Property.AccessMode === ItemVulvaFuturisticVibratorAccessMode.LOCK_MEMBER_ONLY &&
        playerIdFromFriendlist !== Item.Property.LockMemberNumber
    )
        return;

    //already checked that at least one trigger is included
    let newOption: null | VibratingItemOption = null;

    const TriggerValues = Item.Property?.TriggerValues.split(",");
    const msg = InventoryItemVulvaFuturisticVibratorDetectMsg(
        beepMsg.toUpperCase(),
        TriggerValues || ItemVulvaFuturisticVibratorTriggers
    );
    //vibrator modes, can only pick one
    if (msg.includes("Edge")) {
        newOption = data.options.find((o) => o.Name === "Edge");
    } else if (msg.includes("Deny")) {
        newOption = data.options.find((o) => o.Name === "Deny");
    } else if (msg.includes("Tease")) {
        newOption = data.options.find((o) => o.Name === "Tease");
    } else if (msg.includes("Random")) {
        newOption = data.options.find((o) => o.Name === "Random");
    } else if (msg.includes("Disable")) {
        newOption = data.options[0];
    } else if (msg.includes("Increase") || msg.includes("Decrease")) {
        const mode = InventoryItemVulvaFuturisticVibratorGetMode(Item, msg.includes("Increase"));
        newOption = data.options.find((o) => o.Name === mode);
    }

    let feedBackMsg = "";
    if (newOption !== null) {
        const previousOption = TypedItemFindPreviousOption(Object(data.options), Item);

        feedBackMsg += `mode was set from ${previousOption.Name} to ${newOption.Name}`;

        InventoryItemVulvaFuturisticVibratorSetMode(data, C, Item, newOption, previousOption as VibratingItemOption);
    }

    //triggered actions
    if (msg.includes("Shock")) {
        const msgBase = "shock module was triggered";
        if (feedBackMsg) {
            feedBackMsg += " and its " + msgBase;
        } else {
            feedBackMsg += msgBase;
        }

        //Automatic --> false --> shows the message for everyone else too
        PropertyShockPublishAction(C, Item, false);
    }
    voiceTriggerFeedback(feedBackMsg, senderId);

    return;
};

export const beepVoiceToySync: HookFunc = (args: [ServerAccountBeepResponse]) => {
    if (args.length < 1 || !getMiscConfig("beepFutureVibConn")) return;

    const [data] = args;
    if (
        !data ||
        data.BeepType !== null ||
        !data.Message ||
        data.Message.startsWith("*") || //FBC cleans '*' from the text, will have "Emote" int its stuff
        data.Message.startsWith("(")
    )
        return;

    const futureVib = Player.Appearance.find((a) => a?.Asset?.Name === "FuturisticVibrator");
    if (!futureVib) return;

    //fbc instant messenger adds {data} to end of Message --> remove this from morsed text
    const fbcBeepCleanLower = data.Message.split(FBC_BEEP_PREFIX).map((a) => a.toLowerCase()) as string[];
    if (fbcBeepCleanLower.length > 1) {
        const fbcMeta = fbcBeepCleanLower.slice(1).join(FBC_BEEP_PREFIX);
        if (fbcMeta.includes("emote") || fbcMeta.includes("action")) return; //means its FBC edited emote Beep
    }

    const triggerFound = futureVib?.Property?.TriggerValues?.split(",").find((a) =>
        fbcBeepCleanLower[0].includes(a.toLowerCase())
    );

    if (!triggerFound) return;

    const vibItemData = ExtendedItemGetData(futureVib.Asset, ExtendedArchetype.VIBRATING) as VibratingItemData;
    if (!vibItemData) return;
    beepVoiceTriggerProcess({
        data: vibItemData,
        C: Player,
        Item: futureVib,
        beepMsg: fbcBeepCleanLower[0],
        senderId: data.MemberNumber,
    });

    return;
};
