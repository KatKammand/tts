import changelog from "../../changelog.json";
import {
    INTENISTY_VIBRATION,
    SyncIntensities
} from "./constants";

export const mo_PsWait = (wait: number) => new Promise((resolve) => setTimeout(resolve, wait));
export const getPlayerId = (): number => window.Player?.MemberNumber;
export const getRoomPlayerById = (id: number) => window.ChatRoomCharacter.find((c) => c.MemberNumber === id);

export const getRoomPlayerNameById = (id: number): string => {
    const char = getRoomPlayerById(id);
    return char?.Nickname || char?.Name || "";
};

export const getValidCharNameForTTS = (c: Character) => {
    const { Nickname, Name } = c;
    const isValidNick = !!Nickname && !hasSpecialCharacters(Nickname);
    return isValidNick ? Nickname : Name;
};

export const getCommands = (): ICommand[] | undefined => window.Commands;
export const getName = (): string => window.Player?.Nickname || window.Player?.Name || "";
export const getMorserVersion = (): string => process.env.VERSION;

export const loadStyles = (cssString: string, styleId: string) => {
    if (document.getElementById(styleId)) return;
    const styleElement = document.createElement("style");
    styleElement.id = styleId;
    styleElement.appendChild(document.createTextNode(cssString));
    document.head.appendChild(styleElement);
};

export const mo_sendAction = (text: string) =>
    ServerSend("ChatRoomChat", {
        Content: "Beep",
        Type: "Action",
        Dictionary: [
            // EN
            { Tag: "Beep", Text: "msg" },
            // CN
            { Tag: "发送私聊", Text: "msg" },
            // DE
            { Tag: "Biep", Text: "msg" },
            // FR
            { Tag: "Sonner", Text: "msg" },
            // Message itself
            { Tag: "msg", Text: text },
        ],
    });

export const tts_sendBeep = (playerId: number, msg: string) => {
    if (!playerId || !msg) return;
    ServerSend("AccountBeep", { MemberNumber: playerId, Message: msg, BeepType: "" });
};

export const getRandomNumber = (max: number) => Math.floor(Math.random() * max) + 1;

/** Adds to bottom of chatlog for "removeSec" time the msg (Default 20, 0 for no removal) */
export const ttsChatMsg = (text: string | string[], removeSec = 20) => {
    const div = document.createElement("div");
    div.setAttribute("class", "ChatMessage mo-chat-msg");
    div.setAttribute("data-time", ChatRoomCurrentTime());
    div.setAttribute("data-sender", getPlayerId() + "");

    const finalString = Array.isArray(text) ? text.join("\n") : text;

    div.appendChild(document.createTextNode("TTS MOD:\n" + finalString));

    ChatRoomAppendChat(div);
    if (removeSec === 0) return;
    setTimeout(() => div?.parentElement?.removeChild(div), removeSec * 1000);
};

export const strCompare = (s1: string, s2: string) => s1?.toLowerCase() === s2?.toLowerCase();

export const replaceLinks = (inputString: string) => {
    const urlRegex = /(https?:\/\/[^\s]+)/g;
    return inputString.replace(urlRegex, "link");
};

/** Listed characters will be left for TTS to process */
const FILTERED_CHARS = ["~", "*"];
export const removeSpecialCharacters = (inputString: string, filterCharacters = FILTERED_CHARS) => {
    const escapedFilterCharacters = filterCharacters.map((char) => char.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"));
    const pattern = new RegExp(`[${escapedFilterCharacters.join("")}]`, "g");
    return inputString.replace(pattern, " ");
};

/** Special character nicknames dont go through the Speech API... */
export const hasSpecialCharacters = (inputString: string) => {
    return /[^\w\d\s]/.test(inputString);
};

/** -Number if v1 smaller, 0 if same, +Number if v2 smaller */
export const versionsCompare = (v1: string, v2: string): number => {
    const [major1, minor1, patch1] = v1.split(".").map((n) => Number(n));
    const [major2, minor2, patch2] = v2.split(".").map((n) => Number(n));
    if (major1 !== major2) return major1 - major2;
    if (minor1 !== minor2) return minor1 - minor2;
    return patch1 - patch2;
};

const createUpdatesTexts = (oldVersion: string): string[] => {
    const versions = Object.keys(changelog);
    const newUpdates = versions.filter((v) => versionsCompare(oldVersion, v) < 0).sort((a, b) => versionsCompare(a, b));
    return newUpdates.map((v) => {
        const updateText = changelog[v];
        const finalUpdateText = Array.isArray(updateText) ? updateText.map((s) => `- ${s}`).join("\n") : updateText;
        return `${v}:\n${finalUpdateText}`;
    }) as string[];
};

export const postUpdatesMsg = (oldVersion: string, currentVersion: string, displaySec = 0) =>
    ttsChatMsg(
        [
            `NEW TTS Version ${currentVersion} in use - previous version: ${oldVersion}`,
            "Change Log:",
            ...createUpdatesTexts(oldVersion || "0.0.0"),
        ],
        displaySec
    );

export const stringToPng = (text: string, font: string, textColor: string, bgColor: string) => {
    // Create a new canvas element
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");

    // Set the canvas size
    canvas.width = 85; // Set your desired width
    canvas.height = 85; // Set your desired height

    // Set the background color

    // Set the font and text color
    ctx.font = font;
    ctx.fillStyle = textColor;

    // Draw the text on the canvas
    ctx.fillText(text, 10, 50); // Adjust the position as needed

    // Convert the canvas to a data URL
    const dataURL = canvas.toDataURL("image/png");

    // Create an image element with the data URL
    const image = new Image();
    image.src = dataURL;

    return image;
};

function customAction(desc: string, targetIds: number[]) {
    if (targetIds?.length > 0) {
        targetIds.forEach((targetId) => {
            ServerSend("ChatRoomChat", {
                Content: "Beep",
                Type: "Action",
                Dictionary: [{ Tag: "Beep", Text: desc }],
                Target: targetId,
            });
        });
    } else {
        //everyone gets this message
        ServerSend("ChatRoomChat", { Content: "Beep", Type: "Action", Dictionary: [{ Tag: "Beep", Text: desc }] });
    }
}

export const intensityToVibrateLv = (int: SyncIntensities | undefined) => INTENISTY_VIBRATION[int];
export const getIntensityValueList = () => Object.values(INTENISTY_VIBRATION);

export const hasLetterOrNumber = (text: string): boolean => {
    // Regular expression to check for at least one letter or number
    const regex = /[a-zA-Z0-9]/;
    return regex.test(text);
};
