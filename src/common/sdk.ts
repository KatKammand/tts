import { ModSDKGlobalAPI, ModSDKModAPI, PatchHook } from "bondage-club-mod-sdk";
import { TTS_GIT_LINK } from "./constants";
import { getMorserVersion } from "./utils";

/** Used on Module disables --> Eg TTS_ACTIVE --> remove all TTS module hooks */
export enum MODULES {
    TTS = 0,
    TOYSYNC = 1,
    MORSE = 2,
    MISC = 3,
    PISHOCK = 4,
}

/**
 * Used on individual setting disables, eg Individual Misc hooks --> should have own toggle button in UI
 * Use config toggle names? yes? yes.
 */
export enum HOOK_GROUPS {
    RIBBON = 0,
    MORSE_ISACTIVE = 1,
    beepFutureVibConn = 2,
    TOYSYNC_ISACTIVE = 3,
    SHOW_TTS_ICON = 4,
    ttsBcxVoice = 5,
    pishockActive = 6,
}

type StoredHook = {
    hook: Function;
    removeCallback: () => void;
};

type PatchedFunctionData = {
    toggleName: HOOK_GROUPS;
    module: MODULES | null;
    hooks: StoredHook[];
};

let TTS_MOD_SDK: ModSDKModAPI = null;

const HOOKED_FUNCTIONS: Map<HOOK_GROUPS, PatchedFunctionData> = new Map();

export const initModSdk = (ttsModSdkRef: ModSDKGlobalAPI) => {
    TTS_MOD_SDK = ttsModSdkRef.registerMod(
        {
            name: "TTS",
            fullName: "TTS for chat and vibrations of all mediums",
            version: getMorserVersion(),
            repository: TTS_GIT_LINK,
        },
        {
            allowReplace: true,
        }
    );
};

function initHookableFunction(toggleName: HOOK_GROUPS, moduleName: MODULES): PatchedFunctionData {
    let result = HOOKED_FUNCTIONS.get(toggleName);
    if (!result) {
        result = {
            toggleName: toggleName,
            module: moduleName || null,
            hooks: [],
        };
        HOOKED_FUNCTIONS.set(toggleName, result);
    }
    return result;
}

/** The @param hook needs to be a constant function created once or it cant be compared/made sure its unique. */
export function hookFunction(
    toggleName: HOOK_GROUPS,
    module: MODULES | null = null,
    hookName: string,
    priority: number,
    hook: PatchHook
): () => void {
    const data = initHookableFunction(toggleName, module);

    /** Should only use proper Function or this compare wont work. */
    if (data.hooks.some((h) => h.hook === hook)) {
        console.debug("TTS DUPLICATE ATTEMPT HOOK", toggleName, hookName, hook);
        return;
    }

    const removeCallback = TTS_MOD_SDK.hookFunction(hookName, priority, hook);

    data.hooks.push({
        hook,
        removeCallback,
    });
}

export function removeHooksByToggle(target: HOOK_GROUPS) {
    const hooks = HOOKED_FUNCTIONS.get(target);
    if (!hooks) return;

    hooks.hooks.forEach((h) => h.removeCallback());
    hooks.hooks = [];
}

/** UNTESTED */
export function removeHooksByModule(module: MODULES) {
    const allHooks = Array.from(HOOKED_FUNCTIONS.values());
    allHooks.forEach((h) => {
        if (h.module === module) removeHooksByToggle(h.toggleName);
    });
}
