import { loadStyles } from "./utils";
import styles from "./components.module.css";

type ModalOptions = {
    callback?: (input?: string) => void | string | boolean;
    children?: HTMLElement[];
};

export type SelectorOption = {
    value: string;
    id?: string;
};

export type SelectorChangeHandler = (selected: HTMLOptionElement) => void;

export const createDivContainer = (children: HTMLElement[] = [], className: string = "tts-div-container") => {
    const div = document.createElement("div");
    if (className) div.className = className;
    div.append(...children);
    return div;
};

export const createOptionsList = (
    options: SelectorOption[] = [],
    initValue?: string,
    changeHandler?: SelectorChangeHandler
) => {
    const select = document.createElement("select");
    select.className = "tts-select";
    select.name = "tts-select";

    options.forEach((optionData) => {
        const option = document.createElement("option");
        option.className = "tts-option";
        option.text = optionData.value;
        const value = (option.value = optionData.id || optionData.value);
        option.value = value;

        if (initValue === value) {
            option.setAttribute("selected", "selected");
        }

        select.add(option);
    });

    if (!options || options?.length === 0) {
        const nullOption = document.createElement("option");
        nullOption.value = "";
        nullOption.text = "NO VALID OPTIONS - TRY AND OPEN THE SETTINGS AGAIN";
        select.add(nullOption);
    }

    if (changeHandler) {
        select.onchange = (e) => {
            const option = e.target as HTMLOptionElement;
            if (option.value === "") return;
            changeHandler(option);
        };
    }

    return select;
};

export const createButton = (textContent: string, onClick: (event?: Event) => void, className = "tts-btn") => {
    const btn = document.createElement("button");
    btn.onclick = onClick;
    btn.textContent = textContent;
    btn.className = className;
    return btn;
};

export const createListItem = (item: SelectorOption, onRemove?: (item: SelectorOption) => void) => {
    const listItem = document.createElement("li");
    listItem.textContent = item.value;
    listItem.value = Number(item.id || item.value);

    if (onRemove) {
        const delBtn = createButton("X", () => {
            onRemove(item);
            listItem.remove();
        });
        listItem.append(delBtn);
    }
    return listItem;
};

export const createList = (items: SelectorOption[], onRemove?: (item: SelectorOption) => void) => {
    const list = document.createElement("ul");
    list.append(...items.map((i) => createListItem(i, onRemove)));

    return list;
};

export const createClickBlocker = (clickHandler?: Function, focusHandler?: Function) => {
    const blocker = document.createElement("div");
    blocker.className = "tts-blocker";
    blocker.title = "Click to close the modal";
    blocker.addEventListener("click", (e) => {
        if (clickHandler) clickHandler(e);
    });
    blocker.addEventListener("focus", (e) => {
        if (focusHandler) focusHandler(e);
    });
    return blocker;
};

export const createTextSpan = (text: string, className: string = "tts-span") => {
    const span = document.createElement("span");
    span.textContent = text;
    span.className = className;
    return span;
};

export const createCheckbox = (
    isActive: boolean,
    clickHandler: (isActive: boolean) => void,
    name: string,
    isDisabled = false,
    className = "tts-checkbox"
): HTMLElement => {
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.checked = isActive;
    checkbox.name = name;
    checkbox.className = className;

    checkbox.addEventListener("change", () => {
        clickHandler(checkbox.checked);
    });

    if (isDisabled) {
        checkbox.disabled = true;
    }

    return createDivContainer([checkbox, createTextSpan(name, "tts-checkbox-span")], "tts-checkbox-container");
};

export const createModal = (children: HTMLElement[] = []) => {
    const modal = document.createElement("dialog");
    modal.className = "tts-dialog";
    modal.open = true;

    modal.append(...children.filter((a) => !!a));

    return modal;
};

export const createBtnContainer = () => {
    const buttonContainer = document.createElement("div");
    buttonContainer.className = "tts-btn-container";
    return buttonContainer;
};

export const createInput = (initialValue: string, onChangeHandler?: (e: Event) => void) => {
    const input = document.createElement("input");
    input.type = "text";
    input.name = "tts-input";
    input.addEventListener("mouseover", () => {
        input.select();
    });
    input.addEventListener("focus", () => {
        input.select();
    });
    if (onChangeHandler) {
        input.addEventListener("change", onChangeHandler);
    }

    input.value = initialValue;
    return input;
};

export const createTab = (
    view: HTMLElement,
    tabText: string,
    createContent: () => HTMLElement,
    refreshEventName: string,
    isDefault = false
) => {
    const tab = document.createElement("button");
    tab.classList.add("tts-tab");
    tab.textContent = tabText;

    const renderContent = () => {
        const content = createContent();
        content.classList.add("tts-tab-content");
        tab.classList.add("active");
        view.append(content);

        const tabs = document.querySelectorAll(".tts-tab");
        tabs.forEach((t) => t.classList.remove("active"));
        tab.classList.add("active");

        const contents = document.getElementsByClassName("tts-tab-content");
        for (const e of contents) {
            e.remove();
        }
        view.append(content);
    };

    if (isDefault) {
        renderContent();
    }

    view.addEventListener(refreshEventName, renderContent);
    tab.addEventListener("click", renderContent);

    return tab;
};

export const createLinkElement = (src: string, text: string, className = "tts-link"): HTMLAnchorElement => {
    const link = document.createElement("a");
    link.text = text;
    link.href = src;
    link.target = "_blank";
    link.className = className;

    return link;
};

export const createSliderElement = (
    name: string | (() => string),
    min: number | string | Function,
    max: number,
    initValue: number,
    onChange: (value: number) => void,
    sliderStep = 0.1,
    className = "tts-slider"
): HTMLElement => {
    const container = document.createElement("div");
    const label = document.createElement("label");
    const isDynamicNam = typeof name === "function";

    label.textContent = isDynamicNam ? name() : name;
    label.className = "tts-slider-text";
    container.appendChild(label);

    const slider = document.createElement("input");
    slider.type = "range";
    slider.step = `${sliderStep}`;
    slider.min = min.toString();
    slider.max = max.toString();
    slider.value = initValue.toString();
    slider.className = className;

    const valueDisplay = document.createElement("span");
    valueDisplay.className = "tts-slider-text";
    valueDisplay.textContent = slider.value;

    const changerFunc = (newValue: number) => {
        onChange(newValue);
        if (isDynamicNam) {
            label.textContent = name();
        }
    };

    slider.addEventListener("input", (event) => {
        const newValue = (event.target as HTMLInputElement).value;
        valueDisplay.textContent = newValue;
        changerFunc(Number(newValue));
    });

    slider.addEventListener("change", (event) => {
        changerFunc(Number((event.target as HTMLInputElement).value));
    });

    container.append(...[slider, valueDisplay, label]);

    return container;
};

/**
 * ISSUE WITH TAB CONTAINER --> IT CLOSES ALL TABS, EVEN THOSE OF OTHER "TAB GROUPS"
 * So as long as you only have 1 tab group, this works fine. And i will keep it at that.
 */
export const createTabContainer = (tabs: HTMLElement[] = []) => {
    const tabContainer = document.createElement("div");
    tabContainer.classList.add("tts-tabs");

    tabContainer.append(...tabs.filter((t) => !!t));

    return tabContainer;
};

export const showModal = (opts: ModalOptions) => {
    const buttonContainer = createBtnContainer();
    buttonContainer.appendChild(createButton("Close", close));

    const modal = createModal();

    if (opts.children) {
        modal.append(createDivContainer(opts.children, "tts-modal-children"));
    }
    modal.appendChild(buttonContainer);

    modal.addEventListener("click", (e) => {
        e.stopPropagation();
    });

    const keyClick = (e: KeyboardEvent) => {
        e.stopPropagation();
        if (e.key === "Escape") {
            close();
        }
    };
    document.addEventListener("keydown", keyClick);
    const blocker = createClickBlocker(close, () => null);
    document.body.append(blocker);

    function close() {
        blocker.remove();
        modal.close();
        modal.remove();
        document.removeEventListener("keydown", keyClick);
        opts.callback();
    }

    document.body.append(modal);
};

loadStyles(styles, "tts-component-styles");

export const showAsyncModal = (opts: ModalOptions): Promise<string | void> => {
    return new Promise((resolve) => {
        showModal({
            ...opts,
            callback: () => {
                if (opts.callback) opts.callback();
                resolve();
            },
        });
    });
};
