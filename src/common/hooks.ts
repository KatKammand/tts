import { registerBeepVoiceHook } from "../features/misc/hooks";
import { registerMorseHooks } from "../features/morse/hooks";
import { registerPishockHooks } from "../features/pishock/hooks";
import { registerToysyncHooks } from "../features/toysync/hooks";
import { registerTtsHooks } from "../features/tts/hooks";
import { initIconHook } from "./online";

export type HookFunc = (args: any[]) => any[] | void;

export type ItemUpdated = {
    Target: number;
    Group: string;
    Name: string;
    Property: { Effect: string[]; Mode: string; Intensity: number; };
    Craft: any;
};

/** Update all hook functions to new style, this is should be temporary wrapper */
export const hookWrap = (hook: HookFunc, args: any[], next) => {
    const newArgs = hook(args);
    if (newArgs) return next(newArgs);
    return next(args);
};

export const mo_hookToChat = () => {
    registerTtsHooks();

    registerMorseHooks();

    registerBeepVoiceHook();

    registerToysyncHooks();

    initIconHook();

    registerPishockHooks();
};
