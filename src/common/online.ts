import { CONF_PROPS, getConfigValue, toggleConfig } from "./config";
import { HOOK_PRIORITIES } from "./constants";
import { HOOK_GROUPS, hookFunction, removeHooksByToggle } from "./sdk";

/** WIP functions that can be used to share Players personal voice eventually. */
const sharedSettings = () => {
    //get voice set for own PlayerId, if null, share null, if not null
};

//do this only on init --> its visible to others... ahh... make it optional! WAIT FOR UI
const updateOnlineSettings = () =>
    ServerAccountUpdate.QueueData({
        OnlineSharedSettings: Player.OnlineSharedSettings,
    });

function iconHook(args: any[], next) {
    if (args?.length < 4) return;
    const [C, CharX, CharY, Zoom] = args;
    if (
        typeof C?.IsPlayer !== "function" ||
        typeof CharX !== "number" ||
        typeof CharY !== "number" ||
        typeof Zoom !== "number" ||
        ChatRoomHideIconState !== 0 ||
        !C?.FBCOtherAddons?.find((m) => m.name === "TTS")
    )
        return next(args);

    DrawTextFit("—", CharX + 290 * Zoom, CharY + 50 * Zoom, 20 * Zoom, "white", "Black");

    return next(args);
}

export const addIconHook = () => {
    hookFunction(
        HOOK_GROUPS.SHOW_TTS_ICON,
        null,
        "ChatRoomCharacterViewDrawOverlay",
        HOOK_PRIORITIES.AddBehaviour,
        iconHook
    );
};

export const removeIconHook = () => removeHooksByToggle(HOOK_GROUPS.SHOW_TTS_ICON);
export const initIconHook = () => {
    if (getConfigValue(CONF_PROPS.SHOW_TTS_ICON)) addIconHook();
};

export const toggleTTSIcon = () => {
    const newState = toggleConfig(CONF_PROPS.SHOW_TTS_ICON);
    if (newState) {
        addIconHook();
    } else {
        removeIconHook();
    }
    return newState;
};
