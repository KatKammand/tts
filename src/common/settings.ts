import { MISC_TAB_REFRESH_EVENT, miscSettings } from "../features/misc/settings";
import { MORSE_TAB_REFRESH_EVENT, morseSettings } from "../features/morse/settings";
import { PISHOCK_TAB_REFRESH_EVENT, pishockSettings } from "../features/pishock/settings";
import { TOYSYNC_TAB_REFRESH_EVENT, toysyncSettings } from "../features/toysync/settings";
import { TTS_TAB_REFRESH_EVENT, ttsSettings } from "../features/tts/settings";
import {
    createCheckbox,
    createDivContainer,
    createLinkElement,
    createTab,
    createTabContainer,
    createTextSpan,
    showAsyncModal,
} from "./components";
import { CONF_PROPS, getConfigValue } from "./config";
import { TTS_README_LINK } from "./constants";
import { toggleTTSIcon } from "./online";

let isOpen = false;

const SETTINGS_ID = "tts-settings-container";

const createSettings = () => {
    const div = createDivContainer();
    div.id = SETTINGS_ID;

    const iconCheckBox = createCheckbox(
        getConfigValue(CONF_PROPS.SHOW_TTS_ICON) as boolean,
        toggleTTSIcon,
        "Show TTS Icon - Show — under WCE icon for players that are using TTS mod (and sharing addon info in WCE)"
    );

    const tabs = createTabContainer();
    div.append(
        createDivContainer(
            [
                createTextSpan("TTS MOD VERSION: " + getConfigValue(CONF_PROPS.VERSION)),
                createLinkElement(TTS_README_LINK, "README LINK"),
            ],
            "tts-div-container-row"
        ),
        iconCheckBox,
        tabs
    );
    tabs.append(
        ...[
            createTab(div, "TTS Settings", ttsSettings, TTS_TAB_REFRESH_EVENT, true),
            createTab(div, "ToySync Settings", toysyncSettings, TOYSYNC_TAB_REFRESH_EVENT),
            createTab(div, "Morse Settings", morseSettings, MORSE_TAB_REFRESH_EVENT),
            createTab(div, "Pishock Settings", pishockSettings, PISHOCK_TAB_REFRESH_EVENT),
            createTab(div, "Misc Settings", miscSettings, MISC_TAB_REFRESH_EVENT),
        ]
    );

    return div;
};

export const openSettings = async () => {
    if (isOpen) return;
    isOpen = true;

    const div = createSettings();

    await showAsyncModal({
        children: [div],
        callback: () => {
            PreferenceSubscreenExtensionsClear();
        }
    });
    isOpen = false;
};
