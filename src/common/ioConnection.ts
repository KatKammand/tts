import { mo_PsWait, ttsChatMsg } from "./utils";
import * as Buttplug from "../types/buttplug.io.1.0.17";
import { CONF_PROPS, getConfigValue } from "./config";
import { addDevice, getDefaultDevice, removeDevice } from "./ioDevices";
import { getMorseVibrationsActive } from "../features/morse/config";

export type BPlugDevice = Buttplug.ButtplugClientDevice;
type BPlugClient = Buttplug.ButtplugClient;

let mo_bClient: Buttplug.ButtplugClient = null;
let mo_bConnector: Buttplug.ButtplugWebsocketConnectorOptions = null;
let mo_pingIntervalId = null;
let initStarted = false;

export const isIoConnectionNeeded = (): boolean =>
    getMorseVibrationsActive() || getConfigValue(CONF_PROPS.TOYSYNC_ISACTIVE);

//@ts-ignore
const getButtplugIo = () => document.querySelector("iframe")?.contentWindow?.Buttplug;

/** Loads all related stuff and connects */
const mo_bPlugLoader = async () => {
    console.log("TTS BUTTPLUG.IO LOADING");

    if (!!mo_bClient || initStarted) return;
    initStarted = true;

    const bPlug = getButtplugIo();
    if (bPlug) {
        mo_bPlugInitializer();
        return;
    }

    const frame = document.createElement("iframe");
    frame.style.display = "none";
    frame.id = "buttplug-io";
    const script = document.createElement("script");
    script.src = "https://cdn.jsdelivr.net/npm/buttplug@1.0.17/dist/web/buttplug.min.js";
    frame.onload = () => {
        frame.contentDocument.head.appendChild(script);
    };
    document.body.appendChild(frame);

    await (async function () {
        while (!getButtplugIo()) {
            await mo_PsWait(400);
        }

        await getButtplugIo().buttplugInit();
        await mo_bPlugInitializer();
    })();
};

const createConnector = (bPlug: any) => {
    if (!bPlug?.ButtplugWebsocketConnectorOptions) return;

    const url = getConfigValue(CONF_PROPS.INTIFACE_URL);
    const connector = new bPlug.ButtplugWebsocketConnectorOptions() as Buttplug.ButtplugWebsocketConnectorOptions;
    connector.Address = url;
    return connector;
};

//initializes buttplug io connection
const mo_bPlugInitializer = async () => {
    try {
        console.log("TTS BUTTPLUG.IO INITIALIZATION");

        const bPlug = getButtplugIo();
        if (!bPlug) return;

        const client = new bPlug.ButtplugClient("TTS-Sync") as BPlugClient;

        //triggered for all connected devices too.
        client.addListener("deviceadded", addDevice);
        client.addListener("deviceremoved", removeDevice);

        const connector = createConnector(bPlug);
        mo_bConnector = connector;
        mo_bClient = client;
        window["TTS_BUTTPLUGIO_CLIENT"] = mo_bClient; //expose this to window incase someone else wants to use it? also for testing.
    } catch (error) {
        console.error("TTS_IO_INIT_ERROR", error, getButtplugIo());
    }
};

const mo_bPlugConnect = async () => {
    console.log("TTS BUTTPLUG.IO CONNECTING");
    try {
        const bPlug = getButtplugIo();
        if (!bPlug || !mo_bClient) return;
        await mo_bClient?.connect(createConnector(bPlug));

        mo_connectionInitSpam();
    } catch (e) {
        ttsChatMsg("INTIFACE CENTRAL CONNECTION FAILED - MAKE SURE THE SERVER IS RUNNING AND IN PORT " + 12345);
        console.error(e);
    }
};

export const mo_activateConnection = async () => {
    if (getIsIoConnected()) return;

    if (!getClient()) {
        await mo_bPlugLoader();
    }
    await mo_bPlugConnect();
};

export const mo_disconnect = async () => {
    console.log("TTS BUTTPLUG.IO DISCONNECTING");
    await mo_bClient?.disconnect();
};

export const mo_ioReconnect = async () => {
    if (!isIoConnectionNeeded()) return;
    await mo_disconnect();
    await mo_activateConnection();
};

/**
 * spam the connection with stuff at the start, for some users connection timeouts otherwise.
 * After the spam is done, connection stays alive even without action.
 */
export const mo_connectionInitSpam = async () => {
    if (mo_pingIntervalId) {
        clearInterval(mo_pingIntervalId);
    }

    const mo_initEndTime = Date.now() + 35 * 1000;
    const initDevice = getDefaultDevice();
    if (!initDevice) {
        const errorMsg =
            "TTS toy connection initialization lacks device. Connect device to initialize the connection properly & '/tts-intiface-reconnect'. Intiface Central might disconnect after a while otherwise.";
        ttsChatMsg(errorMsg);
        return;
    }

    mo_pingIntervalId = setInterval(async () => {
        if (mo_initEndTime < Date.now()) {
            clearInterval(mo_pingIntervalId);
            console.log("TTS TOY CONNECTION INIT DONE");
            return;
        }

        mo_vibrateDevice(initDevice.LastIntensity, initDevice.bPlugDevice);
    }, 5 * 1000);
};

export const mo_getDevice = (index = 0) => getDevices()[index];

export const getDevices = (): BPlugDevice[] => {
    let devices = [];
    try {
        devices = mo_bClient?.Devices || [];
    } catch (e) {}
    return devices;
};

export const getIsIoConnected = () => {
    try {
        return !!mo_bClient?.Devices;
    } catch (e) {
        return false;
    }
};

export const getClient = () => mo_bClient;

//device.AllowedMessages.includes(0) // 0 is VibrateCmd
export const mo_vibrateDevice = async (str: number, device: BPlugDevice) => {
    await device?.vibrate(str);
};

export const connectionToggler = async (toggleFunc: () => Promise<boolean>, chatText?: string) => {
    const isActive = await toggleFunc();
    if (!isActive && getIsIoConnected && !isIoConnectionNeeded()) {
        mo_disconnect();
    }
    if (!chatText) return;
    ttsChatMsg(chatText + isActive);
};
