import { SlotOptions } from "./ioDevices";
import { CONFIG_NAME } from "./constants";
import { ModSDKModAPI } from "bondage-club-mod-sdk";
import { MORSE_CONFIG, morseConfig } from "../features/morse/config";
import { ValueOf } from "../types/tts"; //whats the use of this ValueOf... nothing... eh.
import { TTS_CONFIG, defaultTTSConfig } from "../features/tts/config";
import { MISC_CONFIG, defaultMiscConfig } from "../features/misc/config";
import { PISHOCK_CONFIG, defaultPishockConfig } from "../features/pishock/config";

export const defaultIntifaceUrl = "ws://127.0.0.1:12345";

export type TOYSYNC_SLOTTED_DEVICES = Record<SlotOptions, string[]> | {}; //slotname: deviceNames[]

export type TOYSYNC_CONFIG = {
    slottedDevices: TOYSYNC_SLOTTED_DEVICES;
};

export enum CONF_PROPS {
    VERSION = "mo_version",
    PREV_VERSION = "mo_prev_version",
    SHOW_TTS_ICON = "tts_icon",
    MORSE_ISACTIVE = "mo_MORSER_isActive",
    MORSE_CONFIG = "mo_morse_config",
    TOYSYNC_ISACTIVE = "mo_ToySync_isActive",
    TOYSYNC_CONFIG = "mo_ToySync_config",
    TTS_ISACTIVE = "tts_isActive",
    TTS_CONFIG = "tts_config",
    INTIFACE_URL = "mo_intiface_url",
    MEDIA_ISACTIVE = "media_isactive",
    MEDIA_CONFIG = "media_config",
    MISC_CONFIG = "misc_config",
    PISHOCK_CONFIG = "pishock_config",
}

type TTS_MOD_CONFIG = {
    [CONF_PROPS.VERSION]: string;
    [CONF_PROPS.PREV_VERSION]: string;
    [CONF_PROPS.SHOW_TTS_ICON]: boolean;
    [CONF_PROPS.MORSE_ISACTIVE]: boolean;
    [CONF_PROPS.MORSE_CONFIG]: MORSE_CONFIG;
    [CONF_PROPS.TOYSYNC_ISACTIVE]: boolean;
    [CONF_PROPS.TOYSYNC_CONFIG]: TOYSYNC_CONFIG;
    [CONF_PROPS.INTIFACE_URL]: string;
    [CONF_PROPS.TTS_ISACTIVE]: boolean;
    [CONF_PROPS.TTS_CONFIG]: TTS_CONFIG;
    [CONF_PROPS.MEDIA_ISACTIVE]: boolean;
    [CONF_PROPS.MISC_CONFIG]: MISC_CONFIG;
    [CONF_PROPS.PISHOCK_CONFIG]: PISHOCK_CONFIG;
};

const toySyncConfig: TOYSYNC_CONFIG = {
    slottedDevices: {},
};

const DEFAULT_SETTINGS = {
    [CONF_PROPS.VERSION]: null,
    [CONF_PROPS.PREV_VERSION]: null, //basically for the changelog command
    [CONF_PROPS.SHOW_TTS_ICON]: false,
    [CONF_PROPS.MORSE_ISACTIVE]: false,
    [CONF_PROPS.MORSE_CONFIG]: morseConfig,
    [CONF_PROPS.TOYSYNC_ISACTIVE]: false,
    [CONF_PROPS.TOYSYNC_CONFIG]: toySyncConfig,
    [CONF_PROPS.INTIFACE_URL]: defaultIntifaceUrl,
    [CONF_PROPS.TTS_ISACTIVE]: true,
    [CONF_PROPS.TTS_CONFIG]: defaultTTSConfig,
    [CONF_PROPS.MEDIA_ISACTIVE]: false,
    [CONF_PROPS.MISC_CONFIG]: defaultMiscConfig,
    [CONF_PROPS.PISHOCK_CONFIG]: defaultPishockConfig,
} as TTS_MOD_CONFIG;

let MORSE_SDK: ModSDKModAPI = null;

const getStorageKey = () => `${Player?.AccountName}.TTS.${CONFIG_NAME}`;

export const toggleConfig = (key: keyof TTS_MOD_CONFIG): boolean => {
    const newV = !getConfigValue(key);
    setConfigValue(key, newV);
    return newV;
};

export const saveAllSettings = () => {
    try {
        const allSettings = window[CONFIG_NAME] as TTS_MOD_CONFIG;
        const settingsString = JSON.stringify(allSettings);
        localStorage.setItem(getStorageKey(), settingsString);
    } catch (e) {}
};

const populateObject = (
    defautlObj: MORSE_CONFIG | TTS_CONFIG | TOYSYNC_CONFIG | MISC_CONFIG | PISHOCK_CONFIG,
    storedObj: MORSE_CONFIG | TTS_CONFIG | TOYSYNC_CONFIG | MISC_CONFIG | PISHOCK_CONFIG | undefined
) => {
    if (!storedObj) return { ...defautlObj };

    return Object.keys(defautlObj).reduce((obj, key) => {
        const storedValue = storedObj[key];
        obj[key] = storedValue ?? defautlObj[key];
        return obj;
    }, {});
};

/** v0.15.8 --> v1.0.0 update --> remove at some point... */
const fixVoiceNameCasing = (ttsconfig: TTS_CONFIG): TTS_CONFIG => {
    const playerVoices = ttsconfig.ttsPlayerVoices;
    let updatedList = null;
    if (playerVoices && Object.keys(playerVoices).length > 0) {
        if (!updatedList) {
            updatedList = {};
        }
        const keys = Object.keys(playerVoices);
        keys.forEach((k) => {
            updatedList[k] = playerVoices[k].toLocaleLowerCase();
        });
    }

    return {
        ...ttsconfig,
        ttsDefaultVoice: ttsconfig?.ttsDefaultVoice?.toLocaleLowerCase() || "",
        ttsPlayerVoices: updatedList ? updatedList : ttsconfig.ttsPlayerVoices,
    };
};

const populateSettings = (storedSettings: TTS_MOD_CONFIG) => {
    if (!storedSettings) return DEFAULT_SETTINGS;

    const newSettings = Object.keys(DEFAULT_SETTINGS).reduce((obj, key) => {
        const storedValue = storedSettings[key];

        switch (key) {
            case CONF_PROPS.TTS_CONFIG:
                const ttsConfigTemp = fixVoiceNameCasing(storedValue);
                obj[key] = populateObject(DEFAULT_SETTINGS[key], ttsConfigTemp);
                break;
            case CONF_PROPS.TOYSYNC_CONFIG:
            case CONF_PROPS.MORSE_CONFIG:
            case CONF_PROPS.MEDIA_CONFIG:
            case CONF_PROPS.MISC_CONFIG:
            case CONF_PROPS.PISHOCK_CONFIG:
                obj[key] = populateObject(DEFAULT_SETTINGS[key], storedValue);
                break;
            default:
                obj[key] = storedValue ?? DEFAULT_SETTINGS[key];
        }
        return obj;
    }, {});
    return newSettings;
};

export const loadAllSettings = () => {
    let storedSettings = null;

    try {
        const settingsString = localStorage.getItem(getStorageKey());
        if (settingsString) {
            storedSettings = JSON.parse(settingsString) as TTS_MOD_CONFIG;
        }
    } catch (e) {
        localStorage.removeItem(getStorageKey());
    }
    const startSettings = populateSettings(storedSettings);
    window[CONFIG_NAME] = startSettings;
};

export const getToySyncConfigValue = (key: keyof TOYSYNC_CONFIG): ValueOf<TOYSYNC_CONFIG> => {
    return getConfigValue(CONF_PROPS.TOYSYNC_CONFIG)[key];
};

export const setToySyncConfigValue = (key: keyof TOYSYNC_CONFIG, value: ValueOf<TOYSYNC_CONFIG>, save = true) => {
    const oldToySyncConfig = getConfigValue(CONF_PROPS.TOYSYNC_CONFIG) as TOYSYNC_CONFIG;
    oldToySyncConfig[key] = value;
    setConfigValue(CONF_PROPS.TOYSYNC_CONFIG, { ...oldToySyncConfig }, save);
};

export const getConfigValue = (key: keyof TTS_MOD_CONFIG): any => window[CONFIG_NAME][key];

export const setConfigValue = (key: keyof TTS_MOD_CONFIG, value: any, save = true) => {
    window[CONFIG_NAME] = { ...window[CONFIG_NAME], [key]: value };
    if (save) saveAllSettings();
};

export const toggleMorse = () => toggleConfig(CONF_PROPS.MORSE_ISACTIVE);
export const mo_toggleToySyncActive = () => toggleConfig(CONF_PROPS.TOYSYNC_ISACTIVE);

export const updateVersion = (newVersion: string) => {
    const oldVersion = getConfigValue(CONF_PROPS.VERSION);
    setConfigValue(CONF_PROPS.PREV_VERSION, oldVersion, false);
    setConfigValue(CONF_PROPS.VERSION, newVersion);
};
