export const CONFIG_NAME = "TTS_CONFIG";
export const OLD_CONFIG_NAME = "MORSER_CONFIG";
export const TTS_GIT_LINK = "https://gitlab.com/KatKammand/tts";

// not feeling like figuring away to make it work in testing and "prod" without this try catch.
const TTS_GIT_PUBLIC_LINK = "https://katkammand.gitlab.io/tts";
let envKOKORO_TTS_LINK = TTS_GIT_PUBLIC_LINK;
try {
    envKOKORO_TTS_LINK = process?.env?.KOKORO_TTS_LINK || TTS_GIT_PUBLIC_LINK;
} catch (e) {
    envKOKORO_TTS_LINK = TTS_GIT_PUBLIC_LINK
}
/** Root link to the hosted files of the TTS project on GitLab. */

export const TTS_GIT_LINK_KOKORO = `${envKOKORO_TTS_LINK}/kokoro-tts`;
export const TTS_README_LINK = "https://gitlab.com/KatKammand/tts/-/blob/main/README.md";
export const TTS_README_LINK_VOICE = `${TTS_README_LINK}#voices`;
export const TTS_README_LINK_TTSTOYSYNC = `${TTS_README_LINK}#tts-toysync`;

export const FBC_BEEP_PREFIX = "\n\n\uf124";

export const BC_CLUB_CHAT_ID = "TextAreaChatLog";

export enum HOOK_PRIORITIES {
    Top = 11,
    OverrideBehaviour = 10,
    ModifyBehaviourHigh = 6,
    ModifyBehaviourMedium = 5,
    ModifyBehaviourLow = 4,
    AddBehaviour = 3,
    Observe = 0,
}

export type SyncIntensities = -1 | 0 | 1 | 2 | 3;

export const INTENISTY_VIBRATION: Readonly<Record<SyncIntensities, number>> = {
    "-1": 0,
    "0": 0.1,
    "1": 0.4,
    "2": 0.75,
    "3": 1,
};
