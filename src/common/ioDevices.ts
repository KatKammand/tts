import * as Buttplug from "../types/buttplug.io.1.0.17";
import { mo_PsWait, strCompare } from "./utils";

export type BPlugDevice = Buttplug.ButtplugClientDevice;

/** With new items coming in, might end up with new ones, but for TS check, assume just these. */
export type SlotOptions =
    | "itemfeet"
    | "itemlegs"
    | "itemvulva"
    | "itemvulvapiercings"
    | "itembutt"
    | "itempelvis"
    | "itemnipples"
    | "itemnipplespiercings"
    | "itembreast"
    | "itemdevices";

export type ToySyncDevice = {
    SlotName: SlotOptions | null;
    /** 0.0 - 1.0 */
    LastIntensity: number;
    bPlugDevice: BPlugDevice;
    isMorsing: boolean;
    isTtsToysyncing: boolean;
    isDisconnected: boolean;
};

export type AddDeviceFunction = (device: ToySyncDevice) => void;
export type DeviceChangeFunction = (device: ToySyncDevice) => void;

/** Name (lower): Device */
const CONNECTED_DEVICES: Map<string, ToySyncDevice> = new Map();

const ON_ADD_ACTIONS: AddDeviceFunction[] = [];
const ON_REMOVE_ACTIONS: DeviceChangeFunction[] = [];

export const subscribeToAddDevice = (func: AddDeviceFunction) => ON_ADD_ACTIONS.push(func);
export const subscribeToRemoveDevice = (func: DeviceChangeFunction) => ON_REMOVE_ACTIONS.push(func);

export const addDevice = (device: BPlugDevice) => {
    //check if device is slotted & do initVibrations to it? --> keep list of deviceadded callbacks?
    console.log("TTS INTIFACE DEVICE ADDED", device);
    CONNECTED_DEVICES.set(device.Name?.toLowerCase(), {
        SlotName: null,
        LastIntensity: 0,
        bPlugDevice: device,
        isMorsing: false,
        isTtsToysyncing: false,
        isDisconnected: false,
    });
    const connDevice = CONNECTED_DEVICES.get(device.Name?.toLowerCase());
    ON_ADD_ACTIONS.forEach((f) => f(connDevice));
};

export const removeDevice = (device: BPlugDevice) => {
    const nameLower = device.Name?.toLowerCase();
    console.log("TTS INTIFACE DEVICE REMOVED", device);
    const foundDevice = getDeviceByName(nameLower);
    foundDevice.isDisconnected = true;
    foundDevice.bPlugDevice = null;
    CONNECTED_DEVICES.delete(nameLower);
    ON_REMOVE_ACTIONS.forEach((f) => f(foundDevice));
};

export const updateDevice = (device: ToySyncDevice) =>
    CONNECTED_DEVICES.set(device.bPlugDevice?.Name?.toLowerCase(), device);

/** use this only when device doesnt matter, like initConnectSpam */
export const getDefaultDevice = () => Array.from(CONNECTED_DEVICES.values())[0];
export const getAllDevices = () => Array.from(CONNECTED_DEVICES.values());

export const getDeviceByName = (name: string) => CONNECTED_DEVICES.get(name?.toLowerCase());

export const getDeviceBySlot = (slotName: SlotOptions) => {
    return Array.from(CONNECTED_DEVICES.values()).find((d) => strCompare(d.SlotName, slotName));
};

export const getDeviceNamesList = () => Array.from(CONNECTED_DEVICES.keys());

//Should override ToySync vibrations while the morsing is in progress, also
export const morseDevice = async (action: (device: BPlugDevice) => Promise<void>, deviceName: string) => {
    const device = getDeviceByName(deviceName);
    if (!device || device.isMorsing) return;
    device.isMorsing = true;

    const { SlotName, LastIntensity } = device;
    const toySyncWasActive = SlotName && LastIntensity !== 0;

    if (toySyncWasActive) {
        //ToySync is active --> pause it while morse is happening && wait to singal incoming morse
        await device.bPlugDevice?.vibrate(0);
        await mo_PsWait(500); //wait to singal incoming morse
    }

    await action(device.bPlugDevice);

    //resume where the device was left at (keep LastIntensity up to date through all this though)
    if (toySyncWasActive) {
        await mo_PsWait(500); //wait to singal incoming morse
        await device?.bPlugDevice?.vibrate(LastIntensity);
    }

    device.isMorsing = false;
};

export const vibrateDevice = async (device: ToySyncDevice, intenistyValue: number) => {
    device.LastIntensity = intenistyValue;
    await device?.bPlugDevice?.vibrate(intenistyValue);
};

/** ['name: string | slot: string'] */
export const getAllDeviceNames = (): string[] => {
    return Array.from(CONNECTED_DEVICES.values()).map((d) => `device: ${d.bPlugDevice?.Name} | slot: ${d.SlotName}`);
};
