import { CONF_PROPS, getConfigValue, toggleMorse } from "./config";
import { setTtsConfigValue } from "../features/tts/config";
import { getTtsConfigValue } from "../features/tts/config";
import { openSettings } from "./settings";
import { getIsIoConnected, mo_disconnect, mo_ioReconnect, isIoConnectionNeeded } from "./ioConnection";
import { mo_train_type, mo_toggleMorseVibrate } from "../features/morse/morse";
import { getCommands, getMorserVersion, postUpdatesMsg, ttsChatMsg } from "./utils";
import { ttsClearQueue, ttsSkipToNext } from "../features/tts/ttsQueue";
import { getToySyncActive } from "../features/toysync/config";
import { toggleTtsActive } from "../features/tts/hooks";
import { getMorseVibrationsActive, getShareDevices } from "../features/morse/config";
import { mo_chatTriggers } from "../features/morse/hooks";
import { toggleToySyncActive } from "../features/toysync/hooks";

const getMorseActive = () => getConfigValue(CONF_PROPS.MORSE_ISACTIVE);
const getMorseVibActive = getMorseVibrationsActive;
const getTtsActive = () => getConfigValue(CONF_PROPS.TTS_ISACTIVE);

const connectionToggler = async (toggleFunc: () => Promise<boolean>, chatText?: string) => {
    const isActive = await toggleFunc();
    if (!isActive && getIsIoConnected && !isIoConnectionNeeded()) {
        mo_disconnect();
    }
    if (!chatText) return;
    ttsChatMsg(chatText + isActive);
};

const morseBasicToggler = (): boolean => {
    const newV = toggleMorse();
    //if base module is off, turn the vibrations module off too
    if (!newV && getMorseVibActive()) connectionToggler(mo_toggleMorseVibrate);
    return newV;
};

const morserHelp = (): string[] => [
    "TTS MOD version: " + getMorserVersion(),
    "Morse module is active: " + getMorseActive(),
    "Morse vibrations module is active: " + getMorseVibActive(),
    "Morse device share is active: " + getShareDevices(),
    "ToySync module is active: " + getToySyncActive(),
    "TTS module is active: " + getTtsActive(),
    "TTS All messages is active: " + getTtsConfigValue("ttsAll"),
    "TTS Own messages is active: " + getTtsConfigValue("ttsOwn"),
    "TTS Sender voicing is active: " + getTtsConfigValue("ttsSender"),
];

/* For vanilla Tag cannot include a previous (shorter) Tag (Eg /morser matches /morser-1 in Vanilla - fine with some mod, idk witch) */
const cmds: ICommand[] = [
    {
        Tag: "tts-mod-status",
        Description: "Shows TTS MODS version & active module statuses.",
        Action: () => ttsChatMsg(morserHelp()),
    },
    {
        Tag: "tts-mod-changelog",
        Description: "Shows TTS MODS changelog since last version you used.",
        Action: () => postUpdatesMsg(getConfigValue(CONF_PROPS.PREV_VERSION), getConfigValue(CONF_PROPS.VERSION)),
    },
    {
        Tag: "morse-help",
        Description:
            "Commands to send to others with the module work through Beep, Chat, Whisper. Replace your own memberId in the commands",
        Prerequisite: getMorseActive,
        Action: () => ttsChatMsg(Object.values(mo_chatTriggers).map((t) => t.help)),
    },
    {
        Tag: "morse-toggle",
        Description: "Toggle Morsing modules active status",
        Action: () => ttsChatMsg("Morse module is: " + morseBasicToggler()),
    },
    {
        Tag: "morse-vibrations-toggle",
        Description: "Toggle Morse messages active status - Morses with Buttplug.io toy vibrations if true",
        Prerequisite: getMorseActive,
        Action: async () => await connectionToggler(mo_toggleMorseVibrate, "Morse Vibrations module is: "),
    },
    {
        Tag: "morse-test",
        Description: "Test morsing yourself a message, opens prompt which you can write the message in.",
        Prerequisite: getMorseActive,
        Action: mo_train_type,
    },
    {
        Tag: "tts-intiface-reconnect",
        Description: "Reconnects TTS to Intiface Central. Only reconnects if ToySync and/or Morse is active",
        Prerequisite: isIoConnectionNeeded,
        Action: mo_ioReconnect,
    },
    {
        Tag: "toysync-toggle",
        Description: "Toggle ToySync module active status",
        Action: async () => await connectionToggler(toggleToySyncActive, "ToySync module is: "),
    },
    {
        Tag: "tts-toggle",
        Description: "Toggle TTS module active status",
        Action: () => ttsChatMsg("TTS module is: " + toggleTtsActive()),
    },
    {
        Tag: "tts-all",
        Description:
            "Toggle TTS all - chat/emotes/whispers or only texts coming after 'TTS:' - Active means ALL are TTS voiced.",
        Prerequisite: getTtsActive,
        Action: () => {
            const newValue = !getTtsConfigValue("ttsAll");
            setTtsConfigValue("ttsAll", newValue);
            ttsChatMsg("TTS all is: " + newValue);
        },
    },
    {
        Tag: "tts-own",
        Description: "Toggle TTS for messages you sent - Active means your own message are TTS voiced",
        Prerequisite: getTtsActive,
        Action: () => {
            const newValue = !getTtsConfigValue("ttsOwn");
            setTtsConfigValue("ttsOwn", newValue);
            ttsChatMsg("TTS own is: " + newValue);
        },
    },
    {
        Tag: "tts-sender",
        Description: "Toggle sender being read in tts speech - Active means sender player name is voiced.",
        Prerequisite: getTtsActive,
        Action: () => {
            const newValue = !getTtsConfigValue("ttsSender");
            setTtsConfigValue("ttsSender", newValue);
            ttsChatMsg("TTS sender voicing is: " + newValue);
        },
    },
    {
        Tag: "tts-skip",
        Description: "TTS skips currently voiced line. Eg someone sent Skyrim intro and you want to move on",
        Prerequisite: getTtsActive,
        Action: ttsSkipToNext,
    },
    {
        Tag: "tts-clear",
        Description:
            "TTS clears current queue completely, all so far unvoiced will not be voiced, and stops current voicing.",
        Prerequisite: getTtsActive,
        Action: ttsClearQueue,
    },
    {
        Tag: "tts-ui",
        Description: "Opens settings ui",
        Action: openSettings,
    },
];

export const mo_addCommands = () => {
    for (const c of cmds) {
        if (getCommands().some((a) => a.Tag === c.Tag)) {
            continue;
        }
        getCommands().push(c);
    }
};
