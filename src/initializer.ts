import { ModSDKGlobalAPI } from "bondage-club-mod-sdk"; //this builds it into my min.js --> works but extra stuff that other mods already bring... but i dont see a better way?
import { mo_addCommands } from "./common/commands";
import { CONF_PROPS, getConfigValue, loadAllSettings, updateVersion } from "./common/config";
import { CONFIG_NAME } from "./common/constants";
import { mo_hookToChat } from "./common/hooks";
import {
    getDevices,
    getIsIoConnected,
    isIoConnectionNeeded,
    mo_activateConnection,
    mo_disconnect,
} from "./common/ioConnection";
import { initModSdk } from "./common/sdk";
import { openSettings } from "./common/settings";
import { getMorserVersion, getPlayerId, loadStyles, postUpdatesMsg, ttsChatMsg, versionsCompare } from "./common/utils";
import { toysyncInitializer } from "./features/toysync/toySync";
import { addTtsListeners, ttsClearQueue } from "./features/tts/ttsQueue";
import styles from "./global.module.css";
import ttsLogo from "./resources/ttslogoSmall.png";

let versionCheckTimeoutId = null;

const versionCheck = (currentVersion: string) => {
    const isChatReady = document.getElementById("TextAreaChatLog");
    if (!isChatReady) {
        if (versionCheckTimeoutId) clearTimeout(versionCheckTimeoutId);
        versionCheckTimeoutId = setTimeout(() => versionCheck(currentVersion), 2000);
        return;
    }

    const storedVersion = getConfigValue(CONF_PROPS.VERSION);
    if (storedVersion && versionsCompare(storedVersion, currentVersion) < 0) {
        postUpdatesMsg(storedVersion, currentVersion);
        updateVersion(currentVersion);
    } else if (!storedVersion) {
        //1st time
        updateVersion(currentVersion);
        ttsChatMsg(
            [
                `TTS VERSION ${currentVersion}`,
                "TTS Settings on BC Extensions page or open with command",
                "To activate Better voices - go to TTS settings and check \"Use Kokoro-TTS High Quality AI\"",
                "'/tts-ui' to open the settings UI",
                "'/help' for list of commands (click bolded parts for autocomplete)",
                "See README at 'https://gitlab.com/KatKammand/tts/-/blob/main/README.md'",
            ],
            0
        );
    }
};

const TTS_MOD_NAME = "TTS";

const initSettingsMenu = () => {
    PreferenceRegisterExtensionSetting({
        Identifier: TTS_MOD_NAME,
        ButtonText: 'TTS Settings',
        Image: ttsLogo,
        load: () => {
            openSettings();
        },
        run: () => {
        },
        click: () => {
        },
        exit: () => {
        },
    });
};

export const mo_start = async (bcModSdkRef?: ModSDKGlobalAPI) => {
    const modSdkRef = bcModSdk || bcModSdkRef;

    if (!getPlayerId() || !modSdkRef) {
        return setTimeout(mo_start, 2000);
    }

    if (!window[CONFIG_NAME]) {
        loadAllSettings();

        const currentVersion = getMorserVersion();
        //@ts-ignore
        const ttsModSdkRef = (modSdkRef?.registerMod ? modSdkRef : modSdkRef.default) as ModSDKGlobalAPI;
        initModSdk(ttsModSdkRef);
        initSettingsMenu();

        versionCheck(currentVersion);
        toysyncInitializer();

        if (isIoConnectionNeeded()) {
            await mo_activateConnection();
        }

        loadStyles(styles, "tts-global-css");
        mo_hookToChat();
        mo_addCommands();
        await addTtsListeners();

        window.addEventListener(
            "beforeunload",
            (e) => {
                console.log("TTS UNLOAD ACTIONS");
                ttsClearQueue();
                if (getIsIoConnected()) {
                    try {
                        getDevices().forEach((d) => d?.vibrate(0));
                        mo_disconnect();
                    } catch (e) { }
                }
                return null;
            },
            {
                capture: true,
            }
        );
    }
};
